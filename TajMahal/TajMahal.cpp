#include<stdio.h>
#include<stdlib.h>
#include<math.h>

#include<GL/glut.h>

#define BLACK 0, 0, 0
#define M_PI 3.1415923


//make a global variable -- for tracking the anglular position of camera
			//in radian

double upX,upY,upZ;
double rollAngle;
double cameraRadiusX;
double cameraAngleX;
double cameraRadiusY;
double cameraAngleY;
double cameraRadiusZ;
double cameraAngleZ;

double cameraHeight;	
double cameraRadius;

double cameraForward;
double cameraAlong;
double cameraUp;

double lookX,lookY,lookZ;

double rectAngle;	//in degree

bool canDrawGrid;
double cameraAngle;

void drawUnitSolidCube();

//sabbir
GLUquadric* IDquadric=gluNewQuadric() ;
void moon(){
	glPushMatrix();{
		glBegin(GL_QUADS);{
			glVertex3f(0,5,0);
			glVertex3f(20,8,0);
			glVertex3f(20,-3,0);
			glVertex3f(0,-7,0);
		}glEnd();
		glBegin(GL_QUADS);{
			glVertex3f(20,8,0);
			glVertex3f(20,-3,0);
			glVertex3f(40,10,0);
			glVertex3f(40,20,0);
		}glEnd();
		glBegin(GL_QUADS);{
			glVertex3f(40,10,0);
			glVertex3f(40,20,0);
			glVertex3f(55,35,0);
			glVertex3f(55,30,0);
		}glEnd();
		glBegin(GL_TRIANGLES);{
			glVertex3f(55,35,0);
			glVertex3f(55,30,0);
			glVertex3f(63,45,0);
		}glEnd();
	}glPopMatrix();
	glPushMatrix();{
		glRotatef(180,0,1,0);
		glBegin(GL_QUADS);{
			glVertex3f(0,5,0);
			glVertex3f(20,8,0);
			glVertex3f(20,-3,0);
			glVertex3f(0,-7,0);
		}glEnd();
		glBegin(GL_QUADS);{
			glVertex3f(20,8,0);
			glVertex3f(20,-3,0);
			glVertex3f(40,10,0);
			glVertex3f(40,20,0);
		}glEnd();
		glBegin(GL_QUADS);{
			glVertex3f(40,10,0);
			glVertex3f(40,20,0);
			glVertex3f(55,35,0);
			glVertex3f(55,30,0);
		}glEnd();
		glBegin(GL_TRIANGLES);{
			glVertex3f(55,35,0);
			glVertex3f(55,30,0);
			glVertex3f(63,45,0);
		}glEnd();
	}glPopMatrix();
}
void glutSolidCircle(float r, int num_segments) {
    int i=0;
	float theta=(i*2*M_PI)/num_segments;
	float x1=r*cos(theta);
	float y1=r*sin(theta);
	for(i=1;i<=num_segments;i++){
		theta=(i*2*M_PI)/num_segments;
		float x2=r*cos(theta);
		float y2=r*sin(theta);
		
		glBegin(GL_TRIANGLES);{
			glVertex3f(0,0,0);
			glVertex3f(x1,y1,0);
			glVertex3f(x2,y2,0);
		}glEnd();
		
		x1=x2;
		y1=y2;

	}
}
void gombujTop(){
	glColor3f(0.7,0.5,0.4);
	glPushMatrix();{
		gluCylinder(IDquadric,1,1,50,20,20);
	}glPopMatrix();
	glPushMatrix();{
		gluCylinder(IDquadric,10,8,5,20,20);
		glTranslatef(0,0,5);
		glutSolidCircle(8,20);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,12);
		gluSphere(IDquadric,6,20,20);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,26);
		gluSphere(IDquadric,8,20,20);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,26);
		gluCylinder(IDquadric,7,1,15,20,20);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,42);
		gluCylinder(IDquadric,1,3,1,20,20);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,43);
		gluCylinder(IDquadric,3,1,1,20,20);
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,0,50);
		gluSphere(IDquadric,4,20,20);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,50);
		gluCylinder(IDquadric,3.5,1,10,20,20);
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,0,62);
		gluSphere(IDquadric,3,20,20);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,65);
		gluCylinder(IDquadric,1,2,1,20,20);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,66);
		gluCylinder(IDquadric,2,0,5,20,20);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,55);
		glRotatef(90,1,0,0);
		glScalef(0.15,0.15,0.15);
		moon();
	}glPopMatrix();
	
	glColor3f(0.7,0.7,0.7);
	glPushMatrix();{
		glTranslatef(0,0,-6);
		gluCylinder(IDquadric,15,10,6,20,20);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,-10);
		gluCylinder(IDquadric,20,15,4,20,20);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,-13);
		gluCylinder(IDquadric,28,20,3,20,20);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,-14);
		gluCylinder(IDquadric,35,28,1,20,20);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,-18);
		gluCylinder(IDquadric,28,35,4,20,20);
	}glPopMatrix();
	
}
void door(){
	//glColor3f(1,1,1);
	glPushMatrix();{
		//glScalef(.333,.333,.333);
		glPushMatrix();{
			glTranslatef(.5,0,33);
			glScalef(1,20,66);
			drawUnitSolidCube();
		}glPopMatrix();
		
		glPushMatrix();{
			glTranslatef(1+39+.5,0,33);
			glScalef(1,20,66);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glTranslatef(20.5,0,.5);
			glScalef(39,20,1);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glTranslatef(20.5,0,66-.5);
			glScalef(39,20,1);
			drawUnitSolidCube();
		}glPopMatrix();

		glColor3f(0.8,0.8,0.8);
	
		//inside door design
		glPushMatrix();{
			glTranslatef(19.5+1,8,22);
			glPushMatrix();{
				glTranslatef(0,0,14.5);
				glScalef(38,1,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(-18.5,0,0);
				glScalef(1,1,30);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(18.5,0,0);
				glScalef(1,1,30);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(-9.5,0,0);
				glScalef(1,1,30);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(9.5,0,0);
				glScalef(1,1,30);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(-14,0,0);
				glScalef(1,1,30);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(14,0,0);
				glScalef(1,1,30);
				drawUnitSolidCube();
			}glPopMatrix();

			glPushMatrix();{
				glTranslatef(14,0,5);
				glScalef(9,1,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(14,0,-5);
				glScalef(9,1,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(14,0,-14.5);
				glScalef(9,1,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(-14,0,5);
				glScalef(9,1,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(-14,0,-5);
				glScalef(9,1,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(-14,0,-14.5);
				glScalef(9,1,1);
				drawUnitSolidCube();
			}glPopMatrix();
		}glPopMatrix();

		//door side
		glPushMatrix();{
			glTranslatef(39.5,-8,22);
			glPushMatrix();{
				glTranslatef(0,0,-3.5);
				glScalef(1,1,37);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,5.5,0);
				glScalef(1,1,30);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,11,0);
				glScalef(1,1,30);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,16,-3.5);
				glScalef(1,1,37);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,8,14.5);
				glScalef(1,16,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,8,5);
				glScalef(1,16,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,8,-5);
				glScalef(1,16,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,8,-14.5);
				glScalef(1,16,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,8,-20.5);
				glScalef(1,16,1);
				drawUnitSolidCube();
			}glPopMatrix();
		}glPopMatrix();

		
		glPushMatrix();{
			glTranslatef(1.5,-8,22);
			glPushMatrix();{
				glTranslatef(0,0,-3.5);
				glScalef(1,1,37);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,5.5,0);
				glScalef(1,1,30);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,11,0);
				glScalef(1,1,30);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,16,-3.5);
				glScalef(1,1,37);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,8,14.5);
				glScalef(1,16,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,8,5);
				glScalef(1,16,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,8,-5);
				glScalef(1,16,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,8,-14.5);
				glScalef(1,16,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,8,-20.5);
				glScalef(1,16,1);
				drawUnitSolidCube();
			}glPopMatrix();
		}glPopMatrix();
		//door back
		glColor3f(0.2,0.3,0.3);
		glPushMatrix();{
			glTranslatef(20.5,10,33);
			glScalef(39,1,66);
			drawUnitSolidCube();
		}glPopMatrix();

		
		glColor3f(0.9,0.9,0.9);
		glPushMatrix();{
			glTranslatef(0,0,50);
			glRotatef(90,1,0,0);
			
			double equ[4];
			equ[0]=0;
			equ[1]=-1;
			equ[2]=0;
			equ[3]=15;

			glClipPlane(GL_CLIP_PLANE0,equ);

			glEnable(GL_CLIP_PLANE0);{
				double tran=0;
				for(double i=1;i<=22;i=i+.3){
					tran=8*log(i);
					glPushMatrix();{
						glTranslatef(i,tran,0);
						glScalef(1,25,16);
						drawUnitSolidCube();
					}glPopMatrix();
					glPushMatrix();{
						glTranslatef(-i+41,tran,0);
						glScalef(1,25,16);
						drawUnitSolidCube();
					}glPopMatrix();
				}
				tran=0;
				glPushMatrix();{
				glColor3f(0.4,0.5,0.4);
				glTranslatef(20.5,15,10);
				for(double i=3;i<28;i=i+.5){
					tran=5.5*log(i);
					glPushMatrix();{
						glTranslatef(0,-i,-tran);
						glScalef(38,1,1);
						drawUnitSolidCube();
					}glPopMatrix();
				}	
				}glPopMatrix();
			};glDisable(GL_CLIP_PLANE0);
		}glPopMatrix();
	}glPopMatrix();
}
void miniDoor(){
	//glColor3f(0.9,0.2,0.2);
	glColor3f(0.2,0.2,0.2);
	glPushMatrix();{
		glPushMatrix();{
			glTranslatef(-.5,0,33);
			glScalef(3,20,66);
			drawUnitSolidCube();
		}glPopMatrix();
		glColor3f(0.2,0.2,0.2);
		glPushMatrix();{
			glTranslatef(1+39+1.5,0,33);
			glScalef(3,20,66);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glTranslatef(20.5,0,1.5);
			glScalef(39,20,3);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glTranslatef(20.5,0,66-1.5);
			glScalef(39,20,3);
			drawUnitSolidCube();
		}glPopMatrix();

		glColor3f(0.8,0.8,0.8);
	
		//inside door design
		glPushMatrix();{
			glTranslatef(19.5+1,8,22);
			glPushMatrix();{
				glTranslatef(0,0,14.5);
				glScalef(38,1,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,0,-14.5-6);
				glScalef(38,1,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(-18.5,0,-3.5);
				glScalef(1,1,37);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(18.5,0,-3.5);
				glScalef(1,1,37);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(-6,0,-3);
				glScalef(1,1,36);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(6,0,-3);
				glScalef(1,1,36);
				drawUnitSolidCube();
			}glPopMatrix();

			glPushMatrix();{
				glTranslatef(0,0,5.5);
				glScalef(12,1,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(14,0,-10.5);
				glScalef(15,1,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(-14,0,-10.5);
				glScalef(15,1,1);
				drawUnitSolidCube();
			}glPopMatrix();
		}glPopMatrix();

		//door side
		glPushMatrix();{
			glTranslatef(39.5,-8,22);
			glPushMatrix();{
				glTranslatef(0,0,-3.5);
				glScalef(1,1,37);
				drawUnitSolidCube();
			}glPopMatrix();
			
			glPushMatrix();{
				glTranslatef(0,8,14.5);
				glScalef(1,16,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,8,-10.5);
				glScalef(1,16,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,8,-20.5);
				glScalef(1,16,1);
				drawUnitSolidCube();
			}glPopMatrix();
		}glPopMatrix();

	
		glPushMatrix();{
			glTranslatef(1.5,-8,22);
			//glTranslatef(39.5,-8,22);
			glPushMatrix();{
				glTranslatef(0,0,-3.5);
				glScalef(1,1,37);
				drawUnitSolidCube();
			}glPopMatrix();
			
			glPushMatrix();{
				glTranslatef(0,8,14.5);
				glScalef(1,16,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,8,-10.5);
				glScalef(1,16,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,8,-20.5);
				glScalef(1,16,1);
				drawUnitSolidCube();
			}glPopMatrix();
		}glPopMatrix();
	
	
		//door back
		glColor3f(0.2,0.3,0.3);
		glPushMatrix();{
			glTranslatef(20.5,10,33);
			glScalef(39,1,66);
			drawUnitSolidCube();
		}glPopMatrix();

		glColor3f(0.9,0.9,0.9);
		glPushMatrix();{
			glTranslatef(0,0,50);
			glRotatef(90,1,0,0);
			
			double equ[4];
			equ[0]=0;
			equ[1]=-1;
			equ[2]=0;
			equ[3]=15;

			glClipPlane(GL_CLIP_PLANE0,equ);

			glEnable(GL_CLIP_PLANE0);{
				double tran=0;
				for(double i=1;i<=22;i=i+.3){
					tran=8*log(i);
					glPushMatrix();{
						glTranslatef(i,tran,0);
						glScalef(1,25,16);
						drawUnitSolidCube();
					}glPopMatrix();
					glPushMatrix();{
						glTranslatef(-i+41,tran,0);
						glScalef(1,25,16);
						drawUnitSolidCube();
					}glPopMatrix();
				}
				tran=0;
				glPushMatrix();{
				glColor3f(0.4,0.5,0.4);
				glTranslatef(20.5,15,10);
				for(double i=3;i<28;i=i+.5){
					tran=5.5*log(i);
					glPushMatrix();{
						glTranslatef(0,-i,-tran);
						glScalef(38,1,1);
						drawUnitSolidCube();
					}glPopMatrix();
				}	
				}glPopMatrix();
			};glDisable(GL_CLIP_PLANE0);
		}glPopMatrix();
		
	}glPopMatrix();
}
void gombuj(){
	double equ[4];
	equ[0]=0;
	equ[1]=0;
	equ[2]=1;
	equ[3]=.4;

	glClipPlane(GL_CLIP_PLANE0,equ);

	glEnable(GL_CLIP_PLANE0);{
		glColor3f(1,1,1);
		glPushMatrix();{
			gluSphere(IDquadric,1,20,20);
		}glPopMatrix();
	}glDisable(GL_CLIP_PLANE0);

	//glPushMatrix();{
	//	glTranslatef(0,0,.81);
	//	gluCylinder(IDquadric,0.58,0,.3,20,20);
	//}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,0,1.12);
		glScalef(0.01,0.01,0.01);
		gombujTop();
	}glPopMatrix();


}
void roofBigGombuj(){
	double height=29;
	glColor3f(0.7,0.7,0.7);
	glPushMatrix();{
		glTranslatef(0,0,height);
		gluCylinder(IDquadric,10,10,5,50,50);
	}glPopMatrix();	
	glColor3f(1,1,1);
	glPushMatrix();{
		glTranslatef(0,0,height+5);
		glutSolidCircle(10,50);
	}glPopMatrix();
	glColor3f(0.5,0.5,0.5);
	glPushMatrix();{
		glTranslatef(0,0,height+5);
		gluCylinder(IDquadric,9,9,4,50,50);
	}glPopMatrix();	
	glColor3f(0.3 ,0.3,0.3);
	glPushMatrix();{
		glTranslatef(0,0,height+5+4);
		gluCylinder(IDquadric,9,9,2,50,50);
	}glPopMatrix();

	glColor3f(0.7 ,0.7,0.7);
	glPushMatrix();{
		glTranslatef(0,0,height+5+4+2);
		gluCylinder(IDquadric,10,10,0.5,50,50);
	}glPopMatrix();
	glColor3f(0.8,0.8,0.8);
	glPushMatrix();{
		glTranslatef(0,0,height+5+4+2);
		glutSolidCircle(10,50);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,height+5+4+2+.5);
		glutSolidCircle(10,50);
	}glPopMatrix();
	//gombuj
	glPushMatrix();{
		glTranslatef(0,0,height+15);
		glScalef(9,9,9);
		gombuj();
	}glPopMatrix();

}


//me
void drawGrid(bool canDrawGrid)
{
	int i;

	//WILL draw grid IF the "canDrawGrid" is true:

	if(canDrawGrid == true){
			//grey
		glBegin(GL_LINES);{
			for(i=-100;i<=100;i++){

				if(i==0)
					continue;	//SKIP the MAIN axes
				if(i%10==0)glColor3f(0.7, 0.7, 0.7);
				else glColor3f(0.3, 0.3, 0.3);
				//lines parallel to Y-axis
				glVertex3f(i*1, -100, 0);
				glVertex3f(i*1,  100, 0);

				//lines parallel to X-axis
				glVertex3f(-100, i*1, 0);
				glVertex3f( 100, i*1, 0);
			}
		}glEnd();
	}

	// draw the two AXES
	glColor3f(1, 1, 1);	//100% white
	glBegin(GL_LINES);{
		//Y axis

		glVertex3f(0, -150, 0);	// intentionally extended to -150 to 150, no big deal
		glVertex3f(0,  150, 0);

		//X axis
		glVertex3f(-150, 0, 0);
		glVertex3f( 150, 0, 0);
	}glEnd();
}

void drawUnitSolidCube(){
	glPushMatrix();{

		glTranslatef(-.5,-.5,-.5);
		
		glBegin(GL_QUADS);{
			glVertex3f(0,0,0);
			glVertex3f(1,0,0);
			glVertex3f(1,1,0);
			glVertex3f(0,1,0);
		}glEnd();
		glBegin(GL_QUADS);{
			glVertex3f(0,0,0);
			glVertex3f(1,0,0);
			glVertex3f(1,0,1);
			glVertex3f(0,0,1);
		}glEnd();
		
		glBegin(GL_QUADS);{
			glVertex3f(0,0,0);
			glVertex3f(0,1,0);
			glVertex3f(0,1,1);
			glVertex3f(0,0,1);
		}glEnd();
		//glColor3f(0,1,0);
		glBegin(GL_QUADS);{
			glVertex3f(0,1,0);
			glVertex3f(1,1,0);
			glVertex3f(1,1,1);
			glVertex3f(0,1,1);
		}glEnd();
		//glColor3f(0,0,1);
		glBegin(GL_QUADS);{
			glVertex3f(0,0,1);
			glVertex3f(1,0,1);
			glVertex3f(1,1,1);
			glVertex3f(0,1,1);
		}glEnd();
		//glColor3f(1,0,0);
		glBegin(GL_QUADS);{
			glVertex3f(1,0,0);
			glVertex3f(1,1,0);
			glVertex3f(1,1,1);
			glVertex3f(1,0,1);
		}glEnd();

	};glPopMatrix();
}
void drawCameraTest(){
	glPushMatrix();{		
			glColor4f(0, .7 ,.9,.8);
			glTranslatef(0,-5,0);
			//glRotatef(180,0,1,0);
			glutSolidCone(1,.5,50,50);		
	}glPopMatrix();

	glPushMatrix();{		
			glColor4f(0, .7 ,.9,.8);
			glTranslatef(0,0,0);
			glutSolidCube(3);		
	}glPopMatrix();

	glPushMatrix();{		
			glColor4f(0, .7 ,.9,.8);
			glTranslatef(0,5,0);
			glutSolidSphere(2.5,50,50);		
	}glPopMatrix();

}
void drawGombuj(){
	glPushMatrix();{
		glTranslatef(0,0,1.5246+1.8376+.1661*2);
		glScalef(9.4403/2,9.4403/2,9.4403/2);
		glTranslatef(0,0,.4);
		gombuj();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.8,0,0);
		glTranslatef(0,0,1.5246+1.8376+.1661*2);
		gluDisk(IDquadric,0,9/2,50,50);
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.8,0,0);
		glTranslatef(0,0,1.5246+1.8376+.1661);
		gluCylinder(IDquadric,9.3/2,9/2,.1661,50,50);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,1.5246+1.8376);
		gluCylinder(IDquadric,9/2,9.3/2,.1661,50,50);
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.8,0,0);
		glTranslatef(0,0,1.8376);
		gluCylinder(IDquadric,9/2,9/2,1.5246,50,50);
	}glPopMatrix();
	
	glPushMatrix();{
		//glTranslatef(0,0,2.7843);
		//glScalef(9.4403/2,9.4403/2,9.4403/2);
		glColor3f(.5,0,0);
		gluCylinder(IDquadric,9/2,9/2,1.8376,50,50);
	}glPopMatrix();
}
void drawBigGombuj(){
	glPushMatrix();{
		glTranslatef(0,0,2.5+2.7843+.2519*2);
		glScalef(14.3035/2,14.3035/2,14.3035/2);
		glTranslatef(0,0,.4);
		gombuj();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.8,0,0);
		glTranslatef(0,0,2.7248+2.7843+.2519*2);
		gluDisk(IDquadric,0,13.9/2,50,50);
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.8,0,0);
		glTranslatef(0,0,2.7248+2.7843+.2519);
		gluCylinder(IDquadric,14.2/2,13.9/2,.2519,50,50);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,2.7248+2.7843);
		gluCylinder(IDquadric,13.9/2,14.2/2,.2519,50,50);
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.8,0,0);
		glTranslatef(0,0,2.7843);
		gluCylinder(IDquadric,13.9/2,13.9/2,2.7248,50,50);
	}glPopMatrix();
	
	glPushMatrix();{
		//glTranslatef(0,0,2.7843);
		//glScalef(9.4403/2,9.4403/2,9.4403/2);
		glColor3f(.5,0,0);
		gluCylinder(IDquadric,13.9/2,13.9/2,2.7843,50,50);
	}glPopMatrix();
}
void drawRedMinarSide(){
	glPushMatrix();{
		glScalef(1.47,.5,.33);
		drawUnitSolidCube();
	};glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,.5,.5);
		glTranslatef(0,0,.33/2+.352/2);
		glScalef(1.47,.5,.352);
		drawUnitSolidCube();
	};glPopMatrix();

	
	glPushMatrix();{
		glColor3f(0,.7,.3);
		glTranslatef(1.47/2-.2024/2,0,.517+1.2121/2);
		glScalef(.2024,.5,1.2121);
		drawUnitSolidCube();
	};glPopMatrix();

	glPushMatrix();{
		glColor3f(0,.7,.3);
		glTranslatef(-1.47/2+.2024/2,0,.517+1.2121/2);
		glScalef(.2024,.5,1.2121);
		drawUnitSolidCube();
	};glPopMatrix();
	
	glPushMatrix();{
		glColor3f(0,.7,0);
		glTranslatef(0,0,1.7291+.352/2);
		glScalef(1.47,.5,.352);
		drawUnitSolidCube();
	};glPopMatrix();

	glPushMatrix();{
		glColor3f(1,0,0);
		glTranslatef(0,-.125,.517+1.25/2);
		glScalef(1.1,.25,1.25);
		drawUnitSolidCube();
	};glPopMatrix();


	glPushMatrix();{
		glTranslatef(0,0,2.0811+17.173/2);
		glScalef(1.47,.5,17.173);
		drawUnitSolidCube();
	};glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,.9);
		//glTranslatef(.5/2,-3.1018/2,2.2461+.3282+15.1449+.5129/2);
		glTranslatef(0,0,2.0811+17.173+.2118/2);
		glScalef(1.47,.5+.1358*2,.2118);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(1,1,1);
		//glTranslatef(.5/2,-3.1018/2,2.2461+.3282+15.1449+.5129/2);
		glTranslatef(0,0,2.0811+17.173+.2118+.1/2);
		glScalef(1.47,.5,.1);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(1,1,1);
		//glTranslatef(.5/2,-3.1018/2,2.2461+.3282+15.1449+.5129/2);
		glTranslatef(0,0,2.0811+17.173+.2118+.1+1.1748/2);
		glScalef(1.47,.5,1.1748);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(1,1,1);
		//glTranslatef(.5/2,-3.1018/2,2.2461+.3282+15.1449+.5129/2);
		glTranslatef(0,0,2.0811+17.173+.2118+.1+1.1748+.1573/2);
		glScalef(1.47,.5+.0524*2,.1573);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.8,0,0);
		//glTranslatef(.5/2,-3.1018/2,2.2461+.3282+15.1449+.5129/2);
		glTranslatef(0,0,2.0811+17.173+.2118+.1+1.1748+.1573+.4577/2);
		glScalef(1.47,.5,.4577);
		drawUnitSolidCube();
	}glPopMatrix();


	glPushMatrix();{
		glColor3f(.8,0,0);
		//glTranslatef(.5/2,-3.1018/2,2.2461+.3282+15.1449+.5129/2);
		glTranslatef(0,0,2.0811+17.173+.2118+.1+1.1748+.1573+.4577+.1005/2);
		glScalef(1.8,.2458+.8659,.1005);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.8,0,0);
		//glTranslatef(.5/2,-3.1018/2,2.2461+.3282+15.1449+.5129/2);
		glTranslatef(0,0,2.0811+17.173+.2118+.1+1.1748+.1573+.4577+.1005+.5725/2);
		glScalef(1.47,.25,.5725);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.8,0,0);
		//glTranslatef(.5/2,-3.1018/2,2.2461+.3282+15.1449+.5129/2);
		glTranslatef(0,0,2.0811+17.173+.2118+.1+1.1748+.1573+.4577+.1005+.5741/2);
		glScalef(1.47,.5,.5741);
		drawUnitSolidCube();
	}glPopMatrix();
	glPushMatrix();{
		glColor3f(.8,0,0);
		//glTranslatef(.5/2,-3.1018/2,2.2461+.3282+15.1449+.5129/2);
		glTranslatef(0,0,2.0811+17.173+.2118+.1+1.1748+.1573+.4577+.1005+.5741+.2691/2);
		glScalef(1.47,.5,.2691);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.8,0,0);
		//glTranslatef(.5/2,-3.1018/2,2.2461+.3282+15.1449+.5129/2);
		glTranslatef(1.47/2-.1862/2,0,2.0811+17.173+.2118+.1+1.1748+.1573+.4577+.1005+.5741+.2691+.2287/2);
		glScalef(.1862,.5,.2287);
		drawUnitSolidCube();
	}glPopMatrix();
	glPushMatrix();{
		glColor3f(.8,0,0);
		//glTranslatef(.5/2,-3.1018/2,2.2461+.3282+15.1449+.5129/2);
		glTranslatef(-1.47/2+.1862/2,0,2.0811+17.173+.2118+.1+1.1748+.1573+.4577+.1005+.5741+.2691+.2287/2);
		glScalef(.1862,.5,.2287);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.8,0,0);
		//glTranslatef(.5/2,-3.1018/2,2.2461+.3282+15.1449+.5129/2);
		glTranslatef(-1.47/2,0,2.0811+17.173+.2118+.1+1.1748+.1573+.4577+.1005+.5741+.2691+.2287);
		
		gluCylinder(IDquadric,.1453,.1453,1.3318,50,50);
	}glPopMatrix();
	
	glPushMatrix();{
		glColor3f(.8,0,0);
		//glTranslatef(.5/2,-3.1018/2,2.2461+.3282+15.1449+.5129/2);
		glTranslatef(1.47/2,0,2.0811+17.173+.2118+.1+1.1748+.1573+.4577+.1005+.5741+.2691+.2287);
		gluCylinder(IDquadric,.1453,.1453,1.3318,50,50);
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.8,0,0);
		//glTranslatef(.5/2,-3.1018/2,2.2461+.3282+15.1449+.5129/2);
		glTranslatef(1.47/2-.1862/2,0,2.0811+17.173+.2118+.1+1.1748+.1573+.4577+.1005+.5741+.2691+.2287+1.3318+.7968/2);
		glScalef(.1862,.5,.7968);
		drawUnitSolidCube();
	}glPopMatrix();
	glPushMatrix();{
		glColor3f(.8,0,0);
		//glTranslatef(.5/2,-3.1018/2,2.2461+.3282+15.1449+.5129/2);
		glTranslatef(-1.47/2+.1862/2,0,22.5281+1.3318+.7968/2);
		glScalef(.1862,.5,.7968);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.8,0,0);
		//glTranslatef(.5/2,-3.1018/2,2.2461+.3282+15.1449+.5129/2);
		glTranslatef(0,0,22.5281+1.3318+.7968+.1818/2);
		glScalef(1.47,.5,.1818);
		drawUnitSolidCube();
	}glPopMatrix();
	glPushMatrix();{
		glColor3f(.8,0,0);
		//glTranslatef(.5/2,-3.1018/2,2.2461+.3282+15.1449+.5129/2);
		glTranslatef(0,0,22.5281+1.3318+.7968+.1818+.1005/2);
		glScalef(2.6,1.9939+.8659,.1005);
		drawUnitSolidCube();
	}glPopMatrix();

	

}
void drawRedMinar(){
	glPushMatrix();{
		glColor3f(1,1,1);
		glTranslatef(.25,0,.33/2);
		glRotatef(90,0,0,1);
		drawRedMinarSide();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(1,1,1);
		glTranslatef(.43+.25,-1.1,.33/2);
		glRotatef(-45,0,0,1);
		drawRedMinarSide();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(1,1,1);
		glTranslatef(.735+1.039,-1.7744+.25,.33/2);
		drawRedMinarSide();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(1,1,1);
		glTranslatef(.25+1.47*sin(45.0)+1.47-.2,-1.47/2-(1.47/2)*sin(45.0)+.25,.33/2);
		glRotatef(45,0,0,1);
		drawRedMinarSide();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(1,1,1);
		glTranslatef(.25+1.47*sin(45.0)+1.47+.25,0,.33/2);
		glRotatef(90,0,0,1);
		drawRedMinarSide();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(1,1,1);
		glTranslatef(.25+1.47*sin(45.0)+1.47-.2,1.47/2+(1.47/2)*sin(45.0)-.25,.33/2);
		glRotatef(-45,0,0,1);
		drawRedMinarSide();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(1,1,1);
		glTranslatef(.735+1.039,1.7744-.25,.33/2);
		drawRedMinarSide();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(1,1,1);
		glTranslatef(.43+.25,1.1,.33/2);
		glRotatef(45,0,0,1);
		drawRedMinarSide();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(1.1+.5,0,2.0811+17.173+.2118+.1+1.1748+.1573+.4577+.1005+.5741);
		gluDisk(IDquadric,0,1.1,50,50);
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(1.1+.5,0,22.5281+1.3318+.7968+.1818+.1005+.2);
		//glScalef(.5,.5,.5);
		//drawGombuj();
		glScalef(2.25,2.25,2.25);
		glTranslatef(0,0,.4);
		gombuj();
	}glPopMatrix();

}
void drawRedNarrowFrontWall(){
	
	//-1.47*sin(45.0)-1.47/2+.25

	//a
	glPushMatrix();{
		glColor3f(1,1,1);
		glTranslatef(2.77/2,-3.4685/2,.33/2);
		glScalef(2.7700,3.4685,.33);
		drawUnitSolidCube();
	}glPopMatrix();

	//b
	glPushMatrix();{
		glColor3f(.5,.5,.5);
		glTranslatef(.5/2,-3.4685/2,.33+.352/2);
		glScalef(.5,3.4685,.352);
		drawUnitSolidCube();
	}glPopMatrix();

	//c
	glPushMatrix();{
		glColor3f(.5,.5,.5);
		glTranslatef(.5/2,-.2947/2,.33+.352+1.2121/2);
		glScalef(.5,.2947,1.2121);
		drawUnitSolidCube();
	}glPopMatrix();

	//d
	glPushMatrix();{
		glColor3f(.5,.5,.5);
		glTranslatef(.5/2,-.2947/2-2.4805-.2947,.33+.352+1.2121/2);
		glScalef(.5,.2947,1.2121);
		drawUnitSolidCube();
	}glPopMatrix();

	//e
	glPushMatrix();{
		glColor3f(.5,.5,.5);
		glTranslatef(.5/2,-3.1018/2,.33+.352+1.2121+.352/2);
		glScalef(.5,3.1018,.352);
		drawUnitSolidCube();
	}glPopMatrix();

	

	//j
	glPushMatrix();{
		glColor3f(.8,.8,.8);
		glTranslatef(.5/2+.15,-2.49/2-.2947,.33+.352+1.22/2);
		glScalef(.5,2.49,1.22);
		drawUnitSolidCube();
	}glPopMatrix();

	//f
	glPushMatrix();{
		glColor3f(.8,.8,.8);
		glTranslatef(.5/2,-3.1018/2,2.2461+.3282/2);
		glScalef(.5,3.1018,.3282);
		drawUnitSolidCube();
	}glPopMatrix();


	//g
	glPushMatrix();{
		glColor3f(.8,.8,.8);
		glTranslatef(.5/2,-.2947/2,2.2461+.3282+15.145/2);
		glScalef(.5,.2947,15.145);
		drawUnitSolidCube();
	}glPopMatrix();
	
	//h
	glPushMatrix();{
		glColor3f(.8,.8,.8);
		glTranslatef(.5/2,-.3241/2-2.4805-.2947,2.2461+.3282+15.1449/2);
		glScalef(.5,.3241,15.145);
		drawUnitSolidCube();
	}glPopMatrix();

	//i
	glPushMatrix();{
		glColor3f(1,1,1);
		glTranslatef(.5/2,-3.1018/2,2.2461+.3282+15.1449+.5129/2);
		glScalef(.5,3.1018,.5129);
		drawUnitSolidCube();
	}glPopMatrix();

	//k
	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2+.15,-2.485/2-.2947,2.2461+.3282+15.145/2);
		glScalef(.5,2.485,15.145);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2+.2,-3.1018/2,2.2461+.3282+15.1449+.5129+1.1914/2);
		glScalef(.5,3.1018,1.1914);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.3,0,0);
		glTranslatef(.5/2,-3.1018/2,2.2461+.3282+15.1449+.5129+1.1914+.2118/2);
		glScalef(.5,3.1018,.2118);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2+.1,-3.1018/2,2.2461+.3282+15.1449+.5129+1.1914+.2118+.1/2);
		glScalef(.5,3.1018,.1);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2+.15,-3.1018/2,2.2461+.3282+15.1449+.5129+1.1914+.2118+.1+1.1748/2);
		glScalef(.5,3.1018,1.1748);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-3.1018/2,2.2461+.3282+15.1449+.5129+1.1914+.2118+.1+1.1748+.1573/2);
		glScalef(.5,3.1018,.1573);
		drawUnitSolidCube();
	}glPopMatrix();
}
void drawRedThinMinarRing(){
	glPushMatrix();{
		glTranslatef(0,0,.5089+.3157+21.6996);
		GLUquadricObj *quadratic;
		quadratic=gluNewQuadric();
		gluDisk(quadratic,0,.405,8,50);	
	}glPopMatrix();


	glPushMatrix();{
		glTranslatef(0,0,.5089+.3157+21.6996);
		GLUquadricObj *quadratic;
		quadratic=gluNewQuadric();
		gluCylinder(quadratic,.405,.455,.1,8,50);	
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,0,.5089+.3157+21.6996+.1);
		GLUquadricObj *quadratic;
		quadratic=gluNewQuadric();
		gluCylinder(quadratic,.455,.405,.1,8,50);	
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,0,.5089+.3157+21.6996+.1+.1);
		GLUquadricObj *quadratic;
		quadratic=gluNewQuadric();
		gluDisk(quadratic,0,.405,8,50);	
	}glPopMatrix();
}
void drawRedThinMinar(){
	glColor3f(.8,0,0);
	

	glPushMatrix();{		
		GLUquadricObj *quadratic;
		quadratic=gluNewQuadric();
		gluCylinder(quadratic,.3865,.3865,.5089,8,50);	
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,0,.5089);
		GLUquadricObj *quadratic;
		quadratic=gluNewQuadric();
		gluDisk(quadratic,0,.3865,8,50);	
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,0,.5089);
		GLUquadricObj *quadratic;
		quadratic=gluNewQuadric();
		gluCylinder(quadratic,.3365,.32,.3157,8,50);	
	}glPopMatrix();

	glColor3f(1,1,1);
	glPushMatrix();{
		glTranslatef(0,0,.5089+.3157);
		GLUquadricObj *quadratic;
		quadratic=gluNewQuadric();
		gluCylinder(quadratic,.32,.32,21.6996,8,50);	
	}glPopMatrix();


	//making of the torus on minar
	glColor3f(.8,0,0);
	drawRedThinMinarRing();
	
	glPushMatrix();{
		
		glTranslatef(0,0,-2.816);
		drawRedThinMinarRing();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,0,-2.816-4.6716*1);
		drawRedThinMinarRing();
	}glPopMatrix();
	
	glPushMatrix();{
		glTranslatef(0,0,-2.816-4.6716*2);
		drawRedThinMinarRing();
	}glPopMatrix();
	
	glPushMatrix();{
		glTranslatef(0,0,-2.816-4.6716*3);
		drawRedThinMinarRing();
	}glPopMatrix();

	glColor3f(.5,0,0);

	glPushMatrix();{
		glTranslatef(0,0,.5089+.3157+21.6996+.1+.1);
		GLUquadricObj *quadratic;
		quadratic=gluNewQuadric();
		gluCylinder(quadratic,.385,.485,.8,8,50);	
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,0,.5089+.3157+21.6996+.1+.1+1);
		GLUquadricObj *quadratic;
		quadratic=gluNewQuadric();
		gluSphere(quadratic,.5,50,50);	
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,0,24.3);
		glScalef(.01,.01,.01);
		gombujTop();
	}glPopMatrix();

}
void drawRedWideFrontWall(){
	glPushMatrix();{
		glTranslatef(2.7702/2,-8.8215/2,.33/2);
		glScalef(2.7702,8.8215,.33);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,-1.5960/2,.33+.352/2);
		glScalef(.5,1.5960,.352);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,-.3372/2,.33+.352+1.2121/2);
		glScalef(.5,.3372,1.2121);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,.3372/2-1.5960,.33+.352+1.2121/2);
		glScalef(.5,.3372,1.2121);
		drawUnitSolidCube();
	}glPopMatrix();
	
	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,-1.5960/2,.33+.352+1.2121+.352/2);
		glScalef(.5,1.5960,.352);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-1.5960/2,2.2461+.3282/2);
		glScalef(.5,1.5960,.3282);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-.4424/2,2.2461+.3282+15.145/2);
		glScalef(.5,.4424,15.145);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,.4424/2-1.5960,2.2461+.3282+6.5687/2);
		glScalef(.5,.4424,6.5687);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-6.5436/2-1.5960+.4424,2.2461+.3282+6.5687+.5012/2);
		glScalef(.5,6.5436,.5012);
		drawUnitSolidCube();
	}glPopMatrix();

	//right design
	glPushMatrix();{
		glTranslatef(0,-8.8215+1.5960,0);
		glPushMatrix();{
			glColor3f(.7,0,0);
			glTranslatef(.5/2,-1.5960/2,.33+.352/2);
			glScalef(.5,1.5960,.352);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glColor3f(.7,0,0);
			glTranslatef(.5/2,-.3372/2,.33+.352+1.2121/2);
			glScalef(.5,.3372,1.2121);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glColor3f(.7,0,0);
			glTranslatef(.5/2,.3372/2-1.5960,.33+.352+1.2121/2);
			glScalef(.5,.3372,1.2121);
			drawUnitSolidCube();
		}glPopMatrix();
	
		glPushMatrix();{
			glColor3f(.7,0,0);
			glTranslatef(.5/2,-1.5960/2,.33+.352+1.2121+.352/2);
			glScalef(.5,1.5960,.352);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glColor3f(.5,0,0);
			glTranslatef(.5/2,-1.5960/2,2.2461+.3282/2);
			glScalef(.5,1.5960,.3282);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glColor3f(.5,0,0);
			glTranslatef(.5/2,.4424/2-1.5960,2.2461+.3282+15.145/2);
			glScalef(.5,.4424,15.145);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glColor3f(.5,0,0);
			glTranslatef(.5/2,-.4424/2,2.2461+.3282+6.5687/2);
			glScalef(.5,.4424,6.5687);
			drawUnitSolidCube();
		}glPopMatrix();
	}glPopMatrix();


	//upper surfaces
	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-8.8215/2,2.2461+.3282+15.1449+.5129/2);
		glScalef(.5,8.8215,.5129);
		drawUnitSolidCube();
	}glPopMatrix();
	
	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2+.2,-8.8215/2,2.2461+.3282+15.1449+.5129+1.1914/2);
		glScalef(.5,8.8215,1.1914);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.3,0,0);
		glTranslatef(.5/2,-8.8215/2,2.2461+.3282+15.1449+.5129+1.1914+.2118/2);
		glScalef(.5,8.8215,.2118);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2+.1,-8.8215/2,2.2461+.3282+15.1449+.5129+1.1914+.2118+.1/2);
		glScalef(.5,8.8215,.1);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2+.15,-8.8215/2,2.2461+.3282+15.1449+.5129+1.1914+.2118+.1+1.1748/2);
		glScalef(.5,8.8215,1.1748);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-8.8215/2,2.2461+.3282+15.1449+.5129+1.1914+.2118+.1+1.1748+.1573/2);
		glScalef(.5,8.8215,.1573);
		drawUnitSolidCube();
	}glPopMatrix();

	//deeper back surface

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2+.25,-7.9/2-.45,9.6125+8.1/2);
		glScalef(.5,7.9,8.1);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2+.25,-.7/2-.45,2.5425+7.1/2);
		glScalef(.5,.7,7.1);
		drawUnitSolidCube();
	}glPopMatrix();
	
	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2+.25,-.7/2-.45-7.2,2.5425+7.1/2);
		glScalef(.5,.7,7.1);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.8,.8,.8);
		glTranslatef(.5/2+.15,-1.2/2-.2,.33+.352+1.22/2);
		glScalef(.5,1.2,1.22);
		drawUnitSolidCube();
	}glPopMatrix();


	glPushMatrix();{
		glColor3f(.8,.8,.8);
		glTranslatef(.5/2+.15,-1.2/2-.2-7.2,.33+.352+1.22/2);
		glScalef(.5,1.2,1.22);
		drawUnitSolidCube();
	}glPopMatrix();

	//window design

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-6.5436/2-1.5960+.4424,2.2461+.3282+6.5687+.5012+.8527+.6/2);
		glScalef(.5,6.5436,.6);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,.4424/2-1.5960,2.2461+.3282+6.5687+.5012+.8527+.6+5.4691/2);
		glScalef(.5,.4424,5.4691);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,.4424*1.5-1.5960-6.5436,2.2461+.3282+6.5687+.5012+.8527+.6+5.4691/2);
		glScalef(.5,.4424,5.4691);
		drawUnitSolidCube();
	}glPopMatrix();
	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-6.5436/2-1.5960+.4424,2.2461+.3282+6.5687+.5012+.8527+.6+5.4691+.6/2);
		glScalef(.5,6.5436,.6);
		drawUnitSolidCube();
	}glPopMatrix();
}
void drawRedBigFrontGate(){
	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(5.4637/2,-21.5350/2,.33/2);
		glScalef(5.4637,21.5350,.33);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,-4.1461/2,.33+.352/2);
		glScalef(.5,4.1461,.352);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,-.3372/2,.33+.352+1.2121/2);
		glScalef(.5,.3372,1.2121);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,.3372/2-4.1461,.33+.352+1.2121/2);
		glScalef(.5,.3372,1.2121);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,-4.1461/2,.33+.352+1.2121+.1940/2);
		glScalef(.5,4.1461,.1940);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,-3.8543/2,.33+.352+1.2121+.1940+.2943/2);
		glScalef(.5,3.8543,.2943);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,-.3/2,.33+.352+1.2121+.1940+.2943+19.3367/2);
		glScalef(.5,.3,19.3367);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,.3/2-3.8543,.33+.352+1.2121+.1940+.2943+16.0824/2);
		glScalef(.5,.3,16.0824);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,-2.4541/2-.7,.33+.352+1.2121+.1940+.2943+.4+.3/2);
		glScalef(.5,2.4541,.3);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,-.3/2-.7,.33+.352+1.2121+.1940+.2943+.4+.3+17.9367/2);
		glScalef(.5,.3,17.9367);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,.3/2-.7-2.4541,.33+.352+1.2121+.1940+.2943+.4+.3+15.7824/2);
		glScalef(.5,.3,15.7824);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,-14.4265/2-3.8543+.3,.33+.352+1.2121+.1940+.2943+16.0824+.3/2-.3);
		glScalef(.5,14.4265,.3);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,-15.8265/2-.7-2.4541+.3,.33+.352+1.2121+.1940+.2943+16.0824+.4+.3/2);
		glScalef(.5,15.8265,.3);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,-20.1348/2-.7,.33+.352+1.2121+.1940+.2943+.4+.3+17.94+.3/2);
		glScalef(.5,20.1348,.3);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,-21.5350/2,.33+.352+1.2121+.1940+.2943+19.3367+.3/2);
		glScalef(.5,21.5350,.3);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,-17.3890,0);
		
		glPushMatrix();{
			glColor3f(.7,0,0);
			glTranslatef(.5/2,-4.1461/2,.33+.352/2);
			glScalef(.5,4.1461,.352);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glColor3f(.7,0,0);
			glTranslatef(.5/2,-.3372/2,.33+.352+1.2121/2);
			glScalef(.5,.3372,1.2121);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glColor3f(.7,0,0);
			glTranslatef(.5/2,.3372/2-4.1461,.33+.352+1.2121/2);
			glScalef(.5,.3372,1.2121);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glColor3f(.7,0,0);
			glTranslatef(.5/2,-4.1461/2,.33+.352+1.2121+.1940/2);
			glScalef(.5,4.1461,.1940);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glColor3f(.7,0,0);
			glTranslatef(.5/2,-3.8543/2-.2918,.33+.352+1.2121+.1940+.2943/2);
			glScalef(.5,3.8543,.2943);
			drawUnitSolidCube();
		}glPopMatrix();
		glPushMatrix();{
			glColor3f(.7,0,0);
			glTranslatef(.5/2,.3/2-3.8543-.2918,.33+.352+1.2121+.1940+.2943+19.3367/2);
			glScalef(.5,.3,19.3367);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glColor3f(.7,0,0);
			glTranslatef(.5/2,-.3/2-.2918,.33+.352+1.2121+.1940+.2943+16.0824/2);
			glScalef(.5,.3,16.0824);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glColor3f(.7,0,0);
			glTranslatef(.5/2,-2.4541/2-.7-.2918,.33+.352+1.2121+.1940+.2943+.4+.3/2);
			glScalef(.5,2.4541,.3);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glColor3f(.7,0,0);
			glTranslatef(.5/2,.3/2-.7-.2918-2.4541,.33+.352+1.2121+.1940+.2943+.4+.3+17.9367/2);
			glScalef(.5,.3,17.9367);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glColor3f(.7,0,0);
			glTranslatef(.5/2,-.3/2-.7-.2918,.33+.352+1.2121+.1940+.2943+.4+.3+15.7824/2);
			glScalef(.5,.3,15.7824);
			drawUnitSolidCube();
		}glPopMatrix();

	}glPopMatrix();

	//upper surface
	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-21.5350/2,.33+.352+1.2121+.1940+.2943+19.3367+.3+.5129/2);
		glScalef(.5,21.5350,.5129);
		drawUnitSolidCube();
	}glPopMatrix();
	
	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2+.2,-21.5350/2,.33+.352+1.2121+.1940+.2943+19.3367+.3+.5129+1.1914/2);
		glScalef(.5,21.5350,1.1914);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.3,0,0);
		glTranslatef(.5/2,-21.5350/2,.33+.352+1.2121+.1940+.2943+19.3367+.3+.5129+1.1914+.2118/2);
		glScalef(.5,21.5350,.2118);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2+.1,-21.5350/2,.33+.352+1.2121+.1940+.2943+19.3367+.3+.5129+1.1914+.2118+.1/2);
		glScalef(.5,21.5350,.1);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2+.15,-21.5350/2,.33+.352+1.2121+.1940+.2943+19.3367+.3+.5129+1.1914+.2118+.1+1.1748/2);
		glScalef(.5,21.5350,1.1748);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-21.5350/2,.33+.352+1.2121+.1940+.2943+19.3367+.3+.5129+1.1914+.2118+.1+1.1748+.1573/2);
		glScalef(.5,21.5350,.1573);
		drawUnitSolidCube();
	}glPopMatrix();


	//deeper back surface

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2+.25,-3.77/2-.1940,.33+.352+1.22/2);
		glScalef(.5,3.77,1.22);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2+.25,-3.77/2-.1940-17.389,.33+.352+1.22/2);
		glScalef(.5,3.77,1.22);
		drawUnitSolidCube();
	}glPopMatrix();
	
	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2+.25,-2.59/2-.7,.33+.352+1.2121+.1940+.2943+.5/2);
		glScalef(.5,2.59,.5);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2+.25,-2.59/2-.7-17.6808,.33+.352+1.2121+.1940+.2943+.5/2);
		glScalef(.5,2.59,.5);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2+.25,-.5/2-.1940,.33+.352+1.2121+.1940+.2943+19.3367/2);
		glScalef(.5,.5,19.3367);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2+.25,-.5/2-.1940-2.8543,.33+.352+1.2121+.1940+.2943+16.4824/2);
		glScalef(.5,.5,16.4824);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2+.25,-20.2/2-.7,.33+.352+1.2121+.1940+.2943+.4+.3+17.9367+.4+.3/2);
		glScalef(.5,20.2,.5);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2+.25,-14.4265/2-3.5545,.33+.352+1.2121+.1940+.2943+16.4824-.4+.5/2);
		glScalef(.5,14.4265,.5);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2+.25,-.5/2-.45-.1940-20.2,.33+.352+1.2121+.1940+.2943+19.3367/2);
		glScalef(.5,.5,19.3367);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2+.25,-.5/2-.1940-2.8543-14.8265,.33+.352+1.2121+.1940+.2943+16.4824/2);
		glScalef(.5,.5,16.4824);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2+.25,-.33/2-3.8543,.33+.352+1.2121+.1940+16.0824/2);
		glScalef(.5,.33,16.0824);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2+.25,-.33/2-3.8543-13.5098,.33+.352+1.2121+.1940+16.0824/2);
		glScalef(.5,.33,16.0824);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2+.25,-13.2429/2-3.8543-.33,.33+.352+1.2121+.1940+16.0824-.2918/2);
		glScalef(.5,13.2429,.2918);
		drawUnitSolidCube();
	}glPopMatrix();

	//back white surface

	glPushMatrix();{
		glColor3f(1,1,1);
		glTranslatef(.5/2+.2,-1.95/2-1,.33+.352+1.2121+.1940+.2943+.7+17.9367/2);
		glScalef(.5,1.95,17.9367);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(1,1,1);
		glTranslatef(.5/2+.2,-1.95/2-1-17.6806,.33+.352+1.2121+.1940+.2943+.7+17.9367/2);
		glScalef(.5,1.95,17.9367);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(1,1,1);
		glTranslatef(.5/2+.2,-15.83/2-2.8543,.33+.352+1.2121+.1940+.2943+.7+16.0824+1.88/2);
		glScalef(.5,15.83,1.88);
		drawUnitSolidCube();
	}glPopMatrix();
}
void drawRedThinMinarTall(){
	glColor3f(.8,0,0);
	

	glPushMatrix();{		
		GLUquadricObj *quadratic;
		quadratic=gluNewQuadric();
		gluCylinder(quadratic,.3865,.3865,.5089,8,50);	
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,0,.5089);
		GLUquadricObj *quadratic;
		quadratic=gluNewQuadric();
		gluDisk(quadratic,0,.3865,8,50);	
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,0,.5089);
		GLUquadricObj *quadratic;
		quadratic=gluNewQuadric();
		gluCylinder(quadratic,.3365,.32,.3157,8,50);	
	}glPopMatrix();

	glColor3f(1,1,1);
	glPushMatrix();{
		glTranslatef(0,0,.5089+.3157);
		GLUquadricObj *quadratic;
		quadratic=gluNewQuadric();
		gluCylinder(quadratic,.32,.32,26.6167,8,50);	
	}glPopMatrix();


	//making of the torus on minar
	glColor3f(.8,0,0);
	drawRedThinMinarRing();
	
	glPushMatrix();{
		
		glTranslatef(0,0,-2.816);
		drawRedThinMinarRing();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,0,-2.816-4.6716*1);
		drawRedThinMinarRing();
	}glPopMatrix();
	
	glPushMatrix();{
		glTranslatef(0,0,-2.816-4.6716*2);
		drawRedThinMinarRing();
	}glPopMatrix();
	
	glPushMatrix();{
		glTranslatef(0,0,-2.816-4.6716*3);
		drawRedThinMinarRing();
	}glPopMatrix();

	glColor3f(.5,0,0);

	glPushMatrix();{
		glTranslatef(0,0,.5089+.3157+26.6167+.1+.1);
		GLUquadricObj *quadratic;
		quadratic=gluNewQuadric();
		gluCylinder(quadratic,.385,.485,.8,8,50);	
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,0,.5089+.3157+26.6167+.1+.1+1);
		GLUquadricObj *quadratic;
		quadratic=gluNewQuadric();
		gluSphere(quadratic,.5,50,50);	
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,0,4.917);
		drawRedThinMinarRing();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,0,24.3+(26.6167-21.6996));
		glScalef(.01,.01,.01);
		gombujTop();
	}glPopMatrix();
}

void drawRedSideWallDesignedPart(){
	glColor3f(.5,0,0);
	glPushMatrix();{
		glTranslatef(2.67/2,-5.0819/2,.33/2);
		glScalef(2.67,5.0819,.33);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,-.8986/2,.33+.352/2);
		glScalef(.5,.8986,.352);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,-.1795/2,.33+.352+1.2121/2);
		glScalef(.5,.1795,1.2121);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,-.1218/2-.7766,.33+.352+1.2121/2);
		glScalef(.5,.1218,1.2121);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,-.8986/2,.33+.352+1.2121+.352/2);
		glScalef(.5,.8986,.352);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-.8986/2,.33+.352+1.2121+.352+.33/2);
		glScalef(.5,.8986,.33);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-.3082/2,.33+.352+1.2121+.352+.33+15.1449/2);
		glScalef(.5,.3082,15.1449);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-.2505/2-.6479,.33+.352+1.2121+.352+.33+5.1719/2);
		glScalef(.5,.2505,5.1719);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-3.7698/2-.6479,.33+.352+1.2121+.352+.33+5.1719+.3738/2);
		glScalef(.5,3.7698,.3738);
		drawUnitSolidCube();
	}glPopMatrix();
	
	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-3.7698/2-.6479,.33+.352+1.2121+.352+.33+5.1719+.3738+3.8499+.5005/2);
		glScalef(.5,3.7698,.5005);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-.2505/2-.6479,.33+.352+1.2121+.352+.33+5.1719+.3738+3.8499+.5005+4.5575/2);
		glScalef(.5,.2505,4.5575);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-3.7695/2-.6479,.33+.352+1.2121+.352+.33+5.1719+.3738+3.8499+.5005+4.5575+.4380/2);
		glScalef(.5,3.7695,.4380);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-.2505/2-.6479-3.5149,.33+.352+1.2121+.352+.33+5.1719+.3738+3.8499+.5005+4.5575/2);
		glScalef(.5,.2505,4.5575);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-5.0823/2,.33+.352+1.2121+.352+.33+5.1719+.3738+3.8499+.5005+4.5575+.6270+.5129/2);
		glScalef(.5,5.0823,.5129);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,-4.1625,0);
		glPushMatrix();{
			glColor3f(.7,0,0);
			glTranslatef(.5/2,-.8986/2,.33+.352/2);
			glScalef(.5,.8986,.352);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glColor3f(.7,0,0);
			glTranslatef(.5/2,-.1795/2,.33+.352+1.2121/2);
			glScalef(.5,.1795,1.2121);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glColor3f(.7,0,0);
			glTranslatef(.5/2,-.1218/2-.7766,.33+.352+1.2121/2);
			glScalef(.5,.1218,1.2121);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glColor3f(.7,0,0);
			glTranslatef(.5/2,-.8986/2,.33+.352+1.2121+.352/2);
			glScalef(.5,.8986,.352);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glColor3f(.5,0,0);
			glTranslatef(.5/2,-.8986/2,.33+.352+1.2121+.352+.33/2);
			glScalef(.5,.8986,.33);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glColor3f(.5,0,0);
			glTranslatef(.5/2,-.3082/2-.5985,.33+.352+1.2121+.352+.33+15.1449/2);
			glScalef(.5,.3082,15.1449);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glColor3f(.5,0,0);
			glTranslatef(.5/2,-.2505/2,.33+.352+1.2121+.352+.33+5.1719/2);
			glScalef(.5,.2505,5.1719);
			drawUnitSolidCube();
		}glPopMatrix();

	}glPopMatrix();


	//upper surface
	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-5.0819/2,2.2461+.3282+15.1449+.5129/2);
		glScalef(.5,5.0819,.5129);
		drawUnitSolidCube();
	}glPopMatrix();
	
	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2+.2,-5.0819/2,2.2461+.3282+15.1449+.5129+1.1914/2);
		glScalef(.5,5.0819,1.1914);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.3,0,0);
		glTranslatef(.5/2,-5.0819/2,2.2461+.3282+15.1449+.5129+1.1914+.2118/2);
		glScalef(.5,5.0819,.2118);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2+.1,-5.0819/2,2.2461+.3282+15.1449+.5129+1.1914+.2118+.1/2);
		glScalef(.5,5.0819,.1);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2+.15,-5.0819/2,2.2461+.3282+15.1449+.5129+1.1914+.2118+.1+1.1748/2);
		glScalef(.5,5.0819,1.1748);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-5.0819/2,2.2461+.3282+15.1449+.5129+1.1914+.2118+.1+1.1748+.1573/2);
		glScalef(.5,5.0819,.1573);
		drawUnitSolidCube();
	}glPopMatrix();


	//back surface
	
	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2+.25,-4.4532/2-.3082,.33+.352+1.2121+.352+.33+5.1719+.3738+9.973/2);
		glScalef(.5,4.4532,9.973);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2+.25,-.3392/2-.3082,.33+.352+1.2121+.352+.33+5.53/2);
		glScalef(.5,.3392,5.53);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2+.25,-.3392/2-.3082,.33+.352+1.2121+.352+.33+5.53/2);
		glScalef(.5,.3392,5.53);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2+.25,-.3392/2-4.4177,.33+.352+1.2121+.352+.33+5.53/2);
		glScalef(.5,.3392,5.53);
		drawUnitSolidCube();
	}glPopMatrix();


	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2+.25,-.5971/2-.1795,.33+.352+1.2121/2);
		glScalef(.5,.5971,1.2121);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2+.25,-.63/2-4.32,.33+.352+1.2121/2);
		glScalef(.5,.63,1.2121);
		drawUnitSolidCube();
	}glPopMatrix();

}
void drawRedSideWallDesignLessPart(){
	glPushMatrix();{
		glTranslatef(2.25/2,-5.6193/2,6.4524/2);
		glScalef(2.25,5.6193,6.1044+.3480);
		drawUnitSolidCube();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(.5/2,-.3081/2,6.4524+11.2668/2);
		glScalef(.5,.3081,11.2668);
		drawUnitSolidCube();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(.5/2,-.5129/2-5.1064,6.4524+11.2668/2);
		glScalef(.5,.5129,11.2668);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(.5/2,-5.6193/2,6.4524+11.2668+.5129/2);
		glScalef(.5,5.6193,.5129);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(2.1/2+.15,-.15/2-.3081-1.6491,6.4524+2.5/2);
		glScalef(2.1,.15,2.5);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(2.1/2+.15,-.15/2-.3081-1.6491-1.35,6.4524+2.5/2);
		glScalef(2.1,.15,2.5);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(2.1/2+.15,-1.5/2-.3081-1.6491,6.4524+2.5+.15/2);
		glScalef(2.1,1.5,.15);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2+.25,-4.7983/2-.3081,6.4524+2.5+.15+8.6123/2);
		glScalef(.5,4.7983,8.6123);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2+.25,-1.6419/2-.3081,6.4524+2.65/2);
		glScalef(.5,1.6419,2.65);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2+.25,-1.6419/2-.3081-3.1491,6.4524+2.65/2);
		glScalef(.5,1.6419,2.65);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,.3,.3);
		glTranslatef(-.5/2+2.25,-1.1999/2-1.6491-.3081-.15,6.4524+2.5/2);
		glScalef(.5,1.1999,2.5);
		drawUnitSolidCube();
	}glPopMatrix();

	//upper surface
	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-5.6193/2,2.2461+.3282+15.1449+.5129/2);
		glScalef(.5,5.6193,.5129);
		drawUnitSolidCube();
	}glPopMatrix();
	
	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2+.2,-5.6193/2,2.2461+.3282+15.1449+.5129+1.1914/2);
		glScalef(.5,5.6193,1.1914);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.3,0,0);
		glTranslatef(.5/2,-5.6193/2,2.2461+.3282+15.1449+.5129+1.1914+.2118/2);
		glScalef(.5,5.6193,.2118);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2+.1,-5.6193/2,2.2461+.3282+15.1449+.5129+1.1914+.2118+.1/2);
		glScalef(.5,5.6193,.1);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2+.15,-5.6193/2,2.2461+.3282+15.1449+.5129+1.1914+.2118+.1+1.1748/2);
		glScalef(.5,5.6193,1.1748);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-5.6193/2,2.2461+.3282+15.1449+.5129+1.1914+.2118+.1+1.1748+.1573/2);
		glScalef(.5,5.6193,.1573);
		drawUnitSolidCube();
	}glPopMatrix();

}
void drawRedSideWall(){
	
	drawRedSideWallDesignedPart();

	glPushMatrix();{
		glTranslatef(0,-5.0819,0);
		drawRedSideWallDesignedPart();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,-5.0819*2,0);
		drawRedSideWallDesignedPart();
	}glPopMatrix();
	
	
	glPushMatrix();{
		glTranslatef(0,-5.0819*3,0);
		drawRedSideWallDesignLessPart();
	}glPopMatrix();
}
void drawRedBackDesignLessWall(){
	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(2.77/2,-5.6104/2,.33/2);
		glScalef(2.7700,5.6104,.33);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,-5.6104/2,.33+.352/2);
		glScalef(.5,5.6104,.352);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,-.5129/2,.33+.352+1.2121/2);
		glScalef(.5,.5129,1.2121);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,-.5264/2-5.0959,.33+.352+1.2121/2);
		glScalef(.5,.5264,1.2121);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,-5.6104/2,.33+.352+1.2121+.352/2);
		glScalef(.5,5.6104,.352);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.8,.8,.8);
		glTranslatef(.5/2,-5.6104/2,2.2461+.3282/2);
		glScalef(.5,5.6104,.3282);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.8,.8,.8);
		glTranslatef(.5/2,-.2947/2,2.2461+.3282+15.145/2);
		glScalef(.5,.2947,15.145);
		drawUnitSolidCube();
	}glPopMatrix();
	
	glPushMatrix();{
		glColor3f(.8,.8,.8);
		glTranslatef(.5/2,.2947/2-5.6104,2.2461+.3282+15.145/2);
		glScalef(.5,.2947,15.145);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(1,1,1);
		glTranslatef(.5/2,-5.6104/2,2.2461+.3282+15.1449+.5129/2);
		glScalef(.5,5.6104,.5129);
		drawUnitSolidCube();
	}glPopMatrix();

	//upper surface
	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-5.6104/2,2.2461+.3282+15.1449+.5129/2);
		glScalef(.5,5.6104,.5129);
		drawUnitSolidCube();
	}glPopMatrix();
	
	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2+.2,-5.6104/2,2.2461+.3282+15.1449+.5129+1.1914/2);
		glScalef(.5,5.6104,1.1914);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.3,0,0);
		glTranslatef(.5/2,-5.6104/2,2.2461+.3282+15.1449+.5129+1.1914+.2118/2);
		glScalef(.5,5.6104,.2118);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2+.1,-5.6104/2,2.2461+.3282+15.1449+.5129+1.1914+.2118+.1/2);
		glScalef(.5,5.6104,.1);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2+.15,-5.6104/2,2.2461+.3282+15.1449+.5129+1.1914+.2118+.1+1.1748/2);
		glScalef(.5,5.6104,1.1748);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-5.6104/2,2.2461+.3282+15.1449+.5129+1.1914+.2118+.1+1.1748+.1573/2);
		glScalef(.5,5.6104,.1573);
		drawUnitSolidCube();
	}glPopMatrix();


	//back surface
	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2+.15,-5.021/2-.2947,2.2461+.3282+15.145/2);
		glScalef(.5,5.021,15.145);
		drawUnitSolidCube();
	}glPopMatrix();
	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2+.15,-4.5711/2-.5129,.33+.352+1.2121/2);
		glScalef(.5,4.5711,1.2121);
		drawUnitSolidCube();
	}glPopMatrix();
}
void drawRedFrontWall(){
	glPushMatrix();{
		glTranslatef(-1.3127,1.47*sin(45.0)+1.47/2-.25,0);
		drawRedMinar();
	}glPopMatrix();
	
	drawRedNarrowFrontWall();
	
	glPushMatrix();{
		glTranslatef(0,-3.4,0);
		drawRedThinMinar();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,-3.6,0);
		drawRedWideFrontWall();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,-3.55-9,0);
		drawRedThinMinarTall();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,-3.55-9-.32,0);
		drawRedBigFrontGate();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,-3.55-9-.32-21.5350-.2,0);
		drawRedThinMinarTall();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,-3.55-9-.32-21.5350-.2-.32,0);
		drawRedWideFrontWall();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,-3.55-9-.32-21.5350-.2-.32-8.95,0);
		drawRedThinMinar();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,-3.55-9-.32-21.5350-.2-.32-8.95-.32,0);
		drawRedNarrowFrontWall();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(-1.3127,-3.55-9-.32-21.5350-.2-.32-8.95-.32-3.1018-(1.47*sin(45.0)+1.47/2-.25),0);
		drawRedMinar();
	}glPopMatrix();

}
void drawRedBackWall(){
	drawRedBackDesignLessWall();
	
	glPushMatrix();{
		glTranslatef(0,-5.6104,0);
		drawRedThinMinar();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,-5.6104-.2,0);
		drawRedWideFrontWall();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,-5.7604-9,0);
		drawRedThinMinarTall();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,-5.7604-9-.32,0);
		drawRedBigFrontGate();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,-5.7604-9-.32-21.5350-.2,0);
		drawRedThinMinarTall();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,-5.7604-9-.32-21.5350-.2-.32,0);
		drawRedWideFrontWall();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,-5.7604-9-.32-21.5350-.2-.32-8.95,0);
		drawRedThinMinar();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,-5.7604-9-.32-21.5350-.2-.32-8.95-.32,0);
		drawRedBackDesignLessWall();
	}glPopMatrix();
}
void drawRedSideWallLeft(){
		
	drawRedSideWallDesignLessPart();

	glPushMatrix();{
		glTranslatef(0,-5.6193,0);
		drawRedSideWallDesignedPart();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,-5.6193-5.0819*1,0);
		drawRedSideWallDesignedPart();
	}glPopMatrix();
	
	
	glPushMatrix();{
		glTranslatef(0,-5.6193-5.0819*2,0);
		drawRedSideWallDesignedPart();
	}glPopMatrix();
}

void drawRedPart(){
	drawRedFrontWall();

	glPushMatrix();{
		glTranslatef(1.47*sin(45.0)+1.47/2-.25+.3127,-3.55-9-.32-21.5350-.2-.32-8.95-.32-3.1018-(1.47*sin(45.0)+1.47/2-.25)*2+1.0322,0);
		glRotatef(90,0,0,1);
		drawRedSideWall();
	}glPopMatrix();

	glPushMatrix();{
		
		glTranslatef(1.89+5.0819*3+5.6193+.5,-49.5,0);
		glRotatef(180,0,0,1);
		drawRedBackWall();
	}glPopMatrix();
	
	glPushMatrix();{
		glTranslatef(1.89+5.0819*3+5.6193,2.5,0);
		glRotatef(-90,0,0,1);
		drawRedSideWallLeft();
	}glPopMatrix();

	glPushMatrix();{
		//roof
		glTranslatef(21/2+.9,-50.3456/2+1.5244,2.0811+17.173+.5);
		glScalef(21,50.3456,1);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(21/2+.9,-50.3456/2+1.5244,2.0811+17.173+1);
		drawBigGombuj();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(21/2+.9,-50.3456/2+1.5244+14.2/2+2.8117+9/2,2.0811+17.173+1);
		drawGombuj();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(21/2+.9,-50.3456/2+1.5244-14.2/2-2.8117-9/2,2.0811+17.173+1);
		drawGombuj();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(17.4835,-1.7744-.5648,0);
		drawRedMinar();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(17.4835,-.4722-1.7744*2-42.9065,0);
		drawRedMinar();
	}glPopMatrix();
}
void display(){

	//clear the display
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(BLACK, 0);	//color black
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	/********************
	/ set-up camera here
	********************/
	//load the correct matrix -- MODEL-VIEW matrix
	glMatrixMode(GL_MODELVIEW);

	//initialize the matrix
	glLoadIdentity();

	//now give three info
	//1. where is the camera (viewer)?
	//2. where is the camera is looking?
	//3. Which direction is the camera's UP direction?

	//instead of CONSTANT information, we will define a circular path.
//	gluLookAt(-30,-30,50,	0,0,0,	0,0,1);

	gluLookAt(cameraForward, cameraAlong, cameraUp,		lookX,lookY,lookZ,		upX,upY,upZ);
	
	//gluLookAt(cameraRadius*cos(cameraAngleX)*cos(cameraAngle), cameraRadius*sin(cameraAngleY), cameraHeight*sin(cameraAngle),		lookX,lookY,lookZ,		0,0,1);
	
	//NOTE: the camera still CONSTANTLY looks at the center
	// cameraAngle is in RADIAN, since you are using inside COS and SIN
	
	
	//again select MODEL-VIEW
	glMatrixMode(GL_MODELVIEW);


	/****************************
	/ Add your objects from here
	****************************/
	
	drawGrid(true);
	drawCameraTest();
	//drawRedMinar();
	//drawRedNarrowFrontWall();
	//drawRedThinMinar();
	//drawRedWideFrontWall();
	//drawRedBigFrontGate();
	//drawRedThinMinarTall();
	//drawRedSideWall();
	//drawRedBackDesignLessWall();
	/*glPushMatrix();{
		glTranslatef(0,3.4685,0);
		drawRedNarrowFrontWall();
	}glPopMatrix();
	*/
	//drawRedPart();
	
	
	//ADD this line in the end --- if you use double buffer (i.e. GL_DOUBLE)
	glutSwapBuffers();
}

void animate(){
	//codes for any changes in Camera

	//cameraAngle += cameraAngleDelta;	// camera will rotate at 0.002 radians per frame.	// keep the camera steady NOW!!
	
	//codes for any changes in Models
	
	rectAngle -= 1;

	//MISSING SOMETHING? -- YES: add the following
	glutPostRedisplay();	//this will call the display AGAIN
}

void keyboardListener(unsigned char key, int x,int y){
	
	double dirX,dirY,dirZ;
	double alongX,alongY,alongZ;
	double newLookX,newLookY,newLookZ;
	double modForward,modAlong,modUp,r;
	double upPointX,upPointY,upPointZ;
	double angle;

	switch(key){

		case '1':	
			//cameraAngleX=cameraAngleX+.01;
			//lookX=cameraForward+cameraRadius*cos(cameraAngleX);
			//lookY= cameraAlong+ cameraRadius*sin(cameraAngleX);
			
			r=sqrt((cameraForward-lookX)*(cameraForward-lookX)+(cameraAlong-lookY)*(cameraAlong-lookY)+(cameraUp-lookZ)*(cameraUp-lookZ));

			dirX=cameraForward-lookX;
			dirY=cameraAlong-lookY;
			dirZ=cameraUp-lookZ;
			
			alongX=-dirY*upZ+dirZ*upY;
			alongY=-dirZ*upX+dirX*upZ;
			alongZ=-dirX*upY+dirY*upX;

			modAlong=1/sqrt(alongX*alongX+alongY*alongY+alongZ*alongZ);
			modForward=1/sqrt(dirX*dirX+dirY*dirY+dirZ*dirZ);

			angle=180*3.1416/180+.1;

			lookX=cameraForward+r*dirX*modForward*cos(angle)+r*alongX*modAlong*sin(angle);
			lookY=cameraAlong+r*dirY*modForward*cos(angle)+r*alongY*modAlong*sin(angle);
			lookZ=cameraUp+r*dirZ*modForward*cos(angle)+r*alongZ*modAlong*sin(angle);

			printf("%lf\n",r);
			printf("%lf,%lf,%lf\n",dirX,dirY,dirZ);
			printf("%lf,%lf,%lf\n",alongX,alongY,alongZ);
			printf("%lf,%lf,%lf\n\n",lookX,lookY,lookZ);

			break;

		case '2':	
			r=sqrt((cameraForward-lookX)*(cameraForward-lookX)+(cameraAlong-lookY)*(cameraAlong-lookY)+(cameraUp-lookZ)*(cameraUp-lookZ));

			dirX=cameraForward-lookX;
			dirY=cameraAlong-lookY;
			dirZ=cameraUp-lookZ;
			
			alongX=-dirY*upZ+dirZ*upY;
			alongY=-dirZ*upX+dirX*upZ;
			alongZ=-dirX*upY+dirY*upX;

			modAlong=1/sqrt(alongX*alongX+alongY*alongY+alongZ*alongZ);
			modForward=1/sqrt(dirX*dirX+dirY*dirY+dirZ*dirZ);

			angle=180*3.1416/180-.1;

			lookX=cameraForward+r*dirX*modForward*cos(angle)+r*alongX*modAlong*sin(angle);
			lookY=cameraAlong+r*dirY*modForward*cos(angle)+r*alongY*modAlong*sin(angle);
			lookZ=cameraUp+r*dirZ*modForward*cos(angle)+r*alongZ*modAlong*sin(angle);

			printf("%lf\n",r);
			printf("%lf,%lf,%lf\n",dirX,dirY,dirZ);
			printf("%lf,%lf,%lf\n",alongX,alongY,alongZ);
			printf("%lf,%lf,%lf\n\n",lookX,lookY,lookZ);
			break;

		case '3':				
			//cameraAngleZ=cameraAngleZ+.01;
			//lookX=cameraForward+cameraRadius*cos(cameraAngleZ);
			//lookZ=cameraUp+cameraRadius*sin(cameraAngleZ);
		
			r=sqrt((cameraForward-lookX)*(cameraForward-lookX)+(cameraAlong-lookY)*(cameraAlong-lookY)+(cameraUp-lookZ)*(cameraUp-lookZ));

			dirX=cameraForward-lookX;
			dirY=cameraAlong-lookY;
			dirZ=cameraUp-lookZ;
			
			alongX=-dirY*upZ+dirZ*upY;
			alongY=-dirZ*upX+dirX*upZ;
			alongZ=-dirX*upY+dirY*upX;

			modForward=1/sqrt(dirX*dirX+dirY*dirY+dirZ*dirZ);
			modUp=1/sqrt(upX*upX+upY*upY+upZ*upZ);
			angle=180*3.1416/180+.1;

			lookX=cameraForward+r*dirX*modForward*cos(angle)+r*upX*modUp*sin(angle);
			lookY=cameraAlong+r*dirY*modForward*cos(angle)+r*upY*modUp*sin(angle);
			lookZ=cameraUp+r*dirZ*modForward*cos(angle)+r*upZ*modUp*sin(angle);
			
			dirX=cameraForward-lookX;
			dirY=cameraAlong-lookY;
			dirZ=cameraUp-lookZ;

			upX=dirY*alongZ-dirZ*alongY;
			upY=dirZ*alongX-dirX*alongZ;
			upZ=dirX*alongY-dirY*alongX;

			modUp=1/sqrt(upX*upX+upY*upY+upZ*upZ);

			upX*=modUp;
			upY*=modUp;
			upZ*=modUp;

			printf("%lf\n",dirX*upX+dirY*upY+dirZ*upZ);
			printf("%lf\n",dirX*alongX+dirY*alongY+dirZ*alongZ);
			printf("%lf\n",alongX*upX+alongY*upY+alongZ*upZ);


			printf("%lf\n",r);
			printf("%lf,%lf,%lf\n",dirX,dirY,dirZ);
			printf("%lf,%lf,%lf\n",alongX,alongY,alongZ);
			printf("%lf,%lf,%lf\n",upX,upY,upZ);
			printf("%lf,%lf,%lf\n\n",lookX,lookY,lookZ);
			break;

		case '4':				
			//cameraAngleZ=cameraAngleZ-.01;
			//lookX=cameraForward+cameraRadius*cos(cameraAngleZ);
			//lookZ=cameraUp+cameraRadius*sin(cameraAngleZ);
			
			r=sqrt((cameraForward-lookX)*(cameraForward-lookX)+(cameraAlong-lookY)*(cameraAlong-lookY)+(cameraUp-lookZ)*(cameraUp-lookZ));

			dirX=cameraForward-lookX;
			dirY=cameraAlong-lookY;
			dirZ=cameraUp-lookZ;
			
			alongX=-dirY*upZ+dirZ*upY;
			alongY=-dirZ*upX+dirX*upZ;
			alongZ=-dirX*upY+dirY*upX;

			modForward=1/sqrt(dirX*dirX+dirY*dirY+dirZ*dirZ);
			modUp=1/sqrt(upX*upX+upY*upY+upZ*upZ);
			angle=180*3.1416/180-.1;

			lookX=cameraForward+r*dirX*modForward*cos(angle)+r*upX*modUp*sin(angle);
			lookY=cameraAlong+r*dirY*modForward*cos(angle)+r*upY*modUp*sin(angle);
			lookZ=cameraUp+r*dirZ*modForward*cos(angle)+r*upZ*modUp*sin(angle);
			
			dirX=cameraForward-lookX;
			dirY=cameraAlong-lookY;
			dirZ=cameraUp-lookZ;

			upX=dirY*alongZ-dirZ*alongY;
			upY=dirZ*alongX-dirX*alongZ;
			upZ=dirX*alongY-dirY*alongX;

			modUp=1/sqrt(upX*upX+upY*upY+upZ*upZ);

			upX*=modUp;
			upY*=modUp;
			upZ*=modUp;

			printf("%lf\n",dirX*upX+dirY*upY+dirZ*upZ);
			printf("%lf\n",dirX*alongX+dirY*alongY+dirZ*alongZ);
			printf("%lf\n",alongX*upX+alongY*upY+alongZ*upZ);


			printf("%lf\n",r);
			printf("%lf,%lf,%lf\n",dirX,dirY,dirZ);
			printf("%lf,%lf,%lf\n",alongX,alongY,alongZ);
			printf("%lf,%lf,%lf\n",upX,upY,upZ);
			printf("%lf,%lf,%lf\n\n",lookX,lookY,lookZ);
			
			break;

		case '5':
			/*rollAngle=rollAngle+.01;
			upX=cos(rollAngle);
			upZ=sin(rollAngle);
			printf("%lf,%lf\n",upX,upZ);*/

			r=sqrt((cameraForward-lookX)*(cameraForward-lookX)+(cameraAlong-lookY)*(cameraAlong-lookY)+(cameraUp-lookZ)*(cameraUp-lookZ));
			
			dirX=cameraForward-lookX;
			dirY=cameraAlong-lookY;
			dirZ=cameraUp-lookZ;
			
			alongX=-dirY*upZ+dirZ*upY;
			alongY=-dirZ*upX+dirX*upZ;
			alongZ=-dirX*upY+dirY*upX;

			modAlong=1/sqrt(alongX*alongX+alongY*alongY+alongZ*alongZ);
			modUp=1/sqrt(upX*upX+upY*upY+upZ*upZ);
			angle=0*3.1416/180+.01;

			upPointX=cameraForward+r*upX*modUp*cos(angle)+r*alongX*modAlong*sin(angle);
			upPointY=cameraAlong+r*upY*modUp*cos(angle)+r*alongY*modAlong*sin(angle);
			upPointZ=cameraUp+r*upZ*modUp*cos(angle)+r*alongZ*modAlong*sin(angle);
			
			/*dirX=cameraForward-lookX;
			dirY=cameraAlong-lookY;
			dirZ=cameraUp-lookZ;

			upX=dirY*alongZ-dirZ*alongY;
			upY=dirZ*alongX-dirX*alongZ;
			upZ=dirX*alongY-dirY*alongX;*/

			upX=upPointX-cameraForward;
			upY=upPointY-cameraAlong;
			upZ=upPointZ-cameraUp;
			
			modUp=1/sqrt(upX*upX+upY*upY+upZ*upZ);

			upX*=modUp;
			upY*=modUp;
			upZ*=modUp;

			printf("%lf\n",dirX*upX+dirY*upY+dirZ*upZ);
			printf("%lf\n",dirX*alongX+dirY*alongY+dirZ*alongZ);
			printf("%lf\n",alongX*upX+alongY*upY+alongZ*upZ);


			printf("%lf\n",r);
			printf("%lf,%lf,%lf\n",dirX,dirY,dirZ);
			printf("%lf,%lf,%lf\n",alongX,alongY,alongZ);
			printf("%lf,%lf,%lf\n",upX,upY,upZ);
			printf("%lf,%lf,%lf\n\n",lookX,lookY,lookZ);

			break;

		case '6':	
			r=sqrt((cameraForward-lookX)*(cameraForward-lookX)+(cameraAlong-lookY)*(cameraAlong-lookY)+(cameraUp-lookZ)*(cameraUp-lookZ));
			
			dirX=cameraForward-lookX;
			dirY=cameraAlong-lookY;
			dirZ=cameraUp-lookZ;
			
			alongX=-dirY*upZ+dirZ*upY;
			alongY=-dirZ*upX+dirX*upZ;
			alongZ=-dirX*upY+dirY*upX;

			modAlong=1/sqrt(alongX*alongX+alongY*alongY+alongZ*alongZ);
			modUp=1/sqrt(upX*upX+upY*upY+upZ*upZ);
			angle=0*3.1416/180-.01;

			upPointX=cameraForward+r*upX*modUp*cos(angle)+r*alongX*modAlong*sin(angle);
			upPointY=cameraAlong+r*upY*modUp*cos(angle)+r*alongY*modAlong*sin(angle);
			upPointZ=cameraUp+r*upZ*modUp*cos(angle)+r*alongZ*modAlong*sin(angle);
			
			/*dirX=cameraForward-lookX;
			dirY=cameraAlong-lookY;
			dirZ=cameraUp-lookZ;

			upX=dirY*alongZ-dirZ*alongY;
			upY=dirZ*alongX-dirX*alongZ;
			upZ=dirX*alongY-dirY*alongX;*/

			upX=upPointX-cameraForward;
			upY=upPointY-cameraAlong;
			upZ=upPointZ-cameraUp;
			
			modUp=1/sqrt(upX*upX+upY*upY+upZ*upZ);

			upX*=modUp;
			upY*=modUp;
			upZ*=modUp;

			printf("%lf\n",dirX*upX+dirY*upY+dirZ*upZ);
			printf("%lf\n",dirX*alongX+dirY*alongY+dirZ*alongZ);
			printf("%lf\n",alongX*upX+alongY*upY+alongZ*upZ);


			printf("%lf\n",r);
			printf("%lf,%lf,%lf\n",dirX,dirY,dirZ);
			printf("%lf,%lf,%lf\n",alongX,alongY,alongZ);
			printf("%lf,%lf,%lf\n",upX,upY,upZ);
			printf("%lf,%lf,%lf\n\n",lookX,lookY,lookZ);
			break;

		case '8':	//toggle grids
			canDrawGrid = 1 - canDrawGrid;
			break;


		case 'w':
			cameraAngleX += .01;
			cameraAngleY +=.01;
			break;

		case 's':
			cameraAngleX -= .01;
			cameraAngleY -=.01;
			break;

		case 'a':
			cameraAngle+=.01;
			break;


		case 'd':
			cameraAngle-=.01;
			break;
	
		case 'x':
			cameraRadius+=1;
			break;

		case 'c':
			cameraRadius-=1;
			break;

		case 'v':
			cameraHeight+=10;
			lookZ=cameraHeight*sin(cameraAngle);
			break;

		case 'b':
			cameraHeight-=10;
			lookZ=cameraHeight*sin(cameraAngle);
			break;
		case 27:	//ESCAPE KEY -- simply exit
			exit(0);
			break;

		default:
			break;
	}
}

void specialKeyListener(int key, int x,int y){
	
	double dirX,dirY,dirZ;
	double alongX,alongY,alongZ;
	double newLookX,newLookY,newLookZ;
	double normalize;

	switch(key){
		case GLUT_KEY_DOWN:		//down arrow key
			//cameraForward=cameraForward+5;
			//lookX+=5;
			//printf("%lf,%lf,%lf %lf,%lf,%lf %lf,%lf,%lf\n",dirX,dirY,dirZ,lookX,lookY,lookZ,cameraForward,cameraAlong,cameraUp);
			
			dirX=cameraForward-lookX;
			dirY=cameraAlong-lookY;
			dirZ=cameraUp-lookZ;

			newLookX=lookX+.5*dirX;
			newLookY=lookY+.5*dirY;
			newLookZ=lookZ+.5*dirZ;

			cameraForward=lookX+1.5*dirX;
			cameraAlong=lookY+1.5*dirY;
			cameraUp=lookZ+1.5*dirZ;

			lookX=newLookX;
			lookY=newLookY;
			lookZ=newLookZ;
					
			break;
		case GLUT_KEY_UP:		// up arrow key
			dirX=cameraForward-lookX;
			dirY=cameraAlong-lookY;
			dirZ=cameraUp-lookZ;

			newLookX=lookX-.5*dirX;
			newLookY=lookY-.5*dirY;
			newLookZ=lookZ-.5*dirZ;

			cameraForward=lookX+.5*dirX;
			cameraAlong=lookY+.5*dirY;
			cameraUp=lookZ+.5*dirZ;

			lookX=newLookX;
			lookY=newLookY;
			lookZ=newLookZ;
			
			break;

		case GLUT_KEY_RIGHT:
			//cameraAlong=cameraAlong+5;
			//lookY+=5;

			//printf("%lf,%lf,%lf\n",cameraForward,cameraAlong,cameraUp);
			
			dirX=cameraForward-lookX;
			dirY=cameraAlong-lookY;
			dirZ=cameraUp-lookZ;

			alongX=dirY*upZ-dirZ*upY;
			alongY=dirZ*upX-dirX*upZ;
			alongZ=dirX*upY-dirY*upX;

			normalize=1/sqrt(alongX*alongX+alongY*alongY+alongZ*alongZ);

			cameraForward=cameraForward-normalize*alongX;
			cameraAlong=cameraAlong-normalize*alongY;
			cameraUp=cameraUp-normalize*alongZ;
			
			lookX=lookX-normalize*alongX;
			lookY=lookY-normalize*alongY;
			lookZ=lookZ-normalize*alongZ;

			printf("%lf,%lf,%lf\n",cameraForward,cameraAlong,cameraUp);
			printf("%lf,%lf,%lf\n",lookX,lookY,lookZ);

			break;
		case GLUT_KEY_LEFT:
			//cameraAlong=cameraAlong-5;
			//lookY-=5;

			dirX=cameraForward-lookX;
			dirY=cameraAlong-lookY;
			dirZ=cameraUp-lookZ;

			alongX=dirY*upZ-dirZ*upY;
			alongY=dirZ*upX-dirX*upZ;
			alongZ=dirX*upY-dirY*upX;

			normalize=1/sqrt(alongX*alongX+alongY*alongY+alongZ*alongZ);

			cameraForward=cameraForward+normalize*alongX;
			cameraAlong=cameraAlong+normalize*alongY;
			cameraUp=cameraUp+normalize*alongZ;
			
			lookX=lookX+normalize*alongX;
			lookY=lookY+normalize*alongY;
			lookZ=lookZ+normalize*alongZ;

			//printf("%lf,%lf,%lf\n",alongX,alongY,alongZ);
			printf("%lf,%lf,%lf\n",cameraForward,cameraAlong,cameraUp);
			printf("%lf,%lf,%lf\n",lookX,lookY,lookZ);
			break;

		case GLUT_KEY_PAGE_UP:
			//cameraUp=cameraUp+2;
			//lookZ+=2;

			normalize=1/sqrt(upX*upX+upY*upY+upZ*upZ);

			cameraForward=cameraForward+normalize*upX;
			cameraAlong=cameraAlong+normalize*upY;
			cameraUp=cameraUp+normalize*upZ;
			
			lookX=lookX+normalize*upX;
			lookY=lookY+normalize*upY;
			lookZ=lookZ+normalize*upZ;
			printf("%lf,%lf,%lf\n",upX,upY,upZ);
			//printf("%lf\n",normalize);
			printf("%lf,%lf,%lf\n",cameraForward,cameraAlong,cameraUp);
			printf("%lf,%lf,%lf\n",lookX,lookY,lookZ);

			break;
		case GLUT_KEY_PAGE_DOWN:
			//cameraUp=cameraUp-2;
			//lookZ-=2;

			normalize=1/sqrt(upX*upX+upY*upY+upZ*upZ);

			cameraForward=cameraForward-normalize*upX;
			cameraAlong=cameraAlong-normalize*upY;
			cameraUp=cameraUp-normalize*upZ;
			
			lookX=lookX-normalize*upX;
			lookY=lookY-normalize*upY;
			lookZ=lookZ-normalize*upZ;
			printf("%lf,%lf,%lf\n",upX,upY,upZ);
			//printf("%lf\n",normalize);
			printf("%lf,%lf,%lf\n",cameraForward,cameraAlong,cameraUp);
			printf("%lf,%lf,%lf\n",lookX,lookY,lookZ);

			break;

		case GLUT_KEY_INSERT:
			break;

		case GLUT_KEY_HOME:
			cameraAngleX+=.01;
			cameraAngleY+=.01;
			break;
		case GLUT_KEY_END:
			cameraAngleX-=.01;
			cameraAngleY-=.01;
			break;

		default:
			break;
	}
}

void mouseListener(int button, int state, int x, int y){	//x, y is the x-y of the screen (2D)
	switch(button){
		case GLUT_LEFT_BUTTON:
			if(state == GLUT_DOWN){		// 2 times?? in ONE click? -- solution is checking DOWN or UP
				//cameraAngleDelta = -cameraAngleDelta;	
			}
			break;

		case GLUT_RIGHT_BUTTON:
			//........
			break;

		case GLUT_MIDDLE_BUTTON:
			//........
			break;

		default:
			break;
	}
}


void init(){
	//codes for initialization
	cameraAngle = 0;	//// init the cameraAngle
	//cameraAngleDelta = 0.002;
	rectAngle = 0;
	canDrawGrid = true;
	cameraHeight = 0;
	cameraRadius = 10;

	cameraForward=-20;
	cameraAlong=0;
	cameraUp=0;

	lookX=0;
	lookY=0;
	lookZ=0;

	upX=0;
	upY=0;
	upZ=1;
	rollAngle=0;
	cameraRadiusX=50;
	cameraRadiusY=50;
	cameraRadiusZ=50;
	cameraAngleX=180*3.1416/180;
	cameraAngleY=0;
	cameraAngleZ=(180-atan(10.0/50))*3.1416/180;

	//clear the screen
	glClearColor(BLACK, 0);

	/************************
	/ set-up projection here
	************************/
	//load the PROJECTION matrix
	glMatrixMode(GL_PROJECTION);
	
	//initialize the matrix
	glLoadIdentity();

	//give PERSPECTIVE parameters
	gluPerspective(70,	1,	0.1,	10000.0);
	//field of view in the Y (vertically)
	//aspect ratio that determines the field of view in the X direction (horizontally)
	//near distance
	//far distance
}

int main(int argc, char **argv){
	glutInit(&argc,argv);
	glutInitWindowSize(500, 500);
	glutInitWindowPosition(0, 0);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGB);	//Depth, Double buffer, RGB color

	glutCreateWindow("My OpenGL Program");

	init();

	glEnable(GL_DEPTH_TEST);	//enable Depth Testing

	glutDisplayFunc(display);	//display callback function
	glutIdleFunc(animate);		//what you want to do in the idle time (when no drawing is occuring)

	//ADD keyboard listeners:
	glutKeyboardFunc(keyboardListener);
	glutSpecialFunc(specialKeyListener);

	//ADD mouse listeners:
	glutMouseFunc(mouseListener);

	glutMainLoop();		//The main loop of OpenGL

	return 0;
}

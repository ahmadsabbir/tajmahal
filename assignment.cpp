#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<windows.h>
#include<GL/glut.h>
#define BLACK 0, 0, 0
#define M_PI 3.1415923

/****************************************************************
/Function Prototype
*****************************************************************/
void drawGrid();
void glutCircle(float cx, float cy, float r, int num_segments);
void glutSolidCircle(float r, int num_segments);
void glutSolidCylinder(float r, float height, int nothing, int num_segments);
void glutWireCylinder(float r, float height, int slices, int num_segments);
void loadBmp(void);
int LoadBitmap(char *filename);


void drawUnitSolidCube();
//new functions
void door();
void miniDoor();
void whiteFloor();
void whiteMinarFloor();
void floorSideDesign();
void drawDoors();
void minarBase();
void minar();
void minarTop();
void roof();
void minarTopBars();
void gombuj();
void gombujTop();
void moon();
void roofBigGombuj();
void drawThinMinars();
void mainDoorSideBar();
void smallDoorSideBar();
void setSideDesign();
void minarRing();
void smallTorus();
void floorRailing();
void drawBase();
void setBase();
void setRedParts();
void redMinarBase();
void setRedBaseSideDesign();
void redFatMinar();
void fatMinarRing();
void fatMinarTop();
void fatMinarTopParts();
void redMinarRoad();
void fatMinarTopWithGombuj();

void drawRedPartDoor();

//r
void drawRedThinMinar();
void drawRedThinMinarRing();
void drawRedPart();
void drawRedSideWallLeft();
void drawRedBackWall();
void drawRedFrontWall();
void drawRedBackDesignLessWall();
void drawRedSideWall();
void drawRedSideWallDesignLessPart();
void drawRedSideWallDesignedPart();
void drawRedThinMinarTall();
void drawRedBigFrontGate();
void drawRedWideFrontWall();
void drawRedNarrowFrontWall();
void drawRedMinar();
void drawRedMinarSide();
void drawGombuj();
void drawBigGombuj();
/****************************************************************
/Global Variables
*****************************************************************/

//make a global variable -- for tracking the anglular position of camera
int num_texture = -1;
GLuint texid1;
GLuint minarBmp;
GLuint minarBaseBmp;
GLuint thinMinarBmp;
GLuint floorBmp;
GLuint roofBmp;
GLuint whiteFloorBmp;
GLuint minarBaseFloorBmp;
GLuint floorSideBmp;
GLuint redBaseOctBmp;
GLuint redBaseSideBmp;
GLuint redFatMinarBmp;
GLuint redMinarRoadBmp;
GLuint redMinarRoadTopBmp;
GLuint redMinarRoadThinBmp;
GLuint redFloorBmp;

GLUquadric* IDquadric=gluNewQuadric() ;//=new GLUquadricObj



int canDrawGrid;

/*double cameraAngle;			//in radian
double cameraAngleDelta;
int stopCamera=1;
int changeLookAt;

double cameraHeight;	
double cameraRadius;

double globeAngle; //in degree

double cameraAxisAngle;			//in radian
double cameraLookAtHeight;


double cameraX;
double cameraY;

double lookAtX;
double lookAtY;*/

/********************************************************
for camera
********************************************************/

//make a global variable -- for tracking the anglular position of camera
			//in radian

double upX,upY,upZ;
double rollAngle;
double cameraRadiusX;
double cameraAngleX;
double cameraRadiusY;
double cameraAngleY;
double cameraRadiusZ;
double cameraAngleZ;

double cameraHeight;	
double cameraRadius;

double cameraForward;
double cameraAlong;
double cameraUp;

double lookX,lookY,lookZ;

double rectAngle;	//in degree

//bool canDrawGrid;
double cameraAngle;


/****************************************************************
/main functions openGL
*****************************************************************/

void display(){

	//clear the display
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(BLACK, 0);	//color black
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	/********************
	/ set-up camera here
	********************/
	//load the correct matrix -- MODEL-VIEW matrix
	glMatrixMode(GL_MODELVIEW);

	//initialize the matrix
	glLoadIdentity();

	//now give three info
	//1. where is the camera (viewer)?
	//2. where is the camera is looking?
	//3. Which direction is the camera's UP direction?

	//instead of CONSTANT information, we will define a circular path.
	//gluLookAt(-30,-30,50,	0,0,0,	0,0,1);

	//gluLookAt(cameraRadius*cos(cameraAngle), cameraRadius*sin(cameraAngle), cameraHeight,		0,0,0,		0,0,1);
	
	/*cameraX=cameraRadius*cos(cameraAngle);
	cameraY=cameraRadius*sin(cameraAngle);
	
	if(changeLookAt==1){
		lookAtX=cameraRadius*cos(cameraAxisAngle)+cameraX;
		lookAtY=cameraRadius*sin(cameraAxisAngle)+cameraY;
		changeLookAt=0;
	}
	
	gluLookAt(cameraX, cameraY, cameraHeight,	lookAtX,lookAtY,cameraLookAtHeight,	0,0,1);*/






	/**********************************
	taj mahal camera
	**********************************/
	gluLookAt(cameraForward, cameraAlong, cameraUp,		lookX,lookY,lookZ,		upX,upY,upZ);

	//NOTE: the camera still CONSTANTLY looks at the center
	// cameraAngle is in RADIAN, since you are using inside COS and SIN
	
	//again select MODEL-VIEW
	glMatrixMode(GL_MODELVIEW);

	glPushMatrix();{
		glTranslatef(0,0,-5);
		//drawGrid();
	}glPopMatrix();
	/****************************
	/ Add your objects from here
	****************************/
	
	minarBase();
	
	glPushMatrix();{
		glTranslatef(0,0,1);
		whiteFloor();
		glPushMatrix();{
			glTranslatef(0,0,3.1);
			drawDoors();
			minar();
			roof();
			drawThinMinars();
			whiteMinarFloor();
		}glPopMatrix();
	}glPopMatrix();
	setSideDesign();
	setBase();
	setRedParts();
	setRedBaseSideDesign();
	redFatMinar();

	//ADD this line in the end --- if you use double buffer (i.e. GL_DOUBLE)
	glutSwapBuffers();
}

void animate(){
	//codes for any changes in Camera
	//if(stopCamera==0){
	//	cameraAngle += cameraAngleDelta;	// camera will rotate at 0.002 radians per frame.	// keep the camera steady NOW!!
	//}

	//globeAngle+=1;
	//if(globeAngle==360){
	//	globeAngle=0;
	//}

	//codes for any changes in Models
	
	//rectAngle -= 1;


	rectAngle -= 1;

	//MISSING SOMETHING? -- YES: add the following
	glutPostRedisplay();	//this will call the display AGAIN
}

void keyboardListener(unsigned char key, int x,int y){
	
	double dirX,dirY,dirZ;
	double alongX,alongY,alongZ;
	//double newLookX,newLookY,newLookZ;
	double modForward,modAlong,modUp,r;
	double upPointX,upPointY,upPointZ;
	double angle;

	switch(key){

		case '1':	
			//cameraAngleX=cameraAngleX+.01;
			//lookX=cameraForward+cameraRadius*cos(cameraAngleX);
			//lookY= cameraAlong+ cameraRadius*sin(cameraAngleX);
			
			r=sqrt((cameraForward-lookX)*(cameraForward-lookX)+(cameraAlong-lookY)*(cameraAlong-lookY)+(cameraUp-lookZ)*(cameraUp-lookZ));

			dirX=cameraForward-lookX;
			dirY=cameraAlong-lookY;
			dirZ=cameraUp-lookZ;
			
			alongX=-dirY*upZ+dirZ*upY;
			alongY=-dirZ*upX+dirX*upZ;
			alongZ=-dirX*upY+dirY*upX;

			modAlong=1/sqrt(alongX*alongX+alongY*alongY+alongZ*alongZ);
			modForward=1/sqrt(dirX*dirX+dirY*dirY+dirZ*dirZ);

			angle=180*3.1416/180+.1;

			lookX=cameraForward+r*dirX*modForward*cos(angle)+r*alongX*modAlong*sin(angle);
			lookY=cameraAlong+r*dirY*modForward*cos(angle)+r*alongY*modAlong*sin(angle);
			lookZ=cameraUp+r*dirZ*modForward*cos(angle)+r*alongZ*modAlong*sin(angle);

			//printf("%lf\n",r);
			//printf("%lf,%lf,%lf\n",dirX,dirY,dirZ);
			//printf("%lf,%lf,%lf\n",alongX,alongY,alongZ);
			//printf("%lf,%lf,%lf\n\n",lookX,lookY,lookZ);

			break;

		case '2':	
			r=sqrt((cameraForward-lookX)*(cameraForward-lookX)+(cameraAlong-lookY)*(cameraAlong-lookY)+(cameraUp-lookZ)*(cameraUp-lookZ));

			dirX=cameraForward-lookX;
			dirY=cameraAlong-lookY;
			dirZ=cameraUp-lookZ;
			
			alongX=-dirY*upZ+dirZ*upY;
			alongY=-dirZ*upX+dirX*upZ;
			alongZ=-dirX*upY+dirY*upX;

			modAlong=1/sqrt(alongX*alongX+alongY*alongY+alongZ*alongZ);
			modForward=1/sqrt(dirX*dirX+dirY*dirY+dirZ*dirZ);

			angle=180*3.1416/180-.1;

			lookX=cameraForward+r*dirX*modForward*cos(angle)+r*alongX*modAlong*sin(angle);
			lookY=cameraAlong+r*dirY*modForward*cos(angle)+r*alongY*modAlong*sin(angle);
			lookZ=cameraUp+r*dirZ*modForward*cos(angle)+r*alongZ*modAlong*sin(angle);

			//printf("%lf\n",r);
			//printf("%lf,%lf,%lf\n",dirX,dirY,dirZ);
			//printf("%lf,%lf,%lf\n",alongX,alongY,alongZ);
			//printf("%lf,%lf,%lf\n\n",lookX,lookY,lookZ);
			break;

		case '3':				
			//cameraAngleZ=cameraAngleZ+.01;
			//lookX=cameraForward+cameraRadius*cos(cameraAngleZ);
			//lookZ=cameraUp+cameraRadius*sin(cameraAngleZ);
		
			r=sqrt((cameraForward-lookX)*(cameraForward-lookX)+(cameraAlong-lookY)*(cameraAlong-lookY)+(cameraUp-lookZ)*(cameraUp-lookZ));

			dirX=cameraForward-lookX;
			dirY=cameraAlong-lookY;
			dirZ=cameraUp-lookZ;
			
			alongX=-dirY*upZ+dirZ*upY;
			alongY=-dirZ*upX+dirX*upZ;
			alongZ=-dirX*upY+dirY*upX;

			modForward=1/sqrt(dirX*dirX+dirY*dirY+dirZ*dirZ);
			modUp=1/sqrt(upX*upX+upY*upY+upZ*upZ);
			angle=180*3.1416/180+.1;

			lookX=cameraForward+r*dirX*modForward*cos(angle)+r*upX*modUp*sin(angle);
			lookY=cameraAlong+r*dirY*modForward*cos(angle)+r*upY*modUp*sin(angle);
			lookZ=cameraUp+r*dirZ*modForward*cos(angle)+r*upZ*modUp*sin(angle);
			
			dirX=cameraForward-lookX;
			dirY=cameraAlong-lookY;
			dirZ=cameraUp-lookZ;

			upX=dirY*alongZ-dirZ*alongY;
			upY=dirZ*alongX-dirX*alongZ;
			upZ=dirX*alongY-dirY*alongX;

			modUp=1/sqrt(upX*upX+upY*upY+upZ*upZ);

			upX*=modUp;
			upY*=modUp;
			upZ*=modUp;

			//printf("%lf\n",dirX*upX+dirY*upY+dirZ*upZ);
			//printf("%lf\n",dirX*alongX+dirY*alongY+dirZ*alongZ);
			//printf("%lf\n",alongX*upX+alongY*upY+alongZ*upZ);


			//printf("%lf\n",r);
			//printf("%lf,%lf,%lf\n",dirX,dirY,dirZ);
			//printf("%lf,%lf,%lf\n",alongX,alongY,alongZ);
			//printf("%lf,%lf,%lf\n",upX,upY,upZ);
			//printf("%lf,%lf,%lf\n\n",lookX,lookY,lookZ);
			break;

		case '4':				
			//cameraAngleZ=cameraAngleZ-.01;
			//lookX=cameraForward+cameraRadius*cos(cameraAngleZ);
			//lookZ=cameraUp+cameraRadius*sin(cameraAngleZ);
			
			r=sqrt((cameraForward-lookX)*(cameraForward-lookX)+(cameraAlong-lookY)*(cameraAlong-lookY)+(cameraUp-lookZ)*(cameraUp-lookZ));

			dirX=cameraForward-lookX;
			dirY=cameraAlong-lookY;
			dirZ=cameraUp-lookZ;
			
			alongX=-dirY*upZ+dirZ*upY;
			alongY=-dirZ*upX+dirX*upZ;
			alongZ=-dirX*upY+dirY*upX;

			modForward=1/sqrt(dirX*dirX+dirY*dirY+dirZ*dirZ);
			modUp=1/sqrt(upX*upX+upY*upY+upZ*upZ);
			angle=180*3.1416/180-.1;

			lookX=cameraForward+r*dirX*modForward*cos(angle)+r*upX*modUp*sin(angle);
			lookY=cameraAlong+r*dirY*modForward*cos(angle)+r*upY*modUp*sin(angle);
			lookZ=cameraUp+r*dirZ*modForward*cos(angle)+r*upZ*modUp*sin(angle);
			
			dirX=cameraForward-lookX;
			dirY=cameraAlong-lookY;
			dirZ=cameraUp-lookZ;

			upX=dirY*alongZ-dirZ*alongY;
			upY=dirZ*alongX-dirX*alongZ;
			upZ=dirX*alongY-dirY*alongX;

			modUp=1/sqrt(upX*upX+upY*upY+upZ*upZ);

			upX*=modUp;
			upY*=modUp;
			upZ*=modUp;

			//printf("%lf\n",dirX*upX+dirY*upY+dirZ*upZ);
			//printf("%lf\n",dirX*alongX+dirY*alongY+dirZ*alongZ);
			//printf("%lf\n",alongX*upX+alongY*upY+alongZ*upZ);


			//printf("%lf\n",r);
			//printf("%lf,%lf,%lf\n",dirX,dirY,dirZ);
			//printf("%lf,%lf,%lf\n",alongX,alongY,alongZ);
			//printf("%lf,%lf,%lf\n",upX,upY,upZ);
			//printf("%lf,%lf,%lf\n\n",lookX,lookY,lookZ);
			
			break;

		case '5':
			/*rollAngle=rollAngle+.01;
			upX=cos(rollAngle);
			upZ=sin(rollAngle);
			printf("%lf,%lf\n",upX,upZ);*/

			r=sqrt((cameraForward-lookX)*(cameraForward-lookX)+(cameraAlong-lookY)*(cameraAlong-lookY)+(cameraUp-lookZ)*(cameraUp-lookZ));
			
			dirX=cameraForward-lookX;
			dirY=cameraAlong-lookY;
			dirZ=cameraUp-lookZ;
			
			alongX=-dirY*upZ+dirZ*upY;
			alongY=-dirZ*upX+dirX*upZ;
			alongZ=-dirX*upY+dirY*upX;

			modAlong=1/sqrt(alongX*alongX+alongY*alongY+alongZ*alongZ);
			modUp=1/sqrt(upX*upX+upY*upY+upZ*upZ);
			angle=0*3.1416/180+.01;

			upPointX=cameraForward+r*upX*modUp*cos(angle)+r*alongX*modAlong*sin(angle);
			upPointY=cameraAlong+r*upY*modUp*cos(angle)+r*alongY*modAlong*sin(angle);
			upPointZ=cameraUp+r*upZ*modUp*cos(angle)+r*alongZ*modAlong*sin(angle);
			
			/*dirX=cameraForward-lookX;
			dirY=cameraAlong-lookY;
			dirZ=cameraUp-lookZ;

			upX=dirY*alongZ-dirZ*alongY;
			upY=dirZ*alongX-dirX*alongZ;
			upZ=dirX*alongY-dirY*alongX;*/

			upX=upPointX-cameraForward;
			upY=upPointY-cameraAlong;
			upZ=upPointZ-cameraUp;
			
			modUp=1/sqrt(upX*upX+upY*upY+upZ*upZ);

			upX*=modUp;
			upY*=modUp;
			upZ*=modUp;

			//printf("%lf\n",dirX*upX+dirY*upY+dirZ*upZ);
			//printf("%lf\n",dirX*alongX+dirY*alongY+dirZ*alongZ);
			//printf("%lf\n",alongX*upX+alongY*upY+alongZ*upZ);


			//printf("%lf\n",r);
			//printf("%lf,%lf,%lf\n",dirX,dirY,dirZ);
			//printf("%lf,%lf,%lf\n",alongX,alongY,alongZ);
			//printf("%lf,%lf,%lf\n",upX,upY,upZ);
			//printf("%lf,%lf,%lf\n\n",lookX,lookY,lookZ);

			break;

		case '6':	
			r=sqrt((cameraForward-lookX)*(cameraForward-lookX)+(cameraAlong-lookY)*(cameraAlong-lookY)+(cameraUp-lookZ)*(cameraUp-lookZ));
			
			dirX=cameraForward-lookX;
			dirY=cameraAlong-lookY;
			dirZ=cameraUp-lookZ;
			
			alongX=-dirY*upZ+dirZ*upY;
			alongY=-dirZ*upX+dirX*upZ;
			alongZ=-dirX*upY+dirY*upX;

			modAlong=1/sqrt(alongX*alongX+alongY*alongY+alongZ*alongZ);
			modUp=1/sqrt(upX*upX+upY*upY+upZ*upZ);
			angle=0*3.1416/180-.01;

			upPointX=cameraForward+r*upX*modUp*cos(angle)+r*alongX*modAlong*sin(angle);
			upPointY=cameraAlong+r*upY*modUp*cos(angle)+r*alongY*modAlong*sin(angle);
			upPointZ=cameraUp+r*upZ*modUp*cos(angle)+r*alongZ*modAlong*sin(angle);
			
			/*dirX=cameraForward-lookX;
			dirY=cameraAlong-lookY;
			dirZ=cameraUp-lookZ;

			upX=dirY*alongZ-dirZ*alongY;
			upY=dirZ*alongX-dirX*alongZ;
			upZ=dirX*alongY-dirY*alongX;*/

			upX=upPointX-cameraForward;
			upY=upPointY-cameraAlong;
			upZ=upPointZ-cameraUp;
			
			modUp=1/sqrt(upX*upX+upY*upY+upZ*upZ);

			upX*=modUp;
			upY*=modUp;
			upZ*=modUp;

			//printf("%lf\n",dirX*upX+dirY*upY+dirZ*upZ);
			//printf("%lf\n",dirX*alongX+dirY*alongY+dirZ*alongZ);
			//printf("%lf\n",alongX*upX+alongY*upY+alongZ*upZ);


			//printf("%lf\n",r);
			//printf("%lf,%lf,%lf\n",dirX,dirY,dirZ);
			//printf("%lf,%lf,%lf\n",alongX,alongY,alongZ);
			//printf("%lf,%lf,%lf\n",upX,upY,upZ);
			//printf("%lf,%lf,%lf\n\n",lookX,lookY,lookZ);
			break;

		case '8':	//toggle grids
			canDrawGrid = 1 - canDrawGrid;
			break;


		case 'w':
			cameraAngleX += .01;
			cameraAngleY +=.01;
			break;

		case 's':
			cameraAngleX -= .01;
			cameraAngleY -=.01;
			break;

		case 'a':
			cameraAngle+=.01;
			break;


		case 'd':
			cameraAngle-=.01;
			break;
	
		case 'x':
			cameraRadius+=1;
			break;

		case 'c':
			cameraRadius-=1;
			break;

		case 'v':
			cameraHeight+=10;
			lookZ=cameraHeight*sin(cameraAngle);
			break;

		case 'b':
			cameraHeight-=10;
			lookZ=cameraHeight*sin(cameraAngle);
			break;
		case 27:	//ESCAPE KEY -- simply exit
			exit(0);
			break;

		default:
			break;
	}
}

void specialKeyListener(int key, int x,int y){
	
	double dirX,dirY,dirZ;
	double alongX,alongY,alongZ;
	double newLookX,newLookY,newLookZ;
	double normalize;

	switch(key){
		case GLUT_KEY_DOWN:		//down arrow key
			//cameraForward=cameraForward+5;
			//lookX+=5;
			//printf("%lf,%lf,%lf %lf,%lf,%lf %lf,%lf,%lf\n",dirX,dirY,dirZ,lookX,lookY,lookZ,cameraForward,cameraAlong,cameraUp);
			
			dirX=cameraForward-lookX;
			dirY=cameraAlong-lookY;
			dirZ=cameraUp-lookZ;

			newLookX=lookX+.5*dirX;
			newLookY=lookY+.5*dirY;
			newLookZ=lookZ+.5*dirZ;

			cameraForward=lookX+1.5*dirX;
			cameraAlong=lookY+1.5*dirY;
			cameraUp=lookZ+1.5*dirZ;

			lookX=newLookX;
			lookY=newLookY;
			lookZ=newLookZ;
					
			break;
		case GLUT_KEY_UP:		// up arrow key
			dirX=cameraForward-lookX;
			dirY=cameraAlong-lookY;
			dirZ=cameraUp-lookZ;

			newLookX=lookX-.5*dirX;
			newLookY=lookY-.5*dirY;
			newLookZ=lookZ-.5*dirZ;

			cameraForward=lookX+.5*dirX;
			cameraAlong=lookY+.5*dirY;
			cameraUp=lookZ+.5*dirZ;

			lookX=newLookX;
			lookY=newLookY;
			lookZ=newLookZ;
			
			break;

		case GLUT_KEY_RIGHT:
			//cameraAlong=cameraAlong+5;
			//lookY+=5;

			//printf("%lf,%lf,%lf\n",cameraForward,cameraAlong,cameraUp);
			
			dirX=cameraForward-lookX;
			dirY=cameraAlong-lookY;
			dirZ=cameraUp-lookZ;

			alongX=dirY*upZ-dirZ*upY;
			alongY=dirZ*upX-dirX*upZ;
			alongZ=dirX*upY-dirY*upX;

			normalize=1/sqrt(alongX*alongX+alongY*alongY+alongZ*alongZ);

			cameraForward=cameraForward-normalize*alongX;
			cameraAlong=cameraAlong-normalize*alongY;
			cameraUp=cameraUp-normalize*alongZ;
			
			lookX=lookX-normalize*alongX;
			lookY=lookY-normalize*alongY;
			lookZ=lookZ-normalize*alongZ;

			//printf("%lf,%lf,%lf\n",cameraForward,cameraAlong,cameraUp);
			//printf("%lf,%lf,%lf\n",lookX,lookY,lookZ);

			break;
		case GLUT_KEY_LEFT:
			//cameraAlong=cameraAlong-5;
			//lookY-=5;

			dirX=cameraForward-lookX;
			dirY=cameraAlong-lookY;
			dirZ=cameraUp-lookZ;

			alongX=dirY*upZ-dirZ*upY;
			alongY=dirZ*upX-dirX*upZ;
			alongZ=dirX*upY-dirY*upX;

			normalize=1/sqrt(alongX*alongX+alongY*alongY+alongZ*alongZ);

			cameraForward=cameraForward+normalize*alongX;
			cameraAlong=cameraAlong+normalize*alongY;
			cameraUp=cameraUp+normalize*alongZ;
			
			lookX=lookX+normalize*alongX;
			lookY=lookY+normalize*alongY;
			lookZ=lookZ+normalize*alongZ;

			//printf("%lf,%lf,%lf\n",alongX,alongY,alongZ);
			//printf("%lf,%lf,%lf\n",cameraForward,cameraAlong,cameraUp);
			//printf("%lf,%lf,%lf\n",lookX,lookY,lookZ);
			break;

		case GLUT_KEY_PAGE_UP:
			//cameraUp=cameraUp+2;
			//lookZ+=2;

			normalize=1/sqrt(upX*upX+upY*upY+upZ*upZ);

			cameraForward=cameraForward+normalize*upX;
			cameraAlong=cameraAlong+normalize*upY;
			cameraUp=cameraUp+normalize*upZ;
			
			lookX=lookX+normalize*upX;
			lookY=lookY+normalize*upY;
			lookZ=lookZ+normalize*upZ;
			//printf("%lf,%lf,%lf\n",upX,upY,upZ);
			//printf("%lf\n",normalize);
			//printf("%lf,%lf,%lf\n",cameraForward,cameraAlong,cameraUp);
			//printf("%lf,%lf,%lf\n",lookX,lookY,lookZ);

			break;
		case GLUT_KEY_PAGE_DOWN:
			//cameraUp=cameraUp-2;
			//lookZ-=2;

			normalize=1/sqrt(upX*upX+upY*upY+upZ*upZ);

			cameraForward=cameraForward-normalize*upX;
			cameraAlong=cameraAlong-normalize*upY;
			cameraUp=cameraUp-normalize*upZ;
			
			lookX=lookX-normalize*upX;
			lookY=lookY-normalize*upY;
			lookZ=lookZ-normalize*upZ;
			//printf("%lf,%lf,%lf\n",upX,upY,upZ);
			//printf("%lf\n",normalize);
			//printf("%lf,%lf,%lf\n",cameraForward,cameraAlong,cameraUp);
			//printf("%lf,%lf,%lf\n",lookX,lookY,lookZ);

			break;

		case GLUT_KEY_INSERT:
			break;

		case GLUT_KEY_HOME:
			cameraAngleX+=.01;
			cameraAngleY+=.01;
			break;
		case GLUT_KEY_END:
			cameraAngleX-=.01;
			cameraAngleY-=.01;
			break;

		default:
			break;
	}
}

void mouseListener(int button, int state, int x, int y){	//x, y is the x-y of the screen (2D)
	switch(button){
		case GLUT_LEFT_BUTTON:
			if(state == GLUT_DOWN){		// 2 times?? in ONE click? -- solution is checking DOWN or UP
				//cameraAngleDelta = -cameraAngleDelta;	
			}
			break;

		case GLUT_RIGHT_BUTTON:
			//........
			break;

		case GLUT_MIDDLE_BUTTON:
			//........
			break;

		default:
			break;
	}
}


void init(){
	//codes for initialization
	/*cameraAngle = 0;	//// init the cameraAngle
	cameraAngleDelta = 0.002;
	globeAngle=0;
	canDrawGrid = true;
	cameraHeight = 150;
	cameraRadius = 150;
	*/
	
	/************************
	camera initialization
	************************/
	//codes for initialization
	cameraAngle = 0;	//// init the cameraAngle
	//cameraAngleDelta = 0.002;
	rectAngle = 0;
	canDrawGrid = true;
	cameraHeight = 0;
	cameraRadius = 10;

	cameraForward=40;
	cameraAlong=250;
	cameraUp=70;

	lookX=0;
	lookY=0;
	lookZ=0;

	upX=0;
	upY=0;
	upZ=1;
	rollAngle=0;
	cameraRadiusX=50;
	cameraRadiusY=50;
	cameraRadiusZ=50;
	cameraAngleX=180*3.1416/180;
	cameraAngleY=0;
	cameraAngleZ=(180-atan(10.0/50))*3.1416/180;

	
	
	loadBmp();
	

	//clear the screen
	glClearColor(BLACK, 0);

	/************************
	/ set-up projection here
	************************/
	//load the PROJECTION matrix
	glMatrixMode(GL_PROJECTION);
	
	//initialize the matrix
	glLoadIdentity();

	//give PERSPECTIVE parameters
	gluPerspective(70,	1,	0.1,	10000.0);
	//field of view in the Y (vertically)
	//aspect ratio that determines the field of view in the X direction (horizontally)
	//near distance
	//far distance
	
	/*
	glShadeModel(GL_SMOOTH);

	GLfloat lmodel_ambient[] = { 1, 1, 1, 1.0 }; //color of the global ambient light
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

	glEnable(GL_NORMALIZE); //Automatically normalize normals needed by the illumination model
    glEnable(GL_LIGHTING);
	*/
}

int main(int argc, char **argv){
	glutInit(&argc,argv);
	glutInitWindowSize(900, 900);
	glutInitWindowPosition(0, 0);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGB);	//Depth, Double buffer, RGB color

	glutCreateWindow("TAJ MAHAL ****");

	init();

	glEnable(GL_DEPTH_TEST);	//enable Depth Testing

	glutDisplayFunc(display);	//display callback function
	glutIdleFunc(animate);		//what you want to do in the idle time (when no drawing is occuring)

	//ADD keyboard listeners:
	glutKeyboardFunc(keyboardListener);
	glutSpecialFunc(specialKeyListener);

	//ADD mouse listeners:
	glutMouseFunc(mouseListener);

	glutMainLoop();		//The main loop of OpenGL

	return 0;
}



void drawGrid(){
	int i;

	//WILL draw grid IF the "canDrawGrid" is true:

	if(canDrawGrid == 1){
		glColor3f(0.3, 0.3, 0.3);	//grey
		glBegin(GL_LINES);{
			for(i=-10;i<=10;i++){

				if(i==0)
					continue;	//SKIP the MAIN axes

				//lines parallel to Y-axis
				glVertex3f(i*10, -100, 0);
				glVertex3f(i*10,  100, 0);

				//lines parallel to X-axis
				glVertex3f(-100, i*10, 0);
				glVertex3f( 100, i*10, 0);

			}
		}glEnd();
		// draw the two AXES
		//glColor3f(1, 1, 1);	//100% white
		glColor3f(0.3, 0.3, 0.3);
		glBegin(GL_LINES);{
			//Y axis
			glVertex3f(0, -150, 0);	// intentionally extended to -150 to 150, no big deal
			glVertex3f(0,  150, 0);

			//X axis
			glVertex3f(-150, 0, 0);
			glVertex3f( 150, 0, 0);
		}glEnd();
	}

	
}

void drawCameraTest(){
	glPushMatrix();{		
			glColor4f(0, .7 ,.9,.8);
			glTranslatef(0,-5,0);
			//glRotatef(180,0,1,0);
			glutSolidCone(1,.5,50,50);		
	}glPopMatrix();

	glPushMatrix();{		
			glColor4f(0, .7 ,.9,.8);
			glTranslatef(0,0,0);
			glutSolidCube(3);		
	}glPopMatrix();

	glPushMatrix();{		
			glColor4f(0, .7 ,.9,.8);
			glTranslatef(0,5,0);
			glutSolidSphere(2.5,50,50);		
	}glPopMatrix();

}

void glutCircle(float cx, float cy, float r, int num_segments) {
    glBegin(GL_LINE_LOOP);
    for (int i = 0; i < num_segments; i++)   {
        float theta = 2.0f * 3.1415926f * float(i) / float(num_segments);//get the current angle 
        float x = r * cosf(theta);//calculate the x component 
        float y = r * sinf(theta);//calculate the y component 
        glVertex2f(x + cx, y + cy);//output vertex 
    }
    glEnd();
}

void glutSolidCircle(float r, int num_segments) {
    int i=0;
	float theta=(i*2*M_PI)/num_segments;
	float x1=r*cos(theta);
	float y1=r*sin(theta);
	for(i=1;i<=num_segments;i++){
		theta=(i*2*M_PI)/num_segments;
		float x2=r*cos(theta);
		float y2=r*sin(theta);
		
		glBegin(GL_TRIANGLES);{
			glVertex3f(0,0,0);
			glVertex3f(x1,y1,0);
			glVertex3f(x2,y2,0);
		}glEnd();
		
		x1=x2;
		y1=y2;

	}
}

void glutSolidCylinder(float r, float height, int nothing, int num_segments) {
    int i=0;
	float theta=(i*2*M_PI)/num_segments;
	float x1=r*cos(theta);
	float y1=r*sin(theta);
	for(i=1;i<=num_segments;i++){
		theta=(i*2*M_PI)/num_segments;
		float x2=r*cos(theta);
		float y2=r*sin(theta);
		
		glBegin(GL_TRIANGLES);{
			glVertex3f(0,0,0);
			glVertex3f(x1,y1,0);
			glVertex3f(x2,y2,0);
		}glEnd();


		glBegin(GL_TRIANGLES);{
			glVertex3f(0,0,height);
			glVertex3f(x1,y1,height);
			glVertex3f(x2,y2,height);
		}glEnd();
		
		glBegin(GL_QUADS);{
			glVertex3f(x1,y1,0);
			glVertex3f(x2,y2,0);
			glVertex3f(x2,y2,height);
			glVertex3f(x1,y1,height);
		}glEnd();


		x1=x2;
		y1=y2;

	}
}

void glutWireCylinder(float r, float height, int slices, int num_segments) {
    int i=0;
	float theta=(i*2*M_PI)/num_segments;
	float x1=r*cos(theta);
	float y1=r*sin(theta);
	for(i=1;i<=num_segments;i++){
		theta=(i*2*M_PI)/num_segments;
		float x2=r*cos(theta);
		float y2=r*sin(theta);
		
		/*glBegin(GL_TRIANGLES);{
			glVertex3f(0,0,0);
			glVertex3f(x1,y1,0);
			glVertex3f(x2,y2,0);
		}glEnd();


		glBegin(GL_TRIANGLES);{
			glVertex3f(0,0,height);
			glVertex3f(x1,y1,height);
			glVertex3f(x2,y2,height);
		}glEnd();
		*/
		float slice_height=height/(slices*1.0);
		float h=0;
		for(int j=0;j<=slices;j++){
			
			glBegin(GL_LINES);{
				glVertex3f(x1,y1,h);
				glVertex3f(x2,y2,h);
			}glEnd();
			h+=slice_height;
		}


		
		glBegin(GL_LINES);{
			glVertex3f(x1,y1,0);
			glVertex3f(x1,y1,height);
		}glEnd();


		glBegin(GL_LINES);{
			glVertex3f(x2,y2,0);
			glVertex3f(x2,y2,height);
		}glEnd();

		x1=x2;
		y1=y2;

	}
}

int LoadBitmap(char *filename)
{
    int i, j=0;
    FILE *l_file;
    unsigned char *l_texture;

    BITMAPFILEHEADER fileheader;
    BITMAPINFOHEADER infoheader;
    RGBTRIPLE rgb;

    num_texture++;

    if( (l_file = fopen(filename, "rb"))==NULL) return (-1);

    fread(&fileheader, sizeof(fileheader), 1, l_file);

    fseek(l_file, sizeof(fileheader), SEEK_SET);
    fread(&infoheader, sizeof(infoheader), 1, l_file);

    l_texture = (byte *) malloc(infoheader.biWidth * infoheader.biHeight * 4);
    memset(l_texture, 0, infoheader.biWidth * infoheader.biHeight * 4);

 for (i=0; i < infoheader.biWidth*infoheader.biHeight; i++)
    {
            fread(&rgb, sizeof(rgb), 1, l_file);

            l_texture[j+0] = rgb.rgbtRed;
            l_texture[j+1] = rgb.rgbtGreen;
            l_texture[j+2] = rgb.rgbtBlue;
            l_texture[j+3] = 255;
            j += 4;
    }
    fclose(l_file);

    glBindTexture(GL_TEXTURE_2D, num_texture);

    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);

// glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    glTexImage2D(GL_TEXTURE_2D, 0, 4, infoheader.biWidth, infoheader.biHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, l_texture);
     gluBuild2DMipmaps(GL_TEXTURE_2D, 4, infoheader.biWidth, infoheader.biHeight, GL_RGBA, GL_UNSIGNED_BYTE, l_texture);

    free(l_texture);

    return (num_texture);

}

void loadBmp(void)
{

	texid1 =LoadBitmap("brickwall.bmp");   /*here bkg1.bmp is the bitmap image to be used as texture, texid is global varible declared to uniquely  identify this particular image*/
	minarBmp =LoadBitmap("minar.bmp");
	minarBaseBmp =LoadBitmap("minarBase.bmp");
	thinMinarBmp =LoadBitmap("thinMinar.bmp");
	floorBmp =LoadBitmap("floor.bmp");
	whiteFloorBmp =LoadBitmap("whiteFloor.bmp");
	roofBmp =LoadBitmap("roof.bmp");
	minarBaseFloorBmp =LoadBitmap("minarBaseFloor.bmp");
	floorSideBmp =LoadBitmap("floorSide.bmp"); 
	redBaseOctBmp =LoadBitmap("redBaseOct.bmp"); 
	redBaseSideBmp =LoadBitmap("redBaseSide.bmp"); 
	redFatMinarBmp=LoadBitmap("redFatMinar.bmp"); 
	redMinarRoadBmp=LoadBitmap("redMinarRoad.bmp"); 
	redMinarRoadTopBmp=LoadBitmap("redMinarRoadTop.bmp"); 
	redMinarRoadThinBmp=LoadBitmap("redMinarRoadThin.bmp");
	redFloorBmp=LoadBitmap("redFloor.bmp");
}

void setSideDesign(){
	glPushMatrix();{
		glTranslatef(-4.5,0,0);
		glScalef(0.89,1,1);
		glPushMatrix();{
			glTranslatef(-9,46,0.7);
			floorSideDesign();
		}glPopMatrix();
		glPushMatrix();{
			glTranslatef(23,46,0.7);
			floorSideDesign();
		}glPopMatrix();
		glPushMatrix();{
			glTranslatef(51,46,0.7);
			floorSideDesign();
		}glPopMatrix();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-4.5,0,0);
		glScalef(0.89,1,1);
		glPushMatrix();{
			glTranslatef(-9,-46,0.7);
			floorSideDesign();
		}glPopMatrix();
		glPushMatrix();{
			glTranslatef(23,-46,0.7);
			floorSideDesign();
		}glPopMatrix();
		glPushMatrix();{
			glTranslatef(51,-46,0.7);
			floorSideDesign();
		}glPopMatrix();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-2,-2.8,0);
		glRotatef(90,0,0,1);
		glScalef(0.89,1,1);
		glPushMatrix();{
			glTranslatef(-9,46,0.7);
			floorSideDesign();
		}glPopMatrix();
		glPushMatrix();{
			glTranslatef(23,46,0.7);
			floorSideDesign();
		}glPopMatrix();
		glPushMatrix();{
			glTranslatef(51,46,0.7);
			floorSideDesign();
		}glPopMatrix();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(2,2.8,0);
		glRotatef(-90,0,0,1);
		glScalef(0.89,1,1);
		glPushMatrix();{
			glTranslatef(-9,46,0.7);
			floorSideDesign();
		}glPopMatrix();
		glPushMatrix();{
			glTranslatef(23,46,0.7);
			floorSideDesign();
		}glPopMatrix();
		glPushMatrix();{
			glTranslatef(51,46,0.7);
			floorSideDesign();
		}glPopMatrix();
	}glPopMatrix();

	/*
	glPushMatrix();{
		glTranslatef(0,4,0);
		glScalef(0.89,1,1);
		glPushMatrix();{
			glTranslatef(16
				,46,0.7);
			floorSideDesign();
		}glPopMatrix();
	}glPopMatrix();
	*/
	glPushMatrix();{
		glTranslatef(0,0,-.2);
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, minarBaseBmp); // here texid corresponds a bitmap image.
		gluQuadricNormals(IDquadric,GLU_SMOOTH);
		gluQuadricTexture(IDquadric, GLU_TRUE);
		glColor3f(0.8,0.8,0.8);
		glPushMatrix();{
			glTranslatef(1.8,46.2,0.6);
			glRotatef(45,0,0,1);
			//glScalef(.15,.1,.15);
			gluCylinder(IDquadric,5,5,9.5,4,20);
		}glPopMatrix();
		glPushMatrix();{
			glTranslatef(-5.3,46.2,0.6);
			glRotatef(45,0,0,1);
			//glScalef(.15,.1,.15);
			gluCylinder(IDquadric,5,5,9.5,4,20);
		}glPopMatrix();
		glPushMatrix();{
			glTranslatef(5.3,46.2,0.6);
			glRotatef(45,0,0,1);
			//glScalef(.15,.1,.15);
			gluCylinder(IDquadric,5,5,9.5,4,20);
		}glPopMatrix();
		glDisable(GL_TEXTURE_2D);
	}glPopMatrix();
	
	//railing
	float dist=0;
	for(int c=0;c<9;c++){
		glPushMatrix();{
			glTranslatef(39.1-dist,46,10);
			glRotatef(180,0,0,1);
			glScalef(0.24,0.24,0.24);
			floorRailing();
		}glPopMatrix();
		dist=dist+3.55;
	}
	dist=0;
	for(c=0;c<5;c++){
		glPushMatrix();{
			glTranslatef(7.15-dist,46+3.7,10);
			glRotatef(180,0,0,1);
			glScalef(0.24,0.24,0.24);
			floorRailing();
		}glPopMatrix();
		dist=dist+3.55;
	}
	
	glPushMatrix();{
		glTranslatef(8.6,46+1.5,10);
		glRotatef(90,0,0,1);
		glScalef(0.24,0.24,0.24);
		floorRailing();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-8.6,46+1.5,10);
		glRotatef(-90,0,0,1);
		glScalef(0.24,0.24,0.24);
		floorRailing();
	}glPopMatrix();

	dist=0;
	for(c=0;c<9;c++){
		glPushMatrix();{
			glTranslatef(-10.7-dist,46,10);
			glRotatef(180,0,0,1);
			glScalef(0.24,0.24,0.24);
			floorRailing();
		}glPopMatrix();
		dist=dist+3.55;
	}
	
	dist=0;
	for(c=0;c<23;c++){
		glPushMatrix();{
			glTranslatef(39.1-dist,-46,10);
			//glRotatef(180,0,0,1);
			glScalef(0.24,0.24,0.24);
			floorRailing();
		}glPopMatrix();
		dist=dist+3.55;
	}
	dist=0;
	for(c=0;c<22;c++){
		glPushMatrix();{
			glTranslatef(48,37.3-dist,10);
			glRotatef(90,0,0,1);
			glScalef(0.24,0.24,0.24);
			floorRailing();
		}glPopMatrix();
		dist=dist+3.55;
	}
	dist=0;
	for(c=0;c<22;c++){
		glPushMatrix();{
			glTranslatef(-48,37.3-dist,10);
			glRotatef(-90,0,0,1);
			glScalef(0.24,0.24,0.24);
			floorRailing();
		}glPopMatrix();
		dist=dist+3.55;
	}

	//minar base railings
	glPushMatrix();{
		glTranslatef(46,46+2.5,10);
		glRotatef(180,0,0,1);
		glScalef(0.25,0.25,0.25);
		floorRailing();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(50,44,10);
		glRotatef(90,0,0,1);
		glScalef(0.25,0.25,0.25);
		floorRailing();
	}glPopMatrix();
	
	glPushMatrix();{
		glTranslatef(46+3,46+1,10);
		glRotatef(135,0,0,1);
		glScalef(0.24,0.24,0.24);
		floorRailing();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(49,41,10);
		glRotatef(45,0,0,1);
		glScalef(0.24,0.24,0.24);
		floorRailing();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(43,47,10);
		glRotatef(45,0,0,1);
		glScalef(0.24,0.24,0.24);
		floorRailing();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(-46,-46-2.5,10);
		glRotatef(180,0,0,1);
		glScalef(0.25,0.25,0.25);
		floorRailing();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-50,-44,10);
		glRotatef(90,0,0,1);
		glScalef(0.25,0.25,0.25);
		floorRailing();
	}glPopMatrix();
	
	glPushMatrix();{
		glTranslatef(-46-3,-46-1,10);
		glRotatef(135,0,0,1);
		glScalef(0.24,0.24,0.24);
		floorRailing();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-49,-41,10);
		glRotatef(45,0,0,1);
		glScalef(0.24,0.24,0.24);
		floorRailing();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-43,-47,10);
		glRotatef(45,0,0,1);
		glScalef(0.24,0.24,0.24);
		floorRailing();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(-46,46+2.5,10);
		glRotatef(-180,0,0,1);
		glScalef(0.25,0.25,0.25);
		floorRailing();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-50,44,10);
		glRotatef(-90,0,0,1);
		glScalef(0.25,0.25,0.25);
		floorRailing();
	}glPopMatrix();
	
	glPushMatrix();{
		glTranslatef(-46-3,46+1,10);
		glRotatef(-135,0,0,1);
		glScalef(0.24,0.24,0.24);
		floorRailing();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-49,41,10);
		glRotatef(-45,0,0,1);
		glScalef(0.24,0.24,0.24);
		floorRailing();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-43,47,10);
		glRotatef(-45,0,0,1);
		glScalef(0.24,0.24,0.24);
		floorRailing();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(46,-46-2.5,10);
		glRotatef(-180,0,0,1);
		glScalef(0.25,0.25,0.25);
		floorRailing();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(50,-44,10);
		glRotatef(-90,0,0,1);
		glScalef(0.25,0.25,0.25);
		floorRailing();
	}glPopMatrix();
	
	glPushMatrix();{
		glTranslatef(46+3,-46-1,10);
		glRotatef(-135,0,0,1);
		glScalef(0.24,0.24,0.24);
		floorRailing();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(49,-41,10);
		glRotatef(-45,0,0,1);
		glScalef(0.24,0.24,0.24);
		floorRailing();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(43,-47,10);
		glRotatef(-45,0,0,1);
		glScalef(0.24,0.24,0.24);
		floorRailing();
	}glPopMatrix();
}

void setBase(){
	glPushMatrix();{
		glTranslatef(57,0,0);
		drawBase();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-57,0,0);
		glRotatef(180,0,0,1);
		drawBase();
	}glPopMatrix();
}

void drawBase(){
	glColor3f(1,1,1);
	glPushMatrix();{
		//glTranslatef(71,0,0);
		glPushMatrix();{
			//glRotatef(-90,0,1,0);
			glEnable(GL_TEXTURE_2D);
			glBindTexture(GL_TEXTURE_2D,redFloorBmp);
			glNormal3f(1.0,0.0,0.0);
			glBegin(GL_QUADS);{
				glTexCoord2f(0,0);glVertex3f(0,10,0);
				glTexCoord2f(0,1);glVertex3f(0,46,0);
				glTexCoord2f(1,1);glVertex3f(50,46,0);
				glTexCoord2f(1,0);glVertex3f(50,10,0);
				//glVertex3f(0,10,0);
				//glVertex3f(0,46,0);
				//glVertex3f(50,46,0);
				//glVertex3f(50,10,0);
			}glEnd();
			glBegin(GL_QUADS);{
				glTexCoord2f(0,0);glVertex3f(0,-10,0);
				glTexCoord2f(0,1);glVertex3f(0,-46,0);
				glTexCoord2f(1,1);glVertex3f(50,-46,0);
				glTexCoord2f(1,0);glVertex3f(50,-10,0);
				/*glVertex3f(0,-10,0);
				glVertex3f(0,-46,0);
				glVertex3f(50,-46,0);
				glVertex3f(50,-10,0);*/
			}glEnd();
			glBegin(GL_QUADS);{
				glTexCoord2f(0,0);glVertex3f(0,-10,0);
				glTexCoord2f(0,0.4);glVertex3f(0,10,0);
				glTexCoord2f(0.4,0.4);glVertex3f(15,10,0);
				glTexCoord2f(0.4,0);glVertex3f(15,-10,0);
				//glVertex3f(0,-10,0);
				//glVertex3f(0,10,0);
				//glVertex3f(15,10,0);
				//glVertex3f(15,-10,0);
			}glEnd();
			glBegin(GL_QUADS);{
				glTexCoord2f(0,0);glVertex3f(35,-10,0);
				glTexCoord2f(0,0.4);glVertex3f(35,10,0);
				glTexCoord2f(0.4,0.4);glVertex3f(50,10,0);
				glTexCoord2f(0.4,0);glVertex3f(50,-10,0);
				/*glVertex3f(35,-10,0);
				glVertex3f(35,10,0);
				glVertex3f(50,10,0);
				glVertex3f(50,-10,0);*/
			}glEnd();
			glDisable(GL_TEXTURE_2D);
		}glPopMatrix();
		glColor3f(0.37,0.2,0.2);
		glPushMatrix();{
			glTranslatef(25,51,-5);
			glScalef(50,10,12);
			drawUnitSolidCube();
		}glPopMatrix();
		glPushMatrix();{
			glTranslatef(25,-51,-5);
			glScalef(50,10,12);
			drawUnitSolidCube();
		}glPopMatrix();
		glPushMatrix();{
			glTranslatef(55,0,-5);
			glScalef(10,112,12);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glTranslatef(82-5,43-2,-5);
			glScalef(44-10,26+4,12);
			drawUnitSolidCube();
		}glPopMatrix();
		glPushMatrix();{
			glTranslatef(82-5,-43+2,-5);
			glScalef(44-10,26+4,12);
			drawUnitSolidCube();
		}glPopMatrix();

		glColor3f(0.35,0.1,0.1);
		glPushMatrix();{
			glTranslatef(108-10,0,-4);
			glScalef(10,112,12);
			drawUnitSolidCube();
		}glPopMatrix();
		glPushMatrix();{
			glTranslatef(82-5,0,-4);
			glScalef(34,60-8,12); //52 was 60
			drawUnitSolidCube();
		}glPopMatrix();
		
		//small 
		glPushMatrix();{
			glTranslatef(25,0,-9);
			glRotatef(45,0,0,1);
			gluCylinder(IDquadric,14.14,14.14,10,4,20);
			gluCylinder(IDquadric,9.14,9.14,10,4,20);
		}glPopMatrix();
		glPushMatrix();{
			glTranslatef(25,0,10-9);
			glRotatef(45,0,0,1);
			gluDisk(IDquadric,9.14,14.14,4,20);
		}glPopMatrix();
		
		glColor3f(1,1,1);
		glPushMatrix();{
			glTranslatef(25,0,-1);
			glRotatef(45,0,0,1);
			//gluCylinder(IDquadric,14.14,14.14,10,4,20);
			gluCylinder(IDquadric,8.14,8.14,1,4,20);
		}glPopMatrix();
		glPushMatrix();{
			glTranslatef(25,0,1-1);
			glRotatef(45,0,0,1);
			gluDisk(IDquadric,8.14,9.14,4,20);
		}glPopMatrix();

		glColor3f(0.5,0.5,0.1);
		glPushMatrix();{
			glTranslatef(25,0,-4);
			glRotatef(45,0,0,1);
			//gluCylinder(IDquadric,14.14,14.14,10,4,20);
			gluCylinder(IDquadric,8.14,8.14,3,4,20);
		}glPopMatrix();
		glPushMatrix();{
			glTranslatef(25,0,3-4);
			glRotatef(45,0,0,1);
			gluDisk(IDquadric,8.14,9.14,4,20);
		}glPopMatrix();

		glColor3f(0.5,0.5,0.4);
		glPushMatrix();{
			glBegin(GL_QUADS);{
				//glTexCoord2f(0,0);glVertex3f(0,0,0);
				//glTexCoord2f(0,1);glVertex3f(9.5,0,0);
				//glTexCoord2f(1,1);glVertex3f(9.5,0,32);
				//glTexCoord2f(1,0);glVertex3f(0,0,32);
				glVertex3f(15,-10,-4);
				glVertex3f(15,10,-4);
				glVertex3f(35,10,-4);
				glVertex3f(35,-10,-4);
			}glEnd();
		}glPopMatrix();

		//sides
		glColor3f(0.35,0,0);
		glPushMatrix();{
			glTranslatef(80.5-57,56.5,-8+2);
			glScalef(161,0.7,16);
			drawUnitSolidCube();
		}glPopMatrix();
		glPushMatrix();{
			glTranslatef(80.5-57,-56.5,-8+2);
			glScalef(161,0.7,16);
			drawUnitSolidCube();
		}glPopMatrix();
		
		glPushMatrix();{
			glTranslatef(103.5,0,-8+2);
			glScalef(0.7,112,16);
			drawUnitSolidCube();
		}glPopMatrix();
	}glPopMatrix();
}

void redMinarBase(){
	//glColor3f(0.5,0.5,0.5);

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, redBaseOctBmp); // here texid corresponds a bitmap image.
	gluQuadricNormals(IDquadric,GLU_SMOOTH);
	gluQuadricTexture(IDquadric, GLU_TRUE);
	glColor3f(1,1,1);
	glPushMatrix();{
		glTranslatef(57+104-2.2,57-1.9,-14);
		glRotatef(22.5,0,0,1);
		//glScalef(.15,.1,.15);
		gluCylinder(IDquadric,7,7,16,8,20);
	}glPopMatrix();

	glColor3f(1,1,1);
	glPushMatrix();{
		glTranslatef(-57-104+2.2,57-1.9,-14);
		glRotatef(22.5,0,0,1);
		//glScalef(.15,.1,.15);
		gluCylinder(IDquadric,7,7,16,8,20);
	}glPopMatrix();
	
	glColor3f(1,1,1);
	glPushMatrix();{
		glTranslatef(-57-104+2.2,-57+1.9,-14);
		glRotatef(22.5,0,0,1);
		//glScalef(.15,.1,.15);
		gluCylinder(IDquadric,7,7,16,8,20);
	}glPopMatrix();

	glColor3f(1,1,1);
	glPushMatrix();{
		glTranslatef(57+104-2.2,-57+1.9,-14);
		glRotatef(22.5,0,0,1);
		//glScalef(.15,.1,.15);
		gluCylinder(IDquadric,7,7,16,8,20);
	}glPopMatrix();
	glDisable(GL_TEXTURE_2D);
	
	/*
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, minarBaseFloorBmp); // here texid corresponds a bitmap image.
	gluQuadricNormals(IDquadric,GLU_SMOOTH);
	gluQuadricTexture(IDquadric, GLU_TRUE);
	*/
	glColor3f(0.35,0,0);
	glPushMatrix();{
		glTranslatef(57+104-2.2,57-1.9,2.1);
		glRotatef(22.5,0,0,1);
		//glScalef(.15,.1,.15);
		//glutSolidCircle(5,8);
		gluDisk(IDquadric,0,7,8,20);
	}glPopMatrix();
	glColor3f(0.35,0,0);
	glPushMatrix();{
		glTranslatef(-57-104+2.2,57-1.9,2.1);
		glRotatef(22.5,0,0,1);
		//glScalef(.15,.1,.15);
		//glutSolidCircle(5,8);
		gluDisk(IDquadric,0,7,8,20);
	}glPopMatrix();
	glColor3f(0.35,0,0);
	glPushMatrix();{
		glTranslatef(-57-104+2.2,-57+1.9,2.1);
		glRotatef(22.5,0,0,1);
		//glScalef(.15,.1,.15);
		//glutSolidCircle(5,8);
		gluDisk(IDquadric,0,7,8,20);
	}glPopMatrix();
	glColor3f(0.35,0,0);
	glPushMatrix();{
		glTranslatef(57+104-2.2,-57+1.9,2.1);
		glRotatef(22.5,0,0,1);
		//glScalef(.15,.1,.15);
		//glutSolidCircle(5,8);
		gluDisk(IDquadric,0,7 ,8,20);
	}glPopMatrix();
	//glDisable(GL_TEXTURE_2D);
	
}

void redFatMinar(){
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, redFatMinarBmp); // here texid corresponds a bitmap image.
	gluQuadricNormals(IDquadric,GLU_SMOOTH);
	gluQuadricTexture(IDquadric, GLU_TRUE);
	glColor3f(1,1,1);
	glPushMatrix();{
		glTranslatef(57+104-2.2,57-1.9,1);
		glRotatef(22.5,0,0,1);
		//glScalef(.15,.1,.15);
		gluCylinder(IDquadric,6,6,12,8,20);
	}glPopMatrix();

	glColor3f(1,1,1);
	glPushMatrix();{
		glTranslatef(-57-104+2.2,57-1.9,1);
		glRotatef(22.5,0,0,1);
		//glScalef(.15,.1,.15);
		gluCylinder(IDquadric,6,6,12,8,20);
	}glPopMatrix();
	
	glColor3f(1,1,1);
	glPushMatrix();{
		glTranslatef(-57-104+2.2,-57+1.9,1);
		glRotatef(22.5,0,0,1);
		//glScalef(.15,.1,.15);
		gluCylinder(IDquadric,6,6,12,8,20);
	}glPopMatrix();

	glColor3f(1,1,1);
	glPushMatrix();{
		glTranslatef(57+104-2.2,-57+1.9,1);
		glRotatef(22.5,0,0,1);
		//glScalef(.15,.1,.15);
		gluCylinder(IDquadric,6,6,12,8,20);
	}glPopMatrix();
	glDisable(GL_TEXTURE_2D);
	//top parts
	glPushMatrix();{
		glTranslatef(57+104-2.2,57-1.9,0);
		glRotatef(22.5,0,0,1);
		fatMinarTopParts();		
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-57-104+2.2,57-1.9,0);
		glRotatef(22.5,0,0,1);
		fatMinarTopParts();		
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-57-104+2.2,-57+1.9,0);
		glRotatef(22.5,0,0,1);
		fatMinarTopParts();		
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(57+104-2.2,-57+1.9,0);
		glRotatef(22.5,0,0,1);
		fatMinarTopParts();		
	}glPopMatrix();


}

void redMinarRoad(){
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, redMinarRoadBmp); // here texid corresponds a bitmap image.
	gluQuadricNormals(IDquadric,GLU_SMOOTH);
	gluQuadricTexture(IDquadric, GLU_TRUE);
	glColor3f(1,1,1);
		glPushMatrix();{
			glTranslatef(0,-36.5,0);
			glBegin(GL_QUADS);{
				glTexCoord2f(0,0);glVertex3f(160,-13,0);
				glTexCoord2f(0,1);glVertex3f(160,-13,10);
				glTexCoord2f(1,1);glVertex3f(160,13,10);
				glTexCoord2f(1,0);glVertex3f(160,13,0);	
			}glEnd();

		}glPopMatrix();
		glPushMatrix();{
			glTranslatef(0,36.5,0);
			glBegin(GL_QUADS);{
				glTexCoord2f(0,0);glVertex3f(160,-13,0);
				glTexCoord2f(0,1);glVertex3f(160,-13,10);
				glTexCoord2f(1,1);glVertex3f(160,13,10);
				glTexCoord2f(1,0);glVertex3f(160,13,0);
			}glEnd();

		}glPopMatrix();
	glDisable(GL_TEXTURE_2D);
	
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, redMinarRoadTopBmp); // here texid corresponds a bitmap image.
	gluQuadricNormals(IDquadric,GLU_SMOOTH);
	gluQuadricTexture(IDquadric, GLU_TRUE);
	glColor3f(1,1,1);
		glPushMatrix();{
			glTranslatef(0,36.5,0);
			glBegin(GL_QUADS);{
				glTexCoord2f(0,0);glVertex3f(160,-13,8);
				glTexCoord2f(0,1);glVertex3f(160-8,-13,8);
				glTexCoord2f(1,1);glVertex3f(160-8,13,8);
				glTexCoord2f(1,0);glVertex3f(160,13,8);
			}glEnd();
		}glPopMatrix();
		glPushMatrix();{
			glTranslatef(0,-36.5,0);
			glBegin(GL_QUADS);{
				glTexCoord2f(0,0);glVertex3f(160,-13,8);
				glTexCoord2f(0,1);glVertex3f(160-6,-13,8);
				glTexCoord2f(1,1);glVertex3f(160-6,13,8);
				glTexCoord2f(1,0);glVertex3f(160,13,8);
			}glEnd();
		}glPopMatrix();
	glDisable(GL_TEXTURE_2D);
	
	//
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, redMinarRoadThinBmp); // here texid corresponds a bitmap image.
	gluQuadricNormals(IDquadric,GLU_SMOOTH);
	gluQuadricTexture(IDquadric, GLU_TRUE);
	glColor3f(1,1,1);
		glPushMatrix();{
			glTranslatef(0,-36.5,0);
			glBegin(GL_QUADS);{
				glTexCoord2f(0,0);glVertex3f(160-0.5,-13,8);
				glTexCoord2f(0,1);glVertex3f(160-0.5,-13,10);
				glTexCoord2f(1,1);glVertex3f(160-0.5,13,10);
				glTexCoord2f(1,0);glVertex3f(160-0.5,13,8);	
			}glEnd();

		}glPopMatrix();
		glPushMatrix();{
			glTranslatef(0,36.5,0);
			glBegin(GL_QUADS);{
				glTexCoord2f(0,0);glVertex3f(160-0.5,-13,8);
				glTexCoord2f(0,1);glVertex3f(160-0.5,-13,10);
				glTexCoord2f(1,1);glVertex3f(160-0.5,13,10);
				glTexCoord2f(1,0);glVertex3f(160-0.5,13,8);
			}glEnd();

		}glPopMatrix();
		glPushMatrix();{
			glTranslatef(0,-36.5,0);
			glBegin(GL_QUADS);{
				glTexCoord2f(0,0);glVertex3f(160-6-0.5,-13,8);
				glTexCoord2f(0,1);glVertex3f(160-6-0.5,-13,10);
				glTexCoord2f(1,1);glVertex3f(160-6-0.5,13,10);
				glTexCoord2f(1,0);glVertex3f(160-6-0.5,13,8);	
			}glEnd();

		}glPopMatrix();
		glPushMatrix();{
			glTranslatef(0,36.5,0);
			glBegin(GL_QUADS);{
				glTexCoord2f(0,0);glVertex3f(160-6-0.5,-13,8);
				glTexCoord2f(0,1);glVertex3f(160-6-0.5,-13,10);
				glTexCoord2f(1,1);glVertex3f(160-6-0.5,13,10);
				glTexCoord2f(1,0);glVertex3f(160-6-0.5,13,8);
			}glEnd();

		}glPopMatrix();
		glPushMatrix();{
			glTranslatef(0,-36.5,0);
			glBegin(GL_QUADS);{
				glTexCoord2f(0,0);glVertex3f(160-6,-13,8);
				glTexCoord2f(0,1);glVertex3f(160-6,-13,10);
				glTexCoord2f(1,1);glVertex3f(160-6,13,10);
				glTexCoord2f(1,0);glVertex3f(160-6,13,8);	
			}glEnd();

		}glPopMatrix();
		glPushMatrix();{
			glTranslatef(0,36.5,0);
			glBegin(GL_QUADS);{
				glTexCoord2f(0,0);glVertex3f(160-6,-13,8);
				glTexCoord2f(0,1);glVertex3f(160-6,-13,10);
				glTexCoord2f(1,1);glVertex3f(160-6,13,10);
				glTexCoord2f(1,0);glVertex3f(160-6,13,8);
			}glEnd();

		}glPopMatrix();
	glDisable(GL_TEXTURE_2D);

	//doors
	glColor3f(0.37,0.1,0.1);
	glPushMatrix();{
		glTranslatef(160-8.25,23.5,6); //6
		glPushMatrix();{
			glTranslatef(0,2,0);
			glScalef(0.5,4,8);
			drawUnitSolidCube();
		}glPopMatrix();
		glPushMatrix();{
			glTranslatef(0,7.25,0);
			glScalef(0.5,0.5,8);
			drawUnitSolidCube();
		}glPopMatrix();
		glPushMatrix();{
			glTranslatef(0,10.75,0);
			glScalef(0.5,0.5,8);
			drawUnitSolidCube();
		}glPopMatrix();
		glPushMatrix();{
			glTranslatef(0,14.25,0);
			glScalef(0.5,0.5,8);
			drawUnitSolidCube();
		}glPopMatrix();
		glPushMatrix();{
			glTranslatef(0,17.75,0);
			glScalef(0.5,0.5,8);
			drawUnitSolidCube();
		}glPopMatrix();
		glPushMatrix();{
			glTranslatef(0,23,0);
			glScalef(0.5,4,8);
			drawUnitSolidCube();
		}glPopMatrix();
		for(float f=0;f<16;f=f+3.5){
			glPushMatrix();{
			glBegin(GL_QUADS);{
				glVertex3f(0,f+4,8-4);
				glVertex3f(0,f+5.5,8-4);
				glVertex3f(0,f+5.5,7-4);
				glVertex3f(0,f+4,5-4);
			}glEnd();
			glBegin(GL_QUADS);{
				glVertex3f(0,f+7,8-4);
				glVertex3f(0,f+5.5,8-4);
				glVertex3f(0,f+5.5,7-4);
				glVertex3f(0,f+7,5-4);
			}glEnd();
			}glPopMatrix();
		}
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(160-8,-23.5-25,6);
		glPushMatrix();{
			glTranslatef(0,2,0);
			glScalef(0.5,4,8);
			drawUnitSolidCube();
		}glPopMatrix();
		glPushMatrix();{
			glTranslatef(0,7.25,0);
			glScalef(0.5,0.5,8);
			drawUnitSolidCube();
		}glPopMatrix();
		glPushMatrix();{
			glTranslatef(0,10.75,0);
			glScalef(0.5,0.5,8);
			drawUnitSolidCube();
		}glPopMatrix();
		glPushMatrix();{
			glTranslatef(0,14.25,0);
			glScalef(0.5,0.5,8);
			drawUnitSolidCube();
		}glPopMatrix();
		glPushMatrix();{
			glTranslatef(0,17.75,0);
			glScalef(0.5,0.5,8);
			drawUnitSolidCube();
		}glPopMatrix();
		glPushMatrix();{
			glTranslatef(0,23,0);
			glScalef(0.5,4,8);
			drawUnitSolidCube();
		}glPopMatrix();
		
		for(float f=0;f<16;f=f+3.5){
			glPushMatrix();{
			glBegin(GL_QUADS);{
				glVertex3f(0,f+4,8-4);
				glVertex3f(0,f+5.5,8-4);
				glVertex3f(0,f+5.5,7-4);
				glVertex3f(0,f+4,5-4);
			}glEnd();
			glBegin(GL_QUADS);{
				glVertex3f(0,f+7,8-4);
				glVertex3f(0,f+5.5,8-4);
				glVertex3f(0,f+5.5,7-4);
				glVertex3f(0,f+7,5-4);
			}glEnd();
			}glPopMatrix();
		}


	}glPopMatrix();
	

}

void fatMinarTopParts(){
	glPushMatrix();{
		glTranslatef(0,0,9);
		glRotatef(22.5,0,0,1);
		glScalef(0.35,0.35,0.35);
		fatMinarRing();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,12);
		//glRotatef(22.5,0,0,1);
		glScalef(0.22,0.22,0.12);
		fatMinarTop();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,16);
		glRotatef(22.5,0,0,1);
		glScalef(0.35,0.35,0.35);
		fatMinarRing();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,20);
		glRotatef(22.5,0,0,1);
		glScalef(0.2,0.2,0.2);
		fatMinarRing();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,0,23);
		//glRotatef(22.5,0,0,1);
		glScalef(0.12,0.12,0.12);
		//glColor3f(0.5,0.1,0.1);
		fatMinarTopWithGombuj();
	}glPopMatrix();
}

void fatMinarTopWithGombuj(){
	//botto,
	glColor3f(0.3,0.1,0.1);
	glPushMatrix();{
		glRotatef(22.5,0,0,1);
		gluCylinder(IDquadric,25,25,2,8,20);
	}glPopMatrix();
	glColor3f(0.3,0.1,0.1);
	glPushMatrix();{
		glTranslatef(0,0,2);
		glRotatef(22.5,0,0,1);
		glutSolidCircle(25,8);
	}glPopMatrix();
	
	//under gombuj
	glColor3f(0.37,0.2,0.2);
	glPushMatrix();{
		glTranslatef(0,0,42);
		glRotatef(22.5,0,0,1);
		gluCylinder(IDquadric,30,30,2,8,20);
	}glPopMatrix();
	glColor3f(0.37,0.2,0.2);
	glPushMatrix();{
		glTranslatef(0,0,44);
		glRotatef(22.5,0,0,1);
		glutSolidCircle(30,8);
	}glPopMatrix();
	glColor3f(0.37,0.2,0.2);
	glPushMatrix();{
		glTranslatef(0,0,42);
		glRotatef(22.5,0,0,1);
		glutSolidCircle(30,8);
	}glPopMatrix();
	

	glColor3f(0.3,0.1,0.1);
	glPushMatrix();{
		glTranslatef(0,0,44);
		glRotatef(22.5,0,0,1);
		gluCylinder(IDquadric,22,22,4,8,20);
	}glPopMatrix();
	glColor3f(0.37,0.15,0.15);
	glPushMatrix();{
		glTranslatef(0,0,48);
		glRotatef(22.5,0,0,1);
		glutSolidCircle(22,8);
	}glPopMatrix();

	glColor3f(1,1,1);
	//gombuj
	glPushMatrix();{
		glTranslatef(0,0,52);
		glScalef(18,18,18);
		gombuj();
	}glPopMatrix();
	
	//bars
	glColor3f(0.3,0.1,0.1);
	glPushMatrix();{
		glTranslatef(0,22,2);
		minarTopBars();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,-22,2);
		minarTopBars();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(22,0,2);
		glRotatef(90,0,0,1);
		minarTopBars();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-22,0,2);
		glRotatef(90,0,0,1);
		minarTopBars();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(15.6,-15.6,2);
		glRotatef(45,0,0,1);
		minarTopBars();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-15.6,15.6,2);
		glRotatef(45,0,0,1);
		minarTopBars();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-15.6,-15.6,2);
		glRotatef(-45,0,0,1);
		minarTopBars();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(15.6,15.6,2);
		glRotatef(-45,0,0,1);
		minarTopBars();
	}glPopMatrix();
}

void fatMinarTop(){
	//botto,
	glColor3f(0.5,0.1,0.1);
	glPushMatrix();{
		glRotatef(22.5,0,0,1);
		gluCylinder(IDquadric,25,25,2,8,20);
	}glPopMatrix();
	glColor3f(0.5,0.1,0.1);
	glPushMatrix();{
		glTranslatef(0,0,2);
		glRotatef(22.5,0,0,1);
		glutSolidCircle(25,8);
	}glPopMatrix();
	
	//under gombuj
	glColor3f(0.3,0.1,0.1);
	glPushMatrix();{
		glTranslatef(0,0,42);
		glRotatef(22.5,0,0,1);
		gluCylinder(IDquadric,25,25,2,8,20);
	}glPopMatrix();
	glColor3f(0.3,0.1,0.1);
	glPushMatrix();{
		glTranslatef(0,0,44);
		glRotatef(22.5,0,0,1);
		glutSolidCircle(25,8);
	}glPopMatrix();
	glColor3f(0.3,0.1,0.1);
	glPushMatrix();{
		glTranslatef(0,0,42);
		glRotatef(22.5,0,0,1);
		glutSolidCircle(25,8);
	}glPopMatrix();
	

	glColor3f(0.3,0.2,0.2);
	glPushMatrix();{
		glTranslatef(0,0,44);
		glRotatef(22.5,0,0,1);
		gluCylinder(IDquadric,22,22,4,8,20);
	}glPopMatrix();
	glColor3f(0.3,0.2,0.2);
	glPushMatrix();{
		glTranslatef(0,0,48);
		glRotatef(22.5,0,0,1);
		glutSolidCircle(22,8);
	}glPopMatrix();

	/*
	//gombuj
	glPushMatrix();{
		glTranslatef(0,0,52);
		glScalef(18,18,18);
		gombuj();
	}glPopMatrix();
	*/
	//bars
	glColor3f(0.35,.1,.11);
	glPushMatrix();{
		glTranslatef(0,22,2);
		minarTopBars();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,-22,2);
		minarTopBars();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(22,0,2);
		glRotatef(90,0,0,1);
		minarTopBars();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-22,0,2);
		glRotatef(90,0,0,1);
		minarTopBars();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(15.6,-15.6,2);
		glRotatef(45,0,0,1);
		minarTopBars();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-15.6,15.6,2);
		glRotatef(45,0,0,1);
		minarTopBars();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-15.6,-15.6,2);
		glRotatef(-45,0,0,1);
		minarTopBars();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(15.6,15.6,2);
		glRotatef(-45,0,0,1);
		minarTopBars();
	}glPopMatrix();
	
}

void fatMinarRing(){
	
	glColor3f(0.5,0.2,0.2);
	glPushMatrix();{
		glTranslatef(0,0,8);
		gluCylinder(IDquadric,22,22,1,8,20);
	}glPopMatrix();
	glColor3f(0.3,0.1,0.1);
	glPushMatrix();{
		glTranslatef(0,0,8);
		gluDisk(IDquadric,0,22,8,20);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,9);
		gluDisk(IDquadric,0,22,8,20);
	}glPopMatrix();

	glColor3f(0.37,0.0,0.0);
	glPushMatrix();{
		glTranslatef(0,0,9);
		gluCylinder(IDquadric,20,20,5,8,20);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,9);
		gluCylinder(IDquadric,19,19,5,8,20);
	}glPopMatrix();
	glColor3f(0.5,0.1,0.1);
	glPushMatrix();{
		glTranslatef(0,0,14);
		gluDisk(IDquadric,19,20,8,20);
	}glPopMatrix();
	
	GLUquadric *qu=gluNewQuadric();
	float x=0;
	float y=0;
	float r=18.5;
	for(int i=0;i<360;i=i+15){
		x=r*cos((M_PI*i)/180);
		y=r*sin((M_PI*i)/180);
		glPushMatrix();{
			glColor3f(0.2,0.0,0.0);
			glTranslatef(x,y,9+3);
			glRotatef(i,0,0,1);
			glScalef(3,0.8,5);
			drawUnitSolidCube();
			//glColor3f(0.8,0.5,0.5);
			//glTranslatef(0,0,0.6);
			//glScalef(1,1,.4);
			//gluSphere(qu,0.4,4,4);
		}glPopMatrix();


		x=(r+1.5)*cos((M_PI*i)/180);
		y=(r+1.5)*sin((M_PI*i)/180);
		glPushMatrix();{
			glColor3f(0.2,0.0,0.0);
			glTranslatef(x,y,3.5);
			glScalef(0.2,0.2,0.2);
			glRotatef(90,1,0,0);
			glRotatef(i,0,1,0);
			smallTorus();
		}glPopMatrix();
		x=(r+2)*cos((M_PI*i)/180);
		y=(r+2)*sin((M_PI*i)/180);
		glPushMatrix();{
			glColor3f(0.7,0.2,0.2);
			glTranslatef(x,y,6.5);
			gluSphere(qu,0.8,4,4);
		}glPopMatrix();
	}

}

void setRedBaseSideDesign(){
	//glColor3f(0.5,0.5,0.4);

	double equ[4];
	equ[0]=-1;
	equ[1]=0;
	equ[2]=0;
	equ[3]=158;

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, redBaseSideBmp); // here texid corresponds a bitmap image.
	gluQuadricNormals(IDquadric,GLU_SMOOTH);
	gluQuadricTexture(IDquadric, GLU_TRUE);
	glColor3f(1,1,1);
		glPushMatrix();{
			glClipPlane(GL_CLIP_PLANE0,equ);
			glEnable(GL_CLIP_PLANE0);{
				glTranslatef(98+38,0,2);
				glRotatef(180,0,0,1);
				for(int i=0;i<310;i=i+60){
					glBegin(GL_QUADS);{
						glTexCoord2f(0,0);glVertex3f(i+0,57,-16);
						glTexCoord2f(0,1);glVertex3f(i+0,57,0);
						glTexCoord2f(1,1);glVertex3f(i+-60,57,0);
						glTexCoord2f(1,0);glVertex3f(i+-60,57,-16);
					}glEnd();
				}
		}glPopMatrix();
		glPushMatrix();{
				glTranslatef(-98,0,2);
				for(int i=0;i<310;i=i+60){
					glBegin(GL_QUADS);{
						glTexCoord2f(0,0);glVertex3f(i+0,57,-16);
						glTexCoord2f(0,1);glVertex3f(i+0,57,0);
						glTexCoord2f(1,1);glVertex3f(i+-60,57,0);
						glTexCoord2f(1,0);glVertex3f(i+-60,57,-16);
					}glEnd();
				}
			}glDisable(GL_CLIP_PLANE0);
		}glPopMatrix();
		glPushMatrix();{
			glTranslatef(218,-2,2);
			glScalef(1,0.92,1);
			glRotatef(90,0,0,1);
			for(int i=0;i<110;i=i+60){
				glBegin(GL_QUADS);{
					glTexCoord2f(0,0);glVertex3f(i+0,57,-16);
					glTexCoord2f(0,1);glVertex3f(i+0,57,0);
					glTexCoord2f(1,1);glVertex3f(i+-60,57,0);
					glTexCoord2f(1,0);glVertex3f(i+-60,57,-16);
				}glEnd();
			}
		}glPopMatrix();
		glPushMatrix();{
			glTranslatef(-218,-2,2);
			glScalef(1,0.92,1);
			glRotatef(-90,0,0,1);
			for(int i=0;i<110;i=i+60){
				glBegin(GL_QUADS);{
					glTexCoord2f(0,0);glVertex3f(i+0,57,-16);
					glTexCoord2f(0,1);glVertex3f(i+0,57,0);
					glTexCoord2f(1,1);glVertex3f(i+-60,57,0);
					glTexCoord2f(1,0);glVertex3f(i+-60,57,-16);
				}glEnd();
			}
		}glPopMatrix();
	glDisable(GL_TEXTURE_2D);
}

void setRedParts(){
	glPushMatrix();{
		glTranslatef(104+35,21.25,1);
		glScalef(0.9,0.9,0.9);
		drawRedPart();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-104-35,-21.25,1);
		glRotatef(180,0,0,1);
		glScalef(0.9,0.9,0.9);
		drawRedPart();
	}glPopMatrix();
	glPushMatrix();{
		redMinarBase();	
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(32.2,0,0);
		glScalef(0.8,1,1);
		redMinarRoad();
		drawRedPartDoor();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-32.2,0,0);
		glScalef(0.8,1,1);
		glRotatef(180,0,0,1);
		redMinarRoad();
		drawRedPartDoor();
	}glPopMatrix();

}

void floorSideDesign(){
	glColor3f(1,1,1);
	glPushMatrix();{
		glPushMatrix();{
			glRotatef(-90,0,1,0);
			glEnable(GL_TEXTURE_2D);
			glBindTexture(GL_TEXTURE_2D,floorSideBmp);
			glNormal3f(1.0,0.0,0.0);
			glBegin(GL_QUADS);{
				glTexCoord2f(0,0);glVertex3f(0,0,0);
				glTexCoord2f(0,1);glVertex3f(9.5,0,0);
				glTexCoord2f(1,1);glVertex3f(9.5,0,32);
				glTexCoord2f(1,0);glVertex3f(0,0,32);
			}glEnd();
			glDisable(GL_TEXTURE_2D);
		}glPopMatrix();
	}glPopMatrix();
}

void whiteMinarFloor(){
	glColor3f(1,1,1);
	glPushMatrix();{
		glTranslatef(0,0,0.6/2+6.4/2);
		/*glPushMatrix();{
			glScalef(94,90,6.4);
			drawUnitSolidCube();
		}glPopMatrix();
		glPushMatrix();{
			glTranslatef(0,92/2+3/2,0);
			glScalef(18,3,6.4);
			drawUnitSolidCube();
		}glPopMatrix();
		*/

		//glColor3f(0.4,0.3,0.5);
		glPushMatrix();{
			glEnable(GL_TEXTURE_2D);
			glBindTexture(GL_TEXTURE_2D,floorBmp);
			glNormal3f(1.0,0.0,0.0);
			glBegin(GL_QUADS);{
				glTexCoord2f(0,0);glVertex3f(-48,-46,3.25);
				glTexCoord2f(0,2);glVertex3f(48,-46,3.25);
				glTexCoord2f(2,2);glVertex3f(48,46,3.25);
				glTexCoord2f(2,0);glVertex3f(-48,46,3.25);
			}glEnd();
			//glColor3f(0,0,0);
			glTranslatef(0,49,0);
			glBegin(GL_QUADS);{
				glTexCoord2f(0,0);glVertex3f(-9,-4,3.25);
				glTexCoord2f(0,1);glVertex3f(9,-4,3.25);
				glTexCoord2f(1,1);glVertex3f(9,0,3.25);
				glTexCoord2f(1,0);glVertex3f(-9,0,3.25);
			}glEnd();


			glDisable(GL_TEXTURE_2D);
		}glPopMatrix();
		glColor3f(1,1,1);
	}glPopMatrix();
}

void whiteFloor(){
	glColor3f(0.8,0.8,0.8);
	glPushMatrix();{
		//glScalef(114,112,0.6);
		glTranslatef(0,0,-2.2);
		glScalef(114,112,5);
		drawUnitSolidCube();

		//glEnable(GL_TEXTURE_2D);
		//glBindTexture(GL_TEXTURE_2D, texid1); // here texid corresponds a bitmap image.
		//gluQuadricNormals(IDquadric,GLU_SMOOTH);
		//gluQuadricTexture(IDquadric, GLU_TRUE);
		//gluCylinder(IDquadric,25 ,25 ,50,20,20);
		//glDisable(GL_TEXTURE_2D);
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,112/2+3/2,0);
		glScalef(34,3,0.4);
		drawUnitSolidCube();
	}glPopMatrix();

	glColor3f(1,1,1);
	glPushMatrix();{
			glEnable(GL_TEXTURE_2D);
			glBindTexture(GL_TEXTURE_2D,whiteFloorBmp);
			glNormal3f(1.0,0.0,0.0);
			glBegin(GL_QUADS);{
				glTexCoord2f(0,0);glVertex3f(-57,-56,0.6);
				glTexCoord2f(0,2);glVertex3f(57,-56,0.6);
				glTexCoord2f(2,2);glVertex3f(57,56,0.6);
				glTexCoord2f(2,0);glVertex3f(-57,56,0.6);
			}glEnd();
			glDisable(GL_TEXTURE_2D);
		}glPopMatrix();

}

void door(){
	//glColor3f(1,1,1);
	glPushMatrix();{
		//glScalef(.333,.333,.333);
		glPushMatrix();{
			glTranslatef(.5,0,33);
			glScalef(1,20,66);
			drawUnitSolidCube();
		}glPopMatrix();
		
		glPushMatrix();{
			glTranslatef(1+39+.5,0,33);
			glScalef(1,20,66);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glTranslatef(20.5,0,.5);
			glScalef(39,20,1);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glTranslatef(20.5,0,66-.5);
			glScalef(39,20,1);
			drawUnitSolidCube();
		}glPopMatrix();

		glColor3f(0.8,0.8,0.8);
	
		//inside door design
		glPushMatrix();{
			glTranslatef(19.5+1,8,22);
			glPushMatrix();{
				glTranslatef(0,0,14.5);
				glScalef(38,1,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(-18.5,0,0);
				glScalef(1,1,30);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(18.5,0,0);
				glScalef(1,1,30);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(-9.5,0,0);
				glScalef(1,1,30);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(9.5,0,0);
				glScalef(1,1,30);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(-14,0,0);
				glScalef(1,1,30);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(14,0,0);
				glScalef(1,1,30);
				drawUnitSolidCube();
			}glPopMatrix();

			glPushMatrix();{
				glTranslatef(14,0,5);
				glScalef(9,1,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(14,0,-5);
				glScalef(9,1,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(14,0,-14.5);
				glScalef(9,1,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(-14,0,5);
				glScalef(9,1,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(-14,0,-5);
				glScalef(9,1,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(-14,0,-14.5);
				glScalef(9,1,1);
				drawUnitSolidCube();
			}glPopMatrix();
		}glPopMatrix();

		//door side
		glPushMatrix();{
			glTranslatef(39.5,-8,22);
			glPushMatrix();{
				glTranslatef(0,0,-3.5);
				glScalef(1,1,37);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,5.5,0);
				glScalef(1,1,30);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,11,0);
				glScalef(1,1,30);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,16,-3.5);
				glScalef(1,1,37);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,8,14.5);
				glScalef(1,16,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,8,5);
				glScalef(1,16,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,8,-5);
				glScalef(1,16,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,8,-14.5);
				glScalef(1,16,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,8,-20.5);
				glScalef(1,16,1);
				drawUnitSolidCube();
			}glPopMatrix();
		}glPopMatrix();

		
		glPushMatrix();{
			glTranslatef(1.5,-8,22);
			glPushMatrix();{
				glTranslatef(0,0,-3.5);
				glScalef(1,1,37);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,5.5,0);
				glScalef(1,1,30);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,11,0);
				glScalef(1,1,30);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,16,-3.5);
				glScalef(1,1,37);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,8,14.5);
				glScalef(1,16,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,8,5);
				glScalef(1,16,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,8,-5);
				glScalef(1,16,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,8,-14.5);
				glScalef(1,16,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,8,-20.5);
				glScalef(1,16,1);
				drawUnitSolidCube();
			}glPopMatrix();
		}glPopMatrix();
		//door back
		glColor3f(0.2,0.3,0.3);
		glPushMatrix();{
			glTranslatef(20.5,10,33);
			glScalef(39,1,66);
			drawUnitSolidCube();
		}glPopMatrix();

		
		glColor3f(0.9,0.9,0.9);
		glPushMatrix();{
			glTranslatef(0,0,50);
			glRotatef(90,1,0,0);
			
			double equ[4];
			equ[0]=0;
			equ[1]=-1;
			equ[2]=0;
			equ[3]=15;

			glClipPlane(GL_CLIP_PLANE0,equ);

			glEnable(GL_CLIP_PLANE0);{
				double tran=0;
				for(double i=1;i<=22;i=i+.3){
					tran=8*log(i);
					glPushMatrix();{
						glTranslatef(i,tran,0);
						glScalef(1,25,16);
						drawUnitSolidCube();
					}glPopMatrix();
					glPushMatrix();{
						glTranslatef(-i+41,tran,0);
						glScalef(1,25,16);
						drawUnitSolidCube();
					}glPopMatrix();
				}
				tran=0;
				glPushMatrix();{
				glColor3f(0.4,0.5,0.4);
				glTranslatef(20.5,15,10);
				for(i=3;i<28;i=i+.5){
					tran=5.5*log(i);
					glPushMatrix();{
						glTranslatef(0,-i,-tran);
						glScalef(38,1,1);
						drawUnitSolidCube();
					}glPopMatrix();
				}	
				}glPopMatrix();
			};glDisable(GL_CLIP_PLANE0);
		}glPopMatrix();
	}glPopMatrix();
}

void miniDoor(){
	//glColor3f(0.9,0.2,0.2);
	glColor3f(0.2,0.2,0.2);
	glPushMatrix();{
		glPushMatrix();{
			glTranslatef(-.5,0,33);
			glScalef(3,20,66);
			drawUnitSolidCube();
		}glPopMatrix();
		glColor3f(0.2,0.2,0.2);
		glPushMatrix();{
			glTranslatef(1+39+1.5,0,33);
			glScalef(3,20,66);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glTranslatef(20.5,0,1.5);
			glScalef(39,20,3);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glTranslatef(20.5,0,66-1.5);
			glScalef(39,20,3);
			drawUnitSolidCube();
		}glPopMatrix();

		glColor3f(0.8,0.8,0.8);
	
		//inside door design
		glPushMatrix();{
			glTranslatef(19.5+1,8,22);
			glPushMatrix();{
				glTranslatef(0,0,14.5);
				glScalef(38,1,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,0,-14.5-6);
				glScalef(38,1,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(-18.5,0,-3.5);
				glScalef(1,1,37);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(18.5,0,-3.5);
				glScalef(1,1,37);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(-6,0,-3);
				glScalef(1,1,36);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(6,0,-3);
				glScalef(1,1,36);
				drawUnitSolidCube();
			}glPopMatrix();

			glPushMatrix();{
				glTranslatef(0,0,5.5);
				glScalef(12,1,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(14,0,-10.5);
				glScalef(15,1,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(-14,0,-10.5);
				glScalef(15,1,1);
				drawUnitSolidCube();
			}glPopMatrix();
		}glPopMatrix();

		//door side
		glPushMatrix();{
			glTranslatef(39.5,-8,22);
			glPushMatrix();{
				glTranslatef(0,0,-3.5);
				glScalef(1,1,37);
				drawUnitSolidCube();
			}glPopMatrix();
			
			glPushMatrix();{
				glTranslatef(0,8,14.5);
				glScalef(1,16,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,8,-10.5);
				glScalef(1,16,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,8,-20.5);
				glScalef(1,16,1);
				drawUnitSolidCube();
			}glPopMatrix();
		}glPopMatrix();

	
		glPushMatrix();{
			glTranslatef(1.5,-8,22);
			//glTranslatef(39.5,-8,22);
			glPushMatrix();{
				glTranslatef(0,0,-3.5);
				glScalef(1,1,37);
				drawUnitSolidCube();
			}glPopMatrix();
			
			glPushMatrix();{
				glTranslatef(0,8,14.5);
				glScalef(1,16,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,8,-10.5);
				glScalef(1,16,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,8,-20.5);
				glScalef(1,16,1);
				drawUnitSolidCube();
			}glPopMatrix();
		}glPopMatrix();
	
	
		//door back
		glColor3f(0.2,0.3,0.3);
		glPushMatrix();{
			glTranslatef(20.5,10,33);
			glScalef(39,1,66);
			drawUnitSolidCube();
		}glPopMatrix();

		glColor3f(0.9,0.9,0.9);
		glPushMatrix();{
			glTranslatef(0,0,50);
			glRotatef(90,1,0,0);
			
			double equ[4];
			equ[0]=0;
			equ[1]=-1;
			equ[2]=0;
			equ[3]=15;

			glClipPlane(GL_CLIP_PLANE0,equ);

			glEnable(GL_CLIP_PLANE0);{
				double tran=0;
				for(double i=1;i<=22;i=i+.3){
					tran=8*log(i);
					glPushMatrix();{
						glTranslatef(i,tran,0);
						glScalef(1,25,16);
						drawUnitSolidCube();
					}glPopMatrix();
					glPushMatrix();{
						glTranslatef(-i+41,tran,0);
						glScalef(1,25,16);
						drawUnitSolidCube();
					}glPopMatrix();
				}
				tran=0;
				glPushMatrix();{
				glColor3f(0.4,0.5,0.4);
				glTranslatef(20.5,15,10);
				for(i=3;i<28;i=i+.5){
					tran=5.5*log(i);
					glPushMatrix();{
						glTranslatef(0,-i,-tran);
						glScalef(38,1,1);
						drawUnitSolidCube();
					}glPopMatrix();
				}	
				}glPopMatrix();
			};glDisable(GL_CLIP_PLANE0);
		}glPopMatrix();
		
	}glPopMatrix();
}

void drawUnitSolidCube(){
	glPushMatrix();{

		glTranslatef(-.5,-.5,-.5);
		glBegin(GL_QUADS);{
			glVertex3f(0,0,0);
			glVertex3f(1,0,0);
			glVertex3f(1,1,0);
			glVertex3f(0,1,0);
		}glEnd();
		glBegin(GL_QUADS);{
			glVertex3f(0,0,0);
			glVertex3f(1,0,0);
			glVertex3f(1,0,1);
			glVertex3f(0,0,1);
		}glEnd();
		
		glBegin(GL_QUADS);{
			glVertex3f(0,0,0);
			glVertex3f(0,1,0);
			glVertex3f(0,1,1);
			glVertex3f(0,0,1);
		}glEnd();
		//glColor3f(0,1,0);
		glBegin(GL_QUADS);{
			glVertex3f(0,1,0);
			glVertex3f(1,1,0);
			glVertex3f(1,1,1);
			glVertex3f(0,1,1);
		}glEnd();
		//glColor3f(0,0,1);
		glBegin(GL_QUADS);{
			glVertex3f(0,0,1);
			glVertex3f(1,0,1);
			glVertex3f(1,1,1);
			glVertex3f(0,1,1);
		}glEnd();
		//glColor3f(1,0,0);
		glBegin(GL_QUADS);{
			glVertex3f(1,0,0);
			glVertex3f(1,1,0);
			glVertex3f(1,1,1);
			glVertex3f(1,0,1);
		}glEnd();

	};glPopMatrix();
}


void mainDoorSideBar(){
	glColor3f(.87,.8,.92);
	glPushMatrix();{
		glTranslatef(0,8.5,7.5+12.5);
		glScalef(4,3.5,25);
		drawUnitSolidCube();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,-8.5,7.5+12.5);
		glScalef(4,3.5,25);
		drawUnitSolidCube();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,7.5+23.8);
		glScalef(4,20,3.5);
		drawUnitSolidCube();
	}glPopMatrix();
	
	glColor3f(0.2,0.2,0.2);
	glPushMatrix();{
		glTranslatef(0,0,7.5+23.8+2);
		glScalef(4,20,1);
		drawUnitSolidCube();
	}glPopMatrix();
	glColor3f(0.5,0.4,0.2);
	glPushMatrix();{
		glTranslatef(0,0,7.5+23.8+2.6);
		glScalef(5,22,0.3);
		drawUnitSolidCube();
	}glPopMatrix();
	glColor3f(0.9,0.9,0.8);
	glPushMatrix();{
		glTranslatef(1.9,0,7.5+23.8+4);
		glScalef(.3,19.5,3);
		drawUnitSolidCube();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-1.9,0,7.5+23.8+4);
		glScalef(.3,19.5,3);
		drawUnitSolidCube();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,-9.6,7.5+23.8+4);
		glScalef(4,.3,3);
		drawUnitSolidCube();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,9.6,7.5+23.8+4);
		glScalef(4,.3,3);
		drawUnitSolidCube();
	}glPopMatrix();

}

void smallDoorSideBar(){
	glColor3f(.7,.7,.7);
	glPushMatrix();{
		glTranslatef(0,4.3,7.5+10.9);
		glScalef(3,2.0,21.8);
		drawUnitSolidCube();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,-4.3,7.5+10.9);
		glScalef(3,2.0,21.8);
		drawUnitSolidCube();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,7.5+21.1);
		glScalef(3,8,1.5);
		drawUnitSolidCube();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,7.5+10.3);
		glScalef(1.5,8,1.5);
		drawUnitSolidCube();
	}glPopMatrix();
}

void drawDoors(){
	double dist=11.5;
	double dist2=6;
	//main 4 doors
	//main 1
	glColor3f(0.5,0.5,0.5);
	glPushMatrix();{
		glTranslatef(-6.9,-25.5,7.5);		
		glScalef(.333,.2,.333);
		door();
	}glPopMatrix();
	//side
	glPushMatrix();{
		glTranslatef(26,0,0);
		mainDoorSideBar();

		//new
		//glTranslatef(26,9.25,8);
		//glScalef(1,0.85,1.2);
		//drawRedBigFrontGate();
	}glPopMatrix();
	//small doors
	glColor3f(0.5,0.5,0.5);
	glPushMatrix();{
		glTranslatef(-dist2-dist,-27,7.5);
		glScalef(.15,.1,.15);
		miniDoor();
	}glPopMatrix();
	glColor3f(0.5,0.5,0.5);
	glPushMatrix();{
		glTranslatef(-dist2-dist,-27,18);
		glScalef(.15,.1,.15);
		miniDoor();
	}glPopMatrix();
	//small door side bar
	glPushMatrix();{
		glTranslatef(14.4,-26.5,0);
		glRotatef(-90,0,0,1);
		//glScalef(.15,.1,.15);
		smallDoorSideBar();
	}glPopMatrix();
	
	glColor3f(0.5,0.5,0.5);
	glPushMatrix();{
		glTranslatef(dist,-27,7.5);
		glScalef(.15,.1,.15);
		miniDoor();
	}glPopMatrix();
	glColor3f(0.5,0.5,0.5);
	glPushMatrix();{
		glTranslatef(dist,-27,18);
		glScalef(.15,.1,.15);
		miniDoor();
	}glPopMatrix();
	//small door side bar
	glPushMatrix();{
		glTranslatef(-14.5,-26.5,0);
		glRotatef(-90,0,0,1);
		//glScalef(.15,.1,.15);
		smallDoorSideBar();
	}glPopMatrix();
	
	//main 2
	glColor3f(0.5,0.5,0.5);
	glPushMatrix();{
		glTranslatef(6.9,25.5,7.5);
		glRotatef(180,0,0,1);
		glScalef(.333,.2,.333);
		door();
	}glPopMatrix();
	//side bar
	glPushMatrix();{
		glTranslatef(0,26,0);
		glRotatef(90,0,0,1);
		mainDoorSideBar();
	}glPopMatrix();
	//small doors
	glColor3f(0.5,0.5,0.5);
	glPushMatrix();{
		glTranslatef(dist2+dist,27,7.5);
		glRotatef(180,0,0,1);
		glScalef(.15,.1,.15);
		miniDoor();
	}glPopMatrix();
	glColor3f(0.5,0.5,0.5);
	glPushMatrix();{
		glTranslatef(dist2+dist,27,18);
		glRotatef(180,0,0,1);
		glScalef(.15,.1,.15);
		miniDoor();
	}glPopMatrix();
	//small door side bar
	glPushMatrix();{
		glTranslatef(-14.5,26.5,0);
		glRotatef(90,0,0,1);
		//glScalef(.15,.1,.15);
		smallDoorSideBar();
	}glPopMatrix();
	
	glColor3f(0.5,0.5,0.5);
	glPushMatrix();{
		glTranslatef(-dist,27,7.5);
		glRotatef(180,0,0,1);
		glScalef(.15,.1,.15);
		miniDoor();
	}glPopMatrix();
	glColor3f(0.5,0.5,0.5);
	glPushMatrix();{
		glTranslatef(-dist,27,18);
		glRotatef(180,0,0,1);
		glScalef(.15,.1,.15);
		miniDoor();
	}glPopMatrix();
	//small door side bar
	glPushMatrix();{
		glTranslatef(14.4,26.5,0);
		glRotatef(90,0,0,1);
		//glScalef(.15,.1,.15);
		smallDoorSideBar();
	}glPopMatrix();
	
	//main 3
	glColor3f(0.5,0.5,0.5);
	glPushMatrix();{
		glTranslatef(25.5,-6.9,7.5);
		glRotatef(90,0,0,1);
		glScalef(.333,.2,.333);
		door();
	}glPopMatrix();
	//side bar
	glPushMatrix();{
		glTranslatef(-26,0,0);
		mainDoorSideBar();
	}glPopMatrix();
	//small doors
	glColor3f(0.5,0.5,0.5);
	glPushMatrix();{
		glTranslatef(26.5,-dist2-dist,7.5);
		//glTranslatef(27,-3.1,7.5);
		glRotatef(90,0,0,1);
		glScalef(.15,.1,.15);
		miniDoor();
	}glPopMatrix();
	glColor3f(0.5,0.5,0.5);
	glPushMatrix();{
		glTranslatef(26.5,-dist2-dist,18);
		//glTranslatef(27,-3.1,18);
		glRotatef(90,0,0,1);
		glScalef(.15,.1,.15);
		miniDoor();
	}glPopMatrix();
	//small door side bars
	glPushMatrix();{
		glTranslatef(26.5,-14.4,0);
		//glRotatef(90,0,0,1);
		//glScalef(.15,.1,.15);
		smallDoorSideBar();
	}glPopMatrix();
	
	glColor3f(0.5,0.5,0.5);
	glPushMatrix();{
		glTranslatef(26.5,dist,7.5);
		glRotatef(90,0,0,1);
		glScalef(.15,.1,.15);
		miniDoor();
	}glPopMatrix();
	glColor3f(0.5,0.5,0.5);
	glPushMatrix();{
		glTranslatef(26.5,dist,18);
		glRotatef(90,0,0,1);
		glScalef(.15,.1,.15);
		miniDoor();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(26.5,14.55,0);
		//glRotatef(90,0,0,1);
		//glScalef(.15,.1,.15);
		smallDoorSideBar();
	}glPopMatrix();
	
	//main 4
	glColor3f(0.5,0.5,0.5);
	glPushMatrix();{
		glTranslatef(-25.5,6.9,7.5);
		glRotatef(-90,0,0,1);
		glScalef(.333,.2,.333);
		door();
	}glPopMatrix();
	//side bar
	glPushMatrix();{
		glTranslatef(0,-26,0);
		glRotatef(-90,0,0,1);
		mainDoorSideBar();
	}glPopMatrix();

	//small doors
	glColor3f(0.5,0.5,0.5);
	glPushMatrix();{
		glTranslatef(-27,dist2+dist,7.5);
		glRotatef(-90,0,0,1);
		glScalef(.15,.1,.15);
		miniDoor();
	}glPopMatrix();
	glColor3f(0.5,0.5,0.5);
	glPushMatrix();{
		glTranslatef(-27,dist2+dist,18);
		glRotatef(-90,0,0,1);
		glScalef(.15,.1,.15);
		miniDoor();
	}glPopMatrix();
	//small door side bars
	glPushMatrix();{
		glTranslatef(-26.5,14.4,0);
		//glRotatef(90,0,0,1);
		//glScalef(.15,.1,.15);
		smallDoorSideBar();
	}glPopMatrix();
	
	glColor3f(0.5,0.5,0.5);
	glPushMatrix();{
		glTranslatef(-27,-dist,7.5);
		glRotatef(-90,0,0,1);
		glScalef(.15,.1,.15);
		miniDoor();
	}glPopMatrix();
	glColor3f(0.5,0.5,0.5);
	glPushMatrix();{
		glTranslatef(-27,-dist,18);
		glRotatef(-90,0,0,1);
		glScalef(.15,.1,.15);
		miniDoor();
	}glPopMatrix();
	//small door side bars
	glPushMatrix();{
		glTranslatef(-26.5,-14.5,0);
		//glRotatef(90,0,0,1);
		//glScalef(.15,.1,.15);
		smallDoorSideBar();
	}glPopMatrix();
	
	//draw 45 degree doors
	glColor3f(0.5,0.5,0.5);
	glPushMatrix();{
		glTranslatef(27-dist2,-25,7.5);
		glRotatef(45,0,0,1);
		glScalef(.15,.1,.15);
		miniDoor();
	}glPopMatrix();
	glColor3f(0.5,0.5,0.5);
	glPushMatrix();{
		glTranslatef(27-dist2,-25,18);
		glRotatef(45,0,0,1);
		glScalef(.15,.1,.15);
		miniDoor();
	}glPopMatrix();
	//small door side bars
	glPushMatrix();{
		glTranslatef(25-2.3,25-2,0);
		glRotatef(45,0,0,1);
		//glScalef(.15,.1,.15);
		smallDoorSideBar();
	}glPopMatrix();
	
	glColor3f(0.5,0.5,0.5);
	glPushMatrix();{
		glTranslatef(-25,-27+dist2,7.5);
		glRotatef(-45,0,0,1);
		glScalef(.15,.1,.15);
		miniDoor();
	}glPopMatrix();
	glColor3f(0.5,0.5,0.5);
	glPushMatrix();{
		glTranslatef(-25,-27+dist2,18);
		glRotatef(-45,0,0,1);
		glScalef(.15,.1,.15);
		miniDoor();
	}glPopMatrix();
	//small door side bars
	glPushMatrix();{
		glTranslatef(-25+2.3,-25+2,0);
		glRotatef(45,0,0,1);
		//glScalef(.15,.1,.15);
		smallDoorSideBar();
	}glPopMatrix();


	glColor3f(0.5,0.5,0.5);
	glPushMatrix();{
		glTranslatef(25,27-dist2,7.5);
		glRotatef(135,0,0,1);
		glScalef(.15,.1,.15);
		miniDoor();
	}glPopMatrix();
	glColor3f(0.5,0.5,0.5);
	glPushMatrix();{
		glTranslatef(25,27-dist2,18);
		glRotatef(135,0,0,1);
		glScalef(.15,.1,.15);
		miniDoor();
	}glPopMatrix();
	//small door side bars
	glPushMatrix();{
		glTranslatef(-25+2,25-2.3,0);
		glRotatef(-45,0,0,1);
		//glScalef(.15,.1,.15);
		smallDoorSideBar();
	}glPopMatrix();

	glColor3f(0.5,0.5,0.5);
	glPushMatrix();{
		glTranslatef(-27+dist2,25,7.5);
		glRotatef(-135,0,0,1);
		glScalef(.15,.1,.15);
		miniDoor();
	}glPopMatrix();
	glColor3f(0.5,0.5,0.5);
	glPushMatrix();{
		glTranslatef(-27+dist2,25,18);
		glRotatef(-135,0,0,1);
		glScalef(.15,.1,.15);
		miniDoor();
	}glPopMatrix();
	//small door side bars
	glPushMatrix();{
		glTranslatef(25-2,-25+2.3,0);
		glRotatef(-45,0,0,1);
		//glScalef(.15,.1,.15);
		smallDoorSideBar();
	}glPopMatrix();
}

void drawRedPartDoor(){
	glPushMatrix();{
		glTranslatef(160-2,-6.4,2);
		glRotatef(90,0,0,1);
		glScalef(.3,.3,.23);
		door();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(160-24,6.4,2);
		glRotatef(-90,0,0,1);
		glScalef(.3,.3,.23);
		door();
	}glPopMatrix();
}

void minarBase(){
	glColor3f(0.5,0.5,0.5);

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, minarBaseBmp); // here texid corresponds a bitmap image.
	gluQuadricNormals(IDquadric,GLU_SMOOTH);
	gluQuadricTexture(IDquadric, GLU_TRUE);
	glColor3f(1,1,1);
	glPushMatrix();{
		glTranslatef(48-2.2,46-1.9,0.6);
		glRotatef(22.5,0,0,1);
		//glScalef(.15,.1,.15);
		gluCylinder(IDquadric,5,5,9.5,8,20);
	}glPopMatrix();

	glColor3f(1,1,1);
	glPushMatrix();{
		glTranslatef(-48+2.2,46-1.9,0.6);
		glRotatef(22.5,0,0,1);
		//glScalef(.15,.1,.15);
		gluCylinder(IDquadric,5,5,9.5,8,20);
	}glPopMatrix();
	
	glColor3f(1,1,1);
	glPushMatrix();{
		glTranslatef(-48+2.2,-46+1.9,0.6);
		glRotatef(22.5,0,0,1);
		//glScalef(.15,.1,.15);
		gluCylinder(IDquadric,5,5,9.5,8,20);
	}glPopMatrix();

	glColor3f(1,1,1);
	glPushMatrix();{
		glTranslatef(48-2.2,-46+1.9,0.6);
		glRotatef(22.5,0,0,1);
		//glScalef(.15,.1,.15);
		gluCylinder(IDquadric,5,5,9.5,8,20);
	}glPopMatrix();
	glDisable(GL_TEXTURE_2D);
	

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, minarBaseFloorBmp); // here texid corresponds a bitmap image.
	gluQuadricNormals(IDquadric,GLU_SMOOTH);
	gluQuadricTexture(IDquadric, GLU_TRUE);
	glColor3f(1,1,1);
	glPushMatrix();{
		glTranslatef(48-2.2,46-1.9,9.5+0.5);
		glRotatef(22.5,0,0,1);
		//glScalef(.15,.1,.15);
		//glutSolidCircle(5,8);
		gluDisk(IDquadric,0,5,8,20);
	}glPopMatrix();
	glColor3f(1,1,1);
	glPushMatrix();{
		glTranslatef(-48+2.2,46-1.9,9.5+0.5);
		glRotatef(22.5,0,0,1);
		//glScalef(.15,.1,.15);
		//glutSolidCircle(5,8);
		gluDisk(IDquadric,0,5,8,20);
	}glPopMatrix();
	glColor3f(1,1,1);
	glPushMatrix();{
		glTranslatef(-48+2.2,-46+1.9,9.5+0.5);
		glRotatef(22.5,0,0,1);
		//glScalef(.15,.1,.15);
		//glutSolidCircle(5,8);
		gluDisk(IDquadric,0,5,8,20);
	}glPopMatrix();
	glColor3f(1,1,1);
	glPushMatrix();{
		glTranslatef(48-2.2,-46+1.9,9.5+0.5);
		glRotatef(22.5,0,0,1);
		//glScalef(.15,.1,.15);
		//glutSolidCircle(5,8);
		gluDisk(IDquadric,0,5,8,20);
	}glPopMatrix();
	glDisable(GL_TEXTURE_2D);
}

void minar(){
	glColor3f(1,1,1);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, minarBmp); // here texid corresponds a bitmap image.
	gluQuadricNormals(IDquadric,GLU_SMOOTH);
	gluQuadricTexture(IDquadric, GLU_TRUE);
	//gluCylinder(IDquadric,25 ,25 ,50,20,20);
	//glDisable(GL_TEXTURE_2D);
	//glColor3f(0.7,0.7,0.7);
	glPushMatrix();{
		glTranslatef(48-2.2,46-1.9,6.8);
		glRotatef(22.5,0,0,1);
		//glScalef(.15,.1,.15);
		gluCylinder(IDquadric,3,2.5,36,20,20);
	}glPopMatrix();
	//glColor3f(0.5,0.5,0.5);
	glPushMatrix();{
		glTranslatef(-48+2.2,46-1.9,6.8);
		glRotatef(22.5,0,0,1);
		//glScalef(.15,.1,.15);
		gluCylinder(IDquadric,3,2.5,36,20,20);
	}glPopMatrix();
	//glColor3f(0.5,0.5,0.5);
	glPushMatrix();{
		glTranslatef(-48+2.2,-46+1.9,6.8);
		glRotatef(22.5,0,0,1);
		//glScalef(.15,.1,.15);
		gluCylinder(IDquadric,3,2.5,36,20,20);
	}glPopMatrix();
	//glColor3f(0.5,0.5,0.5);
	glPushMatrix();{
		glTranslatef(48-2.2,-46+1.9,6.8);
		glRotatef(22.5,0,0,1);
		//glScalef(.15,.1,.15);
		gluCylinder(IDquadric,3,2.5,36,20,20);
	}glPopMatrix();
	glDisable(GL_TEXTURE_2D);

	//gombuj
	glPushMatrix();{
		glTranslatef(48-2.2,46-1.9,6.8+36+1);
		glScalef(0.09,0.09,0.09);
		minarTop();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-48+2.2,46-1.9,6.8+36+1);
		glScalef(0.09,0.09,0.09);
		minarTop();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-48+2.2,-46+1.9,6.8+36+1);
		glScalef(0.09,0.09,0.09);
		minarTop();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(48-2.2,-46+1.9,6.8+36+1);
		glScalef(0.09,0.09,0.09);
		minarTop();
	}glPopMatrix();

	//minar top rings
	float sc=0.18;
	float hei=34;
	glPushMatrix();{
		glTranslatef(48-2.2,46-1.9,6.8+hei+1);
		glScalef(sc,sc,sc);
		minarRing();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-48+2.2,46-1.9,6.8+hei+1);
		glScalef(sc,sc,sc);
		minarRing();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-48+2.2,-46+1.9,6.8+hei+1);
		glScalef(sc,sc,sc);
		minarRing();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(48-2.2,-46+1.9,6.8+hei+1);
		glScalef(sc,sc,sc);
		minarRing();
	}glPopMatrix();
	sc=sc+0.01;

	hei=22;
	glPushMatrix();{
		glTranslatef(48-2.2,46-1.9,6.8+hei+1);
		glScalef(sc,sc,sc);
		minarRing();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-48+2.2,46-1.9,6.8+hei+1);
		glScalef(sc,sc,sc);
		minarRing();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-48+2.2,-46+1.9,6.8+hei+1);
		glScalef(sc,sc,sc);
		minarRing();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(48-2.2,-46+1.9,6.8+hei+1);
		glScalef(sc,sc,sc);
		minarRing();
	}glPopMatrix();
	sc=sc+0.01;

	hei=10;
	glPushMatrix();{
		glTranslatef(48-2.2,46-1.9,6.8+hei+1);
		glScalef(sc,sc,sc);
		minarRing();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-48+2.2,46-1.9,6.8+hei+1);
		glScalef(sc,sc,sc);
		minarRing();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-48+2.2,-46+1.9,6.8+hei+1);
		glScalef(sc,sc,sc);
		minarRing();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(48-2.2,-46+1.9,6.8+hei+1);
		glScalef(sc,sc,sc);
		minarRing();
	}glPopMatrix();
	sc=sc+0.01;

}

void roof(){
	glColor3f(0.9,0.95,0.9);
	double height=29;
	glPushMatrix();{
		glColor3f(1,1,1);
		glEnable(GL_TEXTURE_2D);
			glBindTexture(GL_TEXTURE_2D,roofBmp);
			glNormal3f(1.0,0.0,0.0);
		glBegin(GL_POLYGON);{
			/*glTexCoord2f(0,0);
			glTexCoord2f(0,4);
			glTexCoord2f(4,4);
			glTexCoord2f(4,0);*/
			glTexCoord2f(0.296,0);glVertex3f(-20+1,-27,height);
			glTexCoord2f(1.704,0);glVertex3f(20-1,-27,height);
			glTexCoord2f(2,0.296);glVertex3f(27,-20+1,height);
			glTexCoord2f(2,1.704);glVertex3f(27,20-1,height);
			glTexCoord2f(1.704,2);glVertex3f(20-1,27,height);
			glTexCoord2f(0.296,2);glVertex3f(-20+1,27,height);
			glTexCoord2f(0,1.704);glVertex3f(-27,20-1,height);
			glTexCoord2f(0,0.296);glVertex3f(-27,-20+1,height);

			//glVertex3f(20-1,27,height);
			//glVertex3f(27,20-1,height);
			//glVertex3f(27,-20+1,height);
			//glVertex3f(20-1,-27,height);
			//glVertex3f(-20+1,-27,height);
			//glVertex3f(-27,-20+1,height);
			//glVertex3f(-27,20-1,height);
			//glVertex3f(-20+1,27,height);
		}glEnd();
		glDisable(GL_TEXTURE_2D);
	}glPopMatrix();

	glColor3f(0.95,0.95,0.94);
	glPushMatrix();{
		glTranslatef(19-4,27,height+1);
		glScalef(10,1.5,2);
		drawUnitSolidCube();
	}glPopMatrix();
	
	//glColor3f(0.5,0.5,0.4);
	glPushMatrix();{
		glTranslatef(-19+4,27,height+1);
		glScalef(10,1.5,2);
		drawUnitSolidCube();
	}glPopMatrix();
	//glColor3f(0.5,0.5,0.4);
	glPushMatrix();{
		glTranslatef(19-4,-27,height+1);
		glScalef(10,1.5,2);
		drawUnitSolidCube();
	}glPopMatrix();
	
	//glColor3f(0.5,0.5,0.4);
	glPushMatrix();{
		glTranslatef(-19+4,-27,height+1);
		glScalef(10,1.5,2);
		drawUnitSolidCube();
	}glPopMatrix();

	//glColor3f(0.5,0.5,0.4);
	glPushMatrix();{
		glTranslatef(27,19-4,height+1);
		glScalef(1.5,10,2);
		drawUnitSolidCube();
	}glPopMatrix();
	//glColor3f(0.5,0.5,0.4);
	glPushMatrix();{
		glTranslatef(27,-19+4,height+1);
		glScalef(1.5,10,2);
		drawUnitSolidCube();
	}glPopMatrix();
	//glColor3f(0.5,0.5,0.4);
	glPushMatrix();{
		glTranslatef(-27,19-4,height+1);
		glScalef(1.5,10,2);
		drawUnitSolidCube();
	}glPopMatrix();
	//glColor3f(0.5,0.5,0.4);
	glPushMatrix();{
		glTranslatef(-27,-19+4,height+1);
		glScalef(1.5,10,2);
		drawUnitSolidCube();
	}glPopMatrix();

	//glColor3f(0.8,0.8,0.4);
	glPushMatrix();{
		glTranslatef(23.2,23.2,height+1);
		glRotatef(45,0,0,1);
		glScalef(1.5,11,2);
		drawUnitSolidCube();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-23.2,-23.2,height+1);
		glRotatef(45,0,0,1);
		glScalef(1.5,11,2);
		drawUnitSolidCube();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(23.2,-23.2,height+1);
		glRotatef(-45,0,0,1);
		glScalef(1.5,11.5,2);
		drawUnitSolidCube();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-23.2,23.2,height+1);
		glRotatef(-45,0,0,1);
		glScalef(1.5,11.5,2);
		drawUnitSolidCube();
	}glPopMatrix();


	//roof small gombuj
	glPushMatrix();{
		glTranslatef(15,15,height);
		//glRotatef(-45,0,0,1);
		glScalef(0.2,0.2,0.2);
		minarTop();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-15,15,height);
		//glRotatef(-45,0,0,1);
		glScalef(0.2,0.2,0.2);
		minarTop();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(15,-15,height);
		//glRotatef(-45,0,0,1);
		glScalef(0.2,0.2,0.2);
		minarTop();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-15,-15,height);
		//glRotatef(-45,0,0,1);
		glScalef(0.2,0.2,0.2);
		minarTop();
	}glPopMatrix();

	roofBigGombuj();

}


void roofBigGombuj(){
	double height=29;
	glColor3f(0.7,0.7,0.7);
	glPushMatrix();{
		glTranslatef(0,0,height);
		gluCylinder(IDquadric,10,10,5,50,50);
	}glPopMatrix();	
	glColor3f(1,1,1);
	glPushMatrix();{
		glTranslatef(0,0,height+5);
		glutSolidCircle(10,50);
	}glPopMatrix();
	glColor3f(0.5,0.5,0.5);
	glPushMatrix();{
		glTranslatef(0,0,height+5);
		gluCylinder(IDquadric,9,9,4,50,50);
	}glPopMatrix();	
	glColor3f(0.3 ,0.3,0.3);
	glPushMatrix();{
		glTranslatef(0,0,height+5+4);
		gluCylinder(IDquadric,9,9,2,50,50);
	}glPopMatrix();

	glColor3f(0.7 ,0.7,0.7);
	glPushMatrix();{
		glTranslatef(0,0,height+5+4+2);
		gluCylinder(IDquadric,10,10,0.5,50,50);
	}glPopMatrix();
	glColor3f(0.8,0.8,0.8);
	glPushMatrix();{
		glTranslatef(0,0,height+5+4+2);
		glutSolidCircle(10,50);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,height+5+4+2+.5);
		glutSolidCircle(10,50);
	}glPopMatrix();
	//gombuj
	glPushMatrix();{
		glTranslatef(0,0,height+15);
		glScalef(9,9,9);
		gombuj();
	}glPopMatrix();

}

void minarTop(){
	//botto,
	glColor3f(0.9,0.9,0.9);
	glPushMatrix();{
		glRotatef(22.5,0,0,1);
		gluCylinder(IDquadric,25,25,2,8,20);
	}glPopMatrix();
	glColor3f(0.8,0.8,0.8);
	glPushMatrix();{
		glTranslatef(0,0,2);
		glRotatef(22.5,0,0,1);
		glutSolidCircle(25,8);
	}glPopMatrix();
	
	//under gombuj
	glColor3f(0.9,0.9,0.9);
	glPushMatrix();{
		glTranslatef(0,0,42);
		glRotatef(22.5,0,0,1);
		gluCylinder(IDquadric,30,30,2,8,20);
	}glPopMatrix();
	glColor3f(0.8,0.8,0.8);
	glPushMatrix();{
		glTranslatef(0,0,44);
		glRotatef(22.5,0,0,1);
		glutSolidCircle(30,8);
	}glPopMatrix();
	glColor3f(0.8,0.8,0.8);
	glPushMatrix();{
		glTranslatef(0,0,42);
		glRotatef(22.5,0,0,1);
		glutSolidCircle(30,8);
	}glPopMatrix();
	

	glColor3f(0.9,0.9,0.9);
	glPushMatrix();{
		glTranslatef(0,0,44);
		glRotatef(22.5,0,0,1);
		gluCylinder(IDquadric,22,22,4,8,20);
	}glPopMatrix();
	glColor3f(0.7,0.7,0.7);
	glPushMatrix();{
		glTranslatef(0,0,48);
		glRotatef(22.5,0,0,1);
		glutSolidCircle(22,8);
	}glPopMatrix();

	
	//gombuj
	glPushMatrix();{
		glTranslatef(0,0,52);
		glScalef(18,18,18);
		gombuj();
	}glPopMatrix();
	
	//bars
	glColor3f(1,1,1);
	glPushMatrix();{
		glTranslatef(0,22,2);
		minarTopBars();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,-22,2);
		minarTopBars();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(22,0,2);
		glRotatef(90,0,0,1);
		minarTopBars();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-22,0,2);
		glRotatef(90,0,0,1);
		minarTopBars();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(15.6,-15.6,2);
		glRotatef(45,0,0,1);
		minarTopBars();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-15.6,15.6,2);
		glRotatef(45,0,0,1);
		minarTopBars();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-15.6,-15.6,2);
		glRotatef(-45,0,0,1);
		minarTopBars();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(15.6,15.6,2);
		glRotatef(-45,0,0,1);
		minarTopBars();
	}glPopMatrix();
}

void minarTopBars(){
	glPushMatrix();{
		glTranslatef(0,0,39.5);
		glScalef(20,2,2);
		drawUnitSolidCube();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-9,0,20);
		glScalef(2,2,40);
		drawUnitSolidCube();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(9,0,20);
		glScalef(2,2,40);
		drawUnitSolidCube();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-10,0,29);
		glBegin(GL_QUADS);{
			glVertex3f(0,0,0);
			glVertex3f(0,0,10);
			glVertex3f(10,0,10);
			glVertex3f(10,0,9);
		}glEnd();
		glBegin(GL_QUADS);{
			glVertex3f(20,0,0);
			glVertex3f(20,0,10);
			glVertex3f(10,0,10);
			glVertex3f(10,0,9);
		}glEnd();
	}glPopMatrix();
}

void gombuj(){
	double equ[4];
	equ[0]=0;
	equ[1]=0;
	equ[2]=1;
	equ[3]=.4;

	glClipPlane(GL_CLIP_PLANE0,equ);

	glEnable(GL_CLIP_PLANE0);{
		glColor3f(1,1,1);
		glPushMatrix();{
			gluSphere(IDquadric,1,20,20);
		}glPopMatrix();
	}glDisable(GL_CLIP_PLANE0);

	//glPushMatrix();{
	//	glTranslatef(0,0,.81);
	//	gluCylinder(IDquadric,0.58,0,.3,20,20);
	//}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,0,1.12);
		glScalef(0.01,0.01,0.01);
		gombujTop();
	}glPopMatrix();


}

void gombujTop(){
	glColor3f(0.7,0.5,0.4);
	glPushMatrix();{
		gluCylinder(IDquadric,1,1,50,20,20);
	}glPopMatrix();
	glPushMatrix();{
		gluCylinder(IDquadric,10,8,5,20,20);
		glTranslatef(0,0,5);
		glutSolidCircle(8,20);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,12);
		gluSphere(IDquadric,6,20,20);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,26);
		gluSphere(IDquadric,8,20,20);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,26);
		gluCylinder(IDquadric,7,1,15,20,20);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,42);
		gluCylinder(IDquadric,1,3,1,20,20);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,43);
		gluCylinder(IDquadric,3,1,1,20,20);
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,0,50);
		gluSphere(IDquadric,4,20,20);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,50);
		gluCylinder(IDquadric,3.5,1,10,20,20);
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,0,62);
		gluSphere(IDquadric,3,20,20);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,65);
		gluCylinder(IDquadric,1,2,1,20,20);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,66);
		gluCylinder(IDquadric,2,0,5,20,20);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,55);
		glRotatef(90,1,0,0);
		glScalef(0.15,0.15,0.15);
		moon();
	}glPopMatrix();
	
	glColor3f(0.7,0.7,0.7);
	glPushMatrix();{
		glTranslatef(0,0,-6);
		gluCylinder(IDquadric,15,10,6,20,20);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,-10);
		gluCylinder(IDquadric,20,15,4,20,20);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,-13);
		gluCylinder(IDquadric,28,20,3,20,20);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,-14);
		gluCylinder(IDquadric,35,28,1,20,20);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,-18);
		gluCylinder(IDquadric,28,35,4,20,20);
	}glPopMatrix();
	
}

void moon(){
	glPushMatrix();{
		glBegin(GL_QUADS);{
			glVertex3f(0,5,0);
			glVertex3f(20,8,0);
			glVertex3f(20,-3,0);
			glVertex3f(0,-7,0);
		}glEnd();
		glBegin(GL_QUADS);{
			glVertex3f(20,8,0);
			glVertex3f(20,-3,0);
			glVertex3f(40,10,0);
			glVertex3f(40,20,0);
		}glEnd();
		glBegin(GL_QUADS);{
			glVertex3f(40,10,0);
			glVertex3f(40,20,0);
			glVertex3f(55,35,0);
			glVertex3f(55,30,0);
		}glEnd();
		glBegin(GL_TRIANGLES);{
			glVertex3f(55,35,0);
			glVertex3f(55,30,0);
			glVertex3f(63,45,0);
		}glEnd();
	}glPopMatrix();
	glPushMatrix();{
		glRotatef(180,0,1,0);
		glBegin(GL_QUADS);{
			glVertex3f(0,5,0);
			glVertex3f(20,8,0);
			glVertex3f(20,-3,0);
			glVertex3f(0,-7,0);
		}glEnd();
		glBegin(GL_QUADS);{
			glVertex3f(20,8,0);
			glVertex3f(20,-3,0);
			glVertex3f(40,10,0);
			glVertex3f(40,20,0);
		}glEnd();
		glBegin(GL_QUADS);{
			glVertex3f(40,10,0);
			glVertex3f(40,20,0);
			glVertex3f(55,35,0);
			glVertex3f(55,30,0);
		}glEnd();
		glBegin(GL_TRIANGLES);{
			glVertex3f(55,35,0);
			glVertex3f(55,30,0);
			glVertex3f(63,45,0);
		}glEnd();
	}glPopMatrix();
}


void drawThinMinars(){
	glColor3f(0.3,0.3,0.5);
	glPushMatrix();{
		glTranslatef(28,10,0);
		glScalef(1.2,1.2,1.7);
		drawRedThinMinar();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(28,-10,0);
		glScalef(1,1,1.7);
		drawRedThinMinar();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-28,10,0);
		glScalef(1,1,1.7);
		drawRedThinMinar();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-28,-10,0);
		glScalef(1,1,1.7);
		drawRedThinMinar();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(10,28,0);
		glScalef(1,1,1.7);
		drawRedThinMinar();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-10,28,0);
		glScalef(1,1,1.7);
		drawRedThinMinar();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-10,-28,0);
		glScalef(1,1,1.7);
		drawRedThinMinar();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(10,-28,0);
		glScalef(1,1,1.7);
		drawRedThinMinar();
	}glPopMatrix();

	//small ones
	glPushMatrix();{
		glTranslatef(19.9,-27.8,0);
		glScalef(1.2,1.2,1.5);
		drawRedThinMinar();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-19.9,-27.8,0);
		glScalef(1.2,1.2,1.5);
		drawRedThinMinar();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(19.9,27.8,0);
		glScalef(1.2,1.2,1.5);
		drawRedThinMinar();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-19.9,27.8,0);
		glScalef(1.2,1.2,1.5);
		drawRedThinMinar();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(27.8,-19.9,0);
		glScalef(1.2,1.2,1.5);
		drawRedThinMinar();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-27.8,-19.9,0);
		glScalef(1.2,1.2,1.5);
		drawRedThinMinar();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-27.8,19.9,0);
		glScalef(1.2,1.2,1.5);
		drawRedThinMinar();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(27.8,19.9,0);
		glScalef(1.2,1.2,1.5);
		drawRedThinMinar();
	}glPopMatrix();
}

void minarRing(){
	//torus
	glColor3f(0.6,0.6,0.6);
	glPushMatrix();{
		glTranslatef(0,0,0);
		glutSolidTorus(0.5,14,20,20);
	}glPopMatrix();
	//cone
	glColor3f(0.6,0.6,0.6);
	glPushMatrix();{
		glTranslatef(0,0,2);
		gluCylinder(IDquadric,14,16,1,20,20);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,3);
		gluDisk(IDquadric,0,16,20,20);
	}glPopMatrix();
	//cone
	glColor3f(0.8,0.8,0.8);
	glPushMatrix();{
		glTranslatef(0,0,3);
		gluCylinder(IDquadric,14,20,5,20,20);
	}glPopMatrix();
	glColor3f(0.6,0.6,0.6);
	glPushMatrix();{
		glTranslatef(0,0,8);
		gluCylinder(IDquadric,22,22,1,20,20);
	}glPopMatrix();
	glColor3f(0.9,0.9,0.9);
	glPushMatrix();{
		glTranslatef(0,0,8);
		gluDisk(IDquadric,0,22,20,20);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,9);
		gluDisk(IDquadric,0,22,20,20);
	}glPopMatrix();

	glColor3f(0.7,0.7,0.7);
	glPushMatrix();{
		glTranslatef(0,0,9);
		gluCylinder(IDquadric,20,20,5,20,20);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,9);
		gluCylinder(IDquadric,19,19,5,20,20);
	}glPopMatrix();
	glColor3f(0.8,0.8,0.8);
	glPushMatrix();{
		glTranslatef(0,0,14);
		gluDisk(IDquadric,19,20,20,20);
	}glPopMatrix();
	
	GLUquadric *qu=gluNewQuadric();
	float x=0;
	float y=0;
	float r=19.5;
	for(int i=0;i<360;i=i+20){
		x=r*cos((M_PI*i)/180);
		y=r*sin((M_PI*i)/180);
		glPushMatrix();{
			glColor3f(0.3,0.3,0.3);
			glTranslatef(x,y,9+3);
			glRotatef(i,0,0,1);
			glScalef(1.5,1.5,6);
			drawUnitSolidCube();
			glColor3f(0.8,0.5,0.5);
			glTranslatef(0,0,0.6);
			glScalef(1,1,.4);
			gluSphere(qu,0.4,4,4);
		}glPopMatrix();


		x=(r+1.5)*cos((M_PI*i)/180);
		y=(r+1.5)*sin((M_PI*i)/180);
		glPushMatrix();{
			glColor3f(0.3,0.3,0.3);
			glTranslatef(x,y,3.5);
			glRotatef(90,1,0,0);
			glRotatef(i,0,1,0);
			glScalef(0.2,0.2,0.2);
			smallTorus();
		}glPopMatrix();
		x=(r+2)*cos((M_PI*i)/180);
		y=(r+2)*sin((M_PI*i)/180);
		glPushMatrix();{
			glColor3f(0.8,0.5,0.5);
			glTranslatef(x,y,6.5);
			gluSphere(qu,0.8,4,4);
		}glPopMatrix();
	}

}

void smallTorus(){
	double equ[4];
	equ[0]=-1.5;
	equ[1]=2;
	equ[2]=0;
	equ[3]=-20;

	glClipPlane(GL_CLIP_PLANE0,equ);

	glEnable(GL_CLIP_PLANE0);{
		glPushMatrix();{
			glutSolidTorus(3,20,10,10);
		}glPopMatrix();
	}glDisable(GL_CLIP_PLANE0);
	
}

void floorRailing(){
	glColor3f(0.6,0.6,0.6);
	glPushMatrix();{
		glTranslatef(-6,0,3);
		glScalef(1,2,6);
		drawUnitSolidCube();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(6,0,3);
		glScalef(1,2,6);
		drawUnitSolidCube();
	}glPopMatrix();
	glColor3f(1,1,1);
	glPushMatrix();{
		glBegin(GL_QUADS);{
			glVertex3f(-8,0,0);glColor3f(0.9,0.9,0.9);
			glVertex3f(8,0,0);glColor3f(0.9,0.9,0.9);
			glVertex3f(8,0,5);glColor3f(0.8,0.8,0.8);
			glVertex3f(-8,0,5);glColor3f(0.8,0.8,0.8);
		}glEnd();
		glBegin(GL_QUADS);{
			glVertex3f(-8,1,0);glColor3f(0.9,0.9,0.9);
			glVertex3f(8,1,0);glColor3f(0.9,0.9,0.9);
			glVertex3f(8,1,5);glColor3f(0.8,0.8,0.8);
			glVertex3f(-8,1,5);glColor3f(0.8,0.8,0.8);
		}glEnd();
		glBegin(GL_QUADS);{
			glVertex3f(-8,0,5);glColor3f(0.8,0.8,0.8);
			glVertex3f(8,0,5);glColor3f(0.7,0.7,0.7);
			glVertex3f(8,1,5);glColor3f(0.7,0.7,0.7);
			glVertex3f(-8,1,5);glColor3f(0.8,0.8,0.8);
		}glEnd();
	}glPopMatrix();
}

void drawGombuj(){
	glPushMatrix();{
		glTranslatef(0,0,1.5246+1.8376+.1661*2);
		glScalef(9.4403/2,9.4403/2,9.4403/2);
		glTranslatef(0,0,.4);
		gombuj();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.8,0,0);
		glTranslatef(0,0,1.5246+1.8376+.1661*2);
		gluDisk(IDquadric,0,9/2,50,50);
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.8,0,0);
		glTranslatef(0,0,1.5246+1.8376+.1661);
		gluCylinder(IDquadric,9.3/2,9/2,.1661,50,50);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,1.5246+1.8376);
		gluCylinder(IDquadric,9/2,9.3/2,.1661,50,50);
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.8,0,0);
		glTranslatef(0,0,1.8376);
		gluCylinder(IDquadric,9/2,9/2,1.5246,50,50);
	}glPopMatrix();
	
	glPushMatrix();{
		//glTranslatef(0,0,2.7843);
		//glScalef(9.4403/2,9.4403/2,9.4403/2);
		glColor3f(.5,0,0);
		gluCylinder(IDquadric,9/2,9/2,1.8376,50,50);
	}glPopMatrix();
}
void drawBigGombuj(){
	glPushMatrix();{
		glTranslatef(0,0,2.5+2.7843+.2519*2);
		glScalef(14.3035/2,14.3035/2,14.3035/2);
		glTranslatef(0,0,.4);
		gombuj();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.8,0,0);
		glTranslatef(0,0,2.7248+2.7843+.2519*2);
		gluDisk(IDquadric,0,13.9/2,50,50);
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.8,0,0);
		glTranslatef(0,0,2.7248+2.7843+.2519);
		gluCylinder(IDquadric,14.2/2,13.9/2,.2519,50,50);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,2.7248+2.7843);
		gluCylinder(IDquadric,13.9/2,14.2/2,.2519,50,50);
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.8,0,0);
		glTranslatef(0,0,2.7843);
		gluCylinder(IDquadric,13.9/2,13.9/2,2.7248,50,50);
	}glPopMatrix();
	
	glPushMatrix();{
		//glTranslatef(0,0,2.7843);
		//glScalef(9.4403/2,9.4403/2,9.4403/2);
		glColor3f(.5,0,0);
		gluCylinder(IDquadric,13.9/2,13.9/2,2.7843,50,50);
	}glPopMatrix();
}
void drawRedMinarSide(){
	glPushMatrix();{
		glScalef(1.47,.5,.33);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,.5,.5);
		glTranslatef(0,0,.33/2+.352/2);
		glScalef(1.47,.5,.352);
		drawUnitSolidCube();
	}glPopMatrix();

	
	glPushMatrix();{
		glColor3f(0,.7,.3);
		glTranslatef(1.47/2-.2024/2,0,.517+1.2121/2);
		glScalef(.2024,.5,1.2121);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(0,.7,.3);
		glTranslatef(-1.47/2+.2024/2,0,.517+1.2121/2);
		glScalef(.2024,.5,1.2121);
		drawUnitSolidCube();
	}glPopMatrix();
	
	glPushMatrix();{
		glColor3f(0,.7,0);
		glTranslatef(0,0,1.7291+.352/2);
		glScalef(1.47,.5,.352);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		//glColor3f(1,0,0);
		glColor3f(0.37,0.2,0.2);
		glTranslatef(0,-.125,.517+1.25/2);
		glScalef(1.1,.25,1.25);
		drawUnitSolidCube();
	}glPopMatrix();


	glPushMatrix();{
		glTranslatef(0,0,2.0811+17.173/2);
		glScalef(1.47,.5,17.173);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,.9);
		//glTranslatef(.5/2,-3.1018/2,2.2461+.3282+15.1449+.5129/2);
		glTranslatef(0,0,2.0811+17.173+.2118/2);
		glScalef(1.47,.5+.1358*2,.2118);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(1,1,1);
		//glTranslatef(.5/2,-3.1018/2,2.2461+.3282+15.1449+.5129/2);
		glTranslatef(0,0,2.0811+17.173+.2118+.1/2);
		glScalef(1.47,.5,.1);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(1,1,1);
		//glTranslatef(.5/2,-3.1018/2,2.2461+.3282+15.1449+.5129/2);
		glTranslatef(0,0,2.0811+17.173+.2118+.1+1.1748/2);
		glScalef(1.47,.5,1.1748);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(1,1,1);
		//glTranslatef(.5/2,-3.1018/2,2.2461+.3282+15.1449+.5129/2);
		glTranslatef(0,0,2.0811+17.173+.2118+.1+1.1748+.1573/2);
		glScalef(1.47,.5+.0524*2,.1573);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.8,0,0);
		//glTranslatef(.5/2,-3.1018/2,2.2461+.3282+15.1449+.5129/2);
		glTranslatef(0,0,2.0811+17.173+.2118+.1+1.1748+.1573+.4577/2);
		glScalef(1.47,.5,.4577);
		drawUnitSolidCube();
	}glPopMatrix();


	glPushMatrix();{
		glColor3f(.8,0,0);
		//glTranslatef(.5/2,-3.1018/2,2.2461+.3282+15.1449+.5129/2);
		glTranslatef(0,0,2.0811+17.173+.2118+.1+1.1748+.1573+.4577+.1005/2);
		glScalef(1.8,.2458+.8659,.1005);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.8,0,0);
		//glTranslatef(.5/2,-3.1018/2,2.2461+.3282+15.1449+.5129/2);
		glTranslatef(0,0,2.0811+17.173+.2118+.1+1.1748+.1573+.4577+.1005+.5725/2);
		glScalef(1.47,.25,.5725);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.8,0,0);
		//glTranslatef(.5/2,-3.1018/2,2.2461+.3282+15.1449+.5129/2);
		glTranslatef(0,0,2.0811+17.173+.2118+.1+1.1748+.1573+.4577+.1005+.5741/2);
		glScalef(1.47,.5,.5741);
		drawUnitSolidCube();
	}glPopMatrix();
	glPushMatrix();{
		glColor3f(.8,0,0);
		//glTranslatef(.5/2,-3.1018/2,2.2461+.3282+15.1449+.5129/2);
		glTranslatef(0,0,2.0811+17.173+.2118+.1+1.1748+.1573+.4577+.1005+.5741+.2691/2);
		glScalef(1.47,.5,.2691);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.8,0,0);
		//glTranslatef(.5/2,-3.1018/2,2.2461+.3282+15.1449+.5129/2);
		glTranslatef(1.47/2-.1862/2,0,2.0811+17.173+.2118+.1+1.1748+.1573+.4577+.1005+.5741+.2691+.2287/2);
		glScalef(.1862,.5,.2287);
		drawUnitSolidCube();
	}glPopMatrix();
	glPushMatrix();{
		glColor3f(.8,0,0);
		//glTranslatef(.5/2,-3.1018/2,2.2461+.3282+15.1449+.5129/2);
		glTranslatef(-1.47/2+.1862/2,0,2.0811+17.173+.2118+.1+1.1748+.1573+.4577+.1005+.5741+.2691+.2287/2);
		glScalef(.1862,.5,.2287);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.8,0,0);
		//glTranslatef(.5/2,-3.1018/2,2.2461+.3282+15.1449+.5129/2);
		glTranslatef(-1.47/2,0,2.0811+17.173+.2118+.1+1.1748+.1573+.4577+.1005+.5741+.2691+.2287);
		
		gluCylinder(IDquadric,.1453,.1453,1.3318,50,50);
	}glPopMatrix();
	
	glPushMatrix();{
		glColor3f(.8,0,0);
		//glTranslatef(.5/2,-3.1018/2,2.2461+.3282+15.1449+.5129/2);
		glTranslatef(1.47/2,0,2.0811+17.173+.2118+.1+1.1748+.1573+.4577+.1005+.5741+.2691+.2287);
		gluCylinder(IDquadric,.1453,.1453,1.3318,50,50);
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.8,0,0);
		//glTranslatef(.5/2,-3.1018/2,2.2461+.3282+15.1449+.5129/2);
		glTranslatef(1.47/2-.1862/2,0,2.0811+17.173+.2118+.1+1.1748+.1573+.4577+.1005+.5741+.2691+.2287+1.3318+.7968/2);
		glScalef(.1862,.5,.7968);
		drawUnitSolidCube();
	}glPopMatrix();
	glPushMatrix();{
		glColor3f(.8,0,0);
		//glTranslatef(.5/2,-3.1018/2,2.2461+.3282+15.1449+.5129/2);
		glTranslatef(-1.47/2+.1862/2,0,22.5281+1.3318+.7968/2);
		glScalef(.1862,.5,.7968);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.8,0,0);
		//glTranslatef(.5/2,-3.1018/2,2.2461+.3282+15.1449+.5129/2);
		glTranslatef(0,0,22.5281+1.3318+.7968+.1818/2);
		glScalef(1.47,.5,.1818);
		drawUnitSolidCube();
	}glPopMatrix();
	glPushMatrix();{
		glColor3f(.8,0,0);
		//glTranslatef(.5/2,-3.1018/2,2.2461+.3282+15.1449+.5129/2);
		glTranslatef(0,0,22.5281+1.3318+.7968+.1818+.1005/2);
		glScalef(2.6,1.9939+.8659,.1005);
		drawUnitSolidCube();
	}glPopMatrix();

	

}
void drawRedMinar(){
	glPushMatrix();{
		glColor3f(1,1,1);
		glTranslatef(.25,0,.33/2);
		glRotatef(90,0,0,1);
		drawRedMinarSide();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(1,1,1);
		glTranslatef(.43+.25,-1.1,.33/2);
		glRotatef(-45,0,0,1);
		drawRedMinarSide();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(1,1,1);
		glTranslatef(.735+1.039,-1.7744+.25,.33/2);
		drawRedMinarSide();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(1,1,1);
		glTranslatef(.25+1.47*sin(45.0)+1.47-.2,-1.47/2-(1.47/2)*sin(45.0)+.25,.33/2);
		glRotatef(45,0,0,1);
		drawRedMinarSide();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(1,1,1);
		glTranslatef(.25+1.47*sin(45.0)+1.47+.25,0,.33/2);
		glRotatef(90,0,0,1);
		drawRedMinarSide();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(1,1,1);
		glTranslatef(.25+1.47*sin(45.0)+1.47-.2,1.47/2+(1.47/2)*sin(45.0)-.25,.33/2);
		glRotatef(-45,0,0,1);
		drawRedMinarSide();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(1,1,1);
		glTranslatef(.735+1.039,1.7744-.25,.33/2);
		drawRedMinarSide();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(1,1,1);
		glTranslatef(.43+.25,1.1,.33/2);
		glRotatef(45,0,0,1);
		drawRedMinarSide();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(1.1+.5,0,2.0811+17.173+.2118+.1+1.1748+.1573+.4577+.1005+.5741);
		gluDisk(IDquadric,0,1.1,50,50);
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(1.1+.5,0,22.5281+1.3318+.7968+.1818+.1005+.2);
		//glScalef(.5,.5,.5);
		//drawGombuj();
		glScalef(2.25,2.25,2.25);
		glTranslatef(0,0,.4);
		gombuj();
	}glPopMatrix();

}
void drawRedNarrowFrontWall(){
	
	//-1.47*sin(45.0)-1.47/2+.25

	//a
	glPushMatrix();{
		glColor3f(1,1,1);
		glTranslatef(2.77/2,-3.4685/2,.33/2);
		glScalef(2.7700,3.4685,.33);
		drawUnitSolidCube();
	}glPopMatrix();

	//b
	glPushMatrix();{
		glColor3f(.5,.5,.5);
		glTranslatef(.5/2,-3.4685/2,.33+.352/2);
		glScalef(.5,3.4685,.352);
		drawUnitSolidCube();
	}glPopMatrix();

	//c
	glPushMatrix();{
		glColor3f(.5,.5,.5);
		glTranslatef(.5/2,-.2947/2,.33+.352+1.2121/2);
		glScalef(.5,.2947,1.2121);
		drawUnitSolidCube();
	}glPopMatrix();

	//d
	glPushMatrix();{
		glColor3f(.5,.5,.5);
		glTranslatef(.5/2,-.2947/2-2.4805-.2947,.33+.352+1.2121/2);
		glScalef(.5,.2947,1.2121);
		drawUnitSolidCube();
	}glPopMatrix();

	//e
	glPushMatrix();{
		glColor3f(.5,.5,.5);
		glTranslatef(.5/2,-3.1018/2,.33+.352+1.2121+.352/2);
		glScalef(.5,3.1018,.352);
		drawUnitSolidCube();
	}glPopMatrix();

	

	//j
	glPushMatrix();{
		glColor3f(.8,.8,.8);
		glTranslatef(.5/2+.15,-2.49/2-.2947,.33+.352+1.22/2);
		glScalef(.5,2.49,1.22);
		drawUnitSolidCube();
	}glPopMatrix();

	//f
	glPushMatrix();{
		glColor3f(.8,.8,.8);
		glTranslatef(.5/2,-3.1018/2,2.2461+.3282/2);
		glScalef(.5,3.1018,.3282);
		drawUnitSolidCube();
	}glPopMatrix();


	//g
	glPushMatrix();{
		glColor3f(.8,.8,.8);
		glTranslatef(.5/2,-.2947/2,2.2461+.3282+15.145/2);
		glScalef(.5,.2947,15.145);
		drawUnitSolidCube();
	}glPopMatrix();
	
	//h
	glPushMatrix();{
		glColor3f(.8,.8,.8);
		glTranslatef(.5/2,-.3241/2-2.4805-.2947,2.2461+.3282+15.1449/2);
		glScalef(.5,.3241,15.145);
		drawUnitSolidCube();
	}glPopMatrix();

	//i
	glPushMatrix();{
		glColor3f(1,1,1);
		glTranslatef(.5/2,-3.1018/2,2.2461+.3282+15.1449+.5129/2);
		glScalef(.5,3.1018,.5129);
		drawUnitSolidCube();
	}glPopMatrix();

	//k
	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2+.15,-2.485/2-.2947,2.2461+.3282+15.145/2);
		glScalef(.5,2.485,15.145);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2+.2,-3.1018/2,2.2461+.3282+15.1449+.5129+1.1914/2);
		glScalef(.5,3.1018,1.1914);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.3,0,0);
		glTranslatef(.5/2,-3.1018/2,2.2461+.3282+15.1449+.5129+1.1914+.2118/2);
		glScalef(.5,3.1018,.2118);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2+.1,-3.1018/2,2.2461+.3282+15.1449+.5129+1.1914+.2118+.1/2);
		glScalef(.5,3.1018,.1);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2+.15,-3.1018/2,2.2461+.3282+15.1449+.5129+1.1914+.2118+.1+1.1748/2);
		glScalef(.5,3.1018,1.1748);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-3.1018/2,2.2461+.3282+15.1449+.5129+1.1914+.2118+.1+1.1748+.1573/2);
		glScalef(.5,3.1018,.1573);
		drawUnitSolidCube();
	}glPopMatrix();
}
void drawRedThinMinarRing(){
	glPushMatrix();{
		glTranslatef(0,0,.5089+.3157+21.6996);
		GLUquadricObj *quadratic;
		quadratic=gluNewQuadric();
		gluDisk(quadratic,0,.405,8,50);	
	}glPopMatrix();


	glPushMatrix();{
		glTranslatef(0,0,.5089+.3157+21.6996);
		GLUquadricObj *quadratic;
		quadratic=gluNewQuadric();
		gluCylinder(quadratic,.405,.455,.1,8,50);	
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,0,.5089+.3157+21.6996+.1);
		GLUquadricObj *quadratic;
		quadratic=gluNewQuadric();
		gluCylinder(quadratic,.455,.405,.1,8,50);	
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,0,.5089+.3157+21.6996+.1+.1);
		GLUquadricObj *quadratic;
		quadratic=gluNewQuadric();
		gluDisk(quadratic,0,.405,8,50);	
	}glPopMatrix();
}
void drawRedThinMinar(){
	glColor3f(.8,0,0);
	

	glPushMatrix();{		
		GLUquadricObj *quadratic;
		quadratic=gluNewQuadric();
		gluCylinder(quadratic,.3865,.3865,.5089,8,50);	
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,0,.5089);
		GLUquadricObj *quadratic;
		quadratic=gluNewQuadric();
		gluDisk(quadratic,0,.3865,8,50);	
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,0,.5089);
		GLUquadricObj *quadratic;
		quadratic=gluNewQuadric();
		gluCylinder(quadratic,.3365,.32,.3157,8,50);	
	}glPopMatrix();

	glColor3f(1,1,1);
	glPushMatrix();{
		glTranslatef(0,0,.5089+.3157);
		GLUquadricObj *quadratic;
		quadratic=gluNewQuadric();
		gluCylinder(quadratic,.32,.32,21.6996,8,50);	
	}glPopMatrix();


	//making of the torus on minar
	glColor3f(.8,0,0);
	drawRedThinMinarRing();
	
	glPushMatrix();{
		
		glTranslatef(0,0,-2.816);
		drawRedThinMinarRing();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,0,-2.816-4.6716*1);
		drawRedThinMinarRing();
	}glPopMatrix();
	
	glPushMatrix();{
		glTranslatef(0,0,-2.816-4.6716*2);
		drawRedThinMinarRing();
	}glPopMatrix();
	
	glPushMatrix();{
		glTranslatef(0,0,-2.816-4.6716*3);
		drawRedThinMinarRing();
	}glPopMatrix();

	glColor3f(.5,0,0);

	glPushMatrix();{
		glTranslatef(0,0,.5089+.3157+21.6996+.1+.1);
		GLUquadricObj *quadratic;
		quadratic=gluNewQuadric();
		gluCylinder(quadratic,.385,.485,.8,8,50);	
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,0,.5089+.3157+21.6996+.1+.1+1);
		GLUquadricObj *quadratic;
		quadratic=gluNewQuadric();
		gluSphere(quadratic,.5,50,50);	
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,0,24.3);
		glScalef(.01,.01,.01);
		gombujTop();
	}glPopMatrix();

}
void drawRedWideFrontWall(){
	glPushMatrix();{
		glTranslatef(2.7702/2,-8.8215/2,.33/2);
		glScalef(2.7702,8.8215,.33);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,-1.5960/2,.33+.352/2);
		glScalef(.5,1.5960,.352);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,-.3372/2,.33+.352+1.2121/2);
		glScalef(.5,.3372,1.2121);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,.3372/2-1.5960,.33+.352+1.2121/2);
		glScalef(.5,.3372,1.2121);
		drawUnitSolidCube();
	}glPopMatrix();
	
	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,-1.5960/2,.33+.352+1.2121+.352/2);
		glScalef(.5,1.5960,.352);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-1.5960/2,2.2461+.3282/2);
		glScalef(.5,1.5960,.3282);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-.4424/2,2.2461+.3282+15.145/2);
		glScalef(.5,.4424,15.145);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,.4424/2-1.5960,2.2461+.3282+6.5687/2);
		glScalef(.5,.4424,6.5687);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-6.5436/2-1.5960+.4424,2.2461+.3282+6.5687+.5012/2);
		glScalef(.5,6.5436,.5012);
		drawUnitSolidCube();
	}glPopMatrix();

	//right design
	glPushMatrix();{
		glTranslatef(0,-8.8215+1.5960,0);
		glPushMatrix();{
			glColor3f(.7,0,0);
			glTranslatef(.5/2,-1.5960/2,.33+.352/2);
			glScalef(.5,1.5960,.352);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glColor3f(.7,0,0);
			glTranslatef(.5/2,-.3372/2,.33+.352+1.2121/2);
			glScalef(.5,.3372,1.2121);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glColor3f(.7,0,0);
			glTranslatef(.5/2,.3372/2-1.5960,.33+.352+1.2121/2);
			glScalef(.5,.3372,1.2121);
			drawUnitSolidCube();
		}glPopMatrix();
	
		glPushMatrix();{
			glColor3f(.7,0,0);
			glTranslatef(.5/2,-1.5960/2,.33+.352+1.2121+.352/2);
			glScalef(.5,1.5960,.352);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glColor3f(.5,0,0);
			glTranslatef(.5/2,-1.5960/2,2.2461+.3282/2);
			glScalef(.5,1.5960,.3282);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glColor3f(.5,0,0);
			glTranslatef(.5/2,.4424/2-1.5960,2.2461+.3282+15.145/2);
			glScalef(.5,.4424,15.145);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glColor3f(.5,0,0);
			glTranslatef(.5/2,-.4424/2,2.2461+.3282+6.5687/2);
			glScalef(.5,.4424,6.5687);
			drawUnitSolidCube();
		}glPopMatrix();
	}glPopMatrix();


	//upper surfaces
	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-8.8215/2,2.2461+.3282+15.1449+.5129/2);
		glScalef(.5,8.8215,.5129);
		drawUnitSolidCube();
	}glPopMatrix();
	
	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2+.2,-8.8215/2,2.2461+.3282+15.1449+.5129+1.1914/2);
		glScalef(.5,8.8215,1.1914);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.3,0,0);
		glTranslatef(.5/2,-8.8215/2,2.2461+.3282+15.1449+.5129+1.1914+.2118/2);
		glScalef(.5,8.8215,.2118);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2+.1,-8.8215/2,2.2461+.3282+15.1449+.5129+1.1914+.2118+.1/2);
		glScalef(.5,8.8215,.1);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2+.15,-8.8215/2,2.2461+.3282+15.1449+.5129+1.1914+.2118+.1+1.1748/2);
		glScalef(.5,8.8215,1.1748);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-8.8215/2,2.2461+.3282+15.1449+.5129+1.1914+.2118+.1+1.1748+.1573/2);
		glScalef(.5,8.8215,.1573);
		drawUnitSolidCube();
	}glPopMatrix();

	//deeper back surface

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2+.25,-7.9/2-.45,9.6125+8.1/2);
		glScalef(.5,7.9,8.1);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2+.25,-.7/2-.45,2.5425+7.1/2);
		glScalef(.5,.7,7.1);
		drawUnitSolidCube();
	}glPopMatrix();
	
	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2+.25,-.7/2-.45-7.2,2.5425+7.1/2);
		glScalef(.5,.7,7.1);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.8,.8,.8);
		glTranslatef(.5/2+.15,-1.2/2-.2,.33+.352+1.22/2);
		glScalef(.5,1.2,1.22);
		drawUnitSolidCube();
	}glPopMatrix();


	glPushMatrix();{
		glColor3f(.8,.8,.8);
		glTranslatef(.5/2+.15,-1.2/2-.2-7.2,.33+.352+1.22/2);
		glScalef(.5,1.2,1.22);
		drawUnitSolidCube();
	}glPopMatrix();

	//window design

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-6.5436/2-1.5960+.4424,2.2461+.3282+6.5687+.5012+.8527+.6/2);
		glScalef(.5,6.5436,.6);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,.4424/2-1.5960,2.2461+.3282+6.5687+.5012+.8527+.6+5.4691/2);
		glScalef(.5,.4424,5.4691);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,.4424*1.5-1.5960-6.5436,2.2461+.3282+6.5687+.5012+.8527+.6+5.4691/2);
		glScalef(.5,.4424,5.4691);
		drawUnitSolidCube();
	}glPopMatrix();
	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-6.5436/2-1.5960+.4424,2.2461+.3282+6.5687+.5012+.8527+.6+5.4691+.6/2);
		glScalef(.5,6.5436,.6);
		drawUnitSolidCube();
	}glPopMatrix();
}
void drawRedBigFrontGate(){
	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(5.4637/2,-21.5350/2,.33/2);
		glScalef(5.4637,21.5350,.33);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,-4.1461/2,.33+.352/2);
		glScalef(.5,4.1461,.352);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,-.3372/2,.33+.352+1.2121/2);
		glScalef(.5,.3372,1.2121);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,.3372/2-4.1461,.33+.352+1.2121/2);
		glScalef(.5,.3372,1.2121);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,-4.1461/2,.33+.352+1.2121+.1940/2);
		glScalef(.5,4.1461,.1940);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,-3.8543/2,.33+.352+1.2121+.1940+.2943/2);
		glScalef(.5,3.8543,.2943);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,-.3/2,.33+.352+1.2121+.1940+.2943+19.3367/2);
		glScalef(.5,.3,19.3367);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,.3/2-3.8543,.33+.352+1.2121+.1940+.2943+16.0824/2);
		glScalef(.5,.3,16.0824);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,-2.4541/2-.7,.33+.352+1.2121+.1940+.2943+.4+.3/2);
		glScalef(.5,2.4541,.3);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,-.3/2-.7,.33+.352+1.2121+.1940+.2943+.4+.3+17.9367/2);
		glScalef(.5,.3,17.9367);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,.3/2-.7-2.4541,.33+.352+1.2121+.1940+.2943+.4+.3+15.7824/2);
		glScalef(.5,.3,15.7824);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,-14.4265/2-3.8543+.3,.33+.352+1.2121+.1940+.2943+16.0824+.3/2-.3);
		glScalef(.5,14.4265,.3);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,-15.8265/2-.7-2.4541+.3,.33+.352+1.2121+.1940+.2943+16.0824+.4+.3/2);
		glScalef(.5,15.8265,.3);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,-20.1348/2-.7,.33+.352+1.2121+.1940+.2943+.4+.3+17.94+.3/2);
		glScalef(.5,20.1348,.3);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,-21.5350/2,.33+.352+1.2121+.1940+.2943+19.3367+.3/2);
		glScalef(.5,21.5350,.3);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,-17.3890,0);
		
		glPushMatrix();{
			glColor3f(.7,0,0);
			glTranslatef(.5/2,-4.1461/2,.33+.352/2);
			glScalef(.5,4.1461,.352);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glColor3f(.7,0,0);
			glTranslatef(.5/2,-.3372/2,.33+.352+1.2121/2);
			glScalef(.5,.3372,1.2121);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glColor3f(.7,0,0);
			glTranslatef(.5/2,.3372/2-4.1461,.33+.352+1.2121/2);
			glScalef(.5,.3372,1.2121);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glColor3f(.7,0,0);
			glTranslatef(.5/2,-4.1461/2,.33+.352+1.2121+.1940/2);
			glScalef(.5,4.1461,.1940);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glColor3f(.7,0,0);
			glTranslatef(.5/2,-3.8543/2-.2918,.33+.352+1.2121+.1940+.2943/2);
			glScalef(.5,3.8543,.2943);
			drawUnitSolidCube();
		}glPopMatrix();
		glPushMatrix();{
			glColor3f(.7,0,0);
			glTranslatef(.5/2,.3/2-3.8543-.2918,.33+.352+1.2121+.1940+.2943+19.3367/2);
			glScalef(.5,.3,19.3367);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glColor3f(.7,0,0);
			glTranslatef(.5/2,-.3/2-.2918,.33+.352+1.2121+.1940+.2943+16.0824/2);
			glScalef(.5,.3,16.0824);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glColor3f(.7,0,0);
			glTranslatef(.5/2,-2.4541/2-.7-.2918,.33+.352+1.2121+.1940+.2943+.4+.3/2);
			glScalef(.5,2.4541,.3);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glColor3f(.7,0,0);
			glTranslatef(.5/2,.3/2-.7-.2918-2.4541,.33+.352+1.2121+.1940+.2943+.4+.3+17.9367/2);
			glScalef(.5,.3,17.9367);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glColor3f(.7,0,0);
			glTranslatef(.5/2,-.3/2-.7-.2918,.33+.352+1.2121+.1940+.2943+.4+.3+15.7824/2);
			glScalef(.5,.3,15.7824);
			drawUnitSolidCube();
		}glPopMatrix();

	}glPopMatrix();

	//upper surface
	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-21.5350/2,.33+.352+1.2121+.1940+.2943+19.3367+.3+.5129/2);
		glScalef(.5,21.5350,.5129);
		drawUnitSolidCube();
	}glPopMatrix();
	
	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2+.2,-21.5350/2,.33+.352+1.2121+.1940+.2943+19.3367+.3+.5129+1.1914/2);
		glScalef(.5,21.5350,1.1914);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.3,0,0);
		glTranslatef(.5/2,-21.5350/2,.33+.352+1.2121+.1940+.2943+19.3367+.3+.5129+1.1914+.2118/2);
		glScalef(.5,21.5350,.2118);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2+.1,-21.5350/2,.33+.352+1.2121+.1940+.2943+19.3367+.3+.5129+1.1914+.2118+.1/2);
		glScalef(.5,21.5350,.1);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2+.15,-21.5350/2,.33+.352+1.2121+.1940+.2943+19.3367+.3+.5129+1.1914+.2118+.1+1.1748/2);
		glScalef(.5,21.5350,1.1748);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-21.5350/2,.33+.352+1.2121+.1940+.2943+19.3367+.3+.5129+1.1914+.2118+.1+1.1748+.1573/2);
		glScalef(.5,21.5350,.1573);
		drawUnitSolidCube();
	}glPopMatrix();


	//deeper back surface

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2+.25,-3.77/2-.1940,.33+.352+1.22/2);
		glScalef(.5,3.77,1.22);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2+.25,-3.77/2-.1940-17.389,.33+.352+1.22/2);
		glScalef(.5,3.77,1.22);
		drawUnitSolidCube();
	}glPopMatrix();
	
	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2+.25,-2.59/2-.7,.33+.352+1.2121+.1940+.2943+.5/2);
		glScalef(.5,2.59,.5);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2+.25,-2.59/2-.7-17.6808,.33+.352+1.2121+.1940+.2943+.5/2);
		glScalef(.5,2.59,.5);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2+.25,-.5/2-.1940,.33+.352+1.2121+.1940+.2943+19.3367/2);
		glScalef(.5,.5,19.3367);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2+.25,-.5/2-.1940-2.8543,.33+.352+1.2121+.1940+.2943+16.4824/2);
		glScalef(.5,.5,16.4824);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2+.25,-20.2/2-.7,.33+.352+1.2121+.1940+.2943+.4+.3+17.9367+.4+.3/2);
		glScalef(.5,20.2,.5);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2+.25,-14.4265/2-3.5545,.33+.352+1.2121+.1940+.2943+16.4824-.4+.5/2);
		glScalef(.5,14.4265,.5);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2+.25,-.5/2-.45-.1940-20.2,.33+.352+1.2121+.1940+.2943+19.3367/2);
		glScalef(.5,.5,19.3367);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2+.25,-.5/2-.1940-2.8543-14.8265,.33+.352+1.2121+.1940+.2943+16.4824/2);
		glScalef(.5,.5,16.4824);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2+.25,-.33/2-3.8543,.33+.352+1.2121+.1940+16.0824/2);
		glScalef(.5,.33,16.0824);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2+.25,-.33/2-3.8543-13.5098,.33+.352+1.2121+.1940+16.0824/2);
		glScalef(.5,.33,16.0824);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2+.25,-13.2429/2-3.8543-.33,.33+.352+1.2121+.1940+16.0824-.2918/2);
		glScalef(.5,13.2429,.2918);
		drawUnitSolidCube();
	}glPopMatrix();

	//back white surface

	glPushMatrix();{
		glColor3f(1,1,1);
		glTranslatef(.5/2+.2,-1.95/2-1,.33+.352+1.2121+.1940+.2943+.7+17.9367/2);
		glScalef(.5,1.95,17.9367);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(1,1,1);
		glTranslatef(.5/2+.2,-1.95/2-1-17.6806,.33+.352+1.2121+.1940+.2943+.7+17.9367/2);
		glScalef(.5,1.95,17.9367);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(1,1,1);
		glTranslatef(.5/2+.2,-15.83/2-2.8543,.33+.352+1.2121+.1940+.2943+.7+16.0824+1.88/2);
		glScalef(.5,15.83,1.88);
		drawUnitSolidCube();
	}glPopMatrix();
}
void drawRedThinMinarTall(){
	glColor3f(.8,0,0);
	

	glPushMatrix();{		
		GLUquadricObj *quadratic;
		quadratic=gluNewQuadric();
		gluCylinder(quadratic,.3865,.3865,.5089,8,50);	
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,0,.5089);
		GLUquadricObj *quadratic;
		quadratic=gluNewQuadric();
		gluDisk(quadratic,0,.3865,8,50);	
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,0,.5089);
		GLUquadricObj *quadratic;
		quadratic=gluNewQuadric();
		gluCylinder(quadratic,.3365,.32,.3157,8,50);	
	}glPopMatrix();

	glColor3f(1,1,1);
	glPushMatrix();{
		glTranslatef(0,0,.5089+.3157);
		GLUquadricObj *quadratic;
		quadratic=gluNewQuadric();
		gluCylinder(quadratic,.32,.32,26.6167,8,50);	
	}glPopMatrix();


	//making of the torus on minar
	glColor3f(.8,0,0);
	drawRedThinMinarRing();
	
	glPushMatrix();{
		
		glTranslatef(0,0,-2.816);
		drawRedThinMinarRing();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,0,-2.816-4.6716*1);
		drawRedThinMinarRing();
	}glPopMatrix();
	
	glPushMatrix();{
		glTranslatef(0,0,-2.816-4.6716*2);
		drawRedThinMinarRing();
	}glPopMatrix();
	
	glPushMatrix();{
		glTranslatef(0,0,-2.816-4.6716*3);
		drawRedThinMinarRing();
	}glPopMatrix();

	glColor3f(.5,0,0);

	glPushMatrix();{
		glTranslatef(0,0,.5089+.3157+26.6167+.1+.1);
		GLUquadricObj *quadratic;
		quadratic=gluNewQuadric();
		gluCylinder(quadratic,.385,.485,.8,8,50);	
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,0,.5089+.3157+26.6167+.1+.1+1);
		GLUquadricObj *quadratic;
		quadratic=gluNewQuadric();
		gluSphere(quadratic,.5,50,50);	
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,0,4.917);
		drawRedThinMinarRing();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,0,24.3+(26.6167-21.6996));
		glScalef(.01,.01,.01);
		gombujTop();
	}glPopMatrix();
}

void drawRedSideWallDesignedPart(){
	glColor3f(.5,0,0);
	glPushMatrix();{
		glTranslatef(2.67/2,-5.0819/2,.33/2);
		glScalef(2.67,5.0819,.33);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,-.8986/2,.33+.352/2);
		glScalef(.5,.8986,.352);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,-.1795/2,.33+.352+1.2121/2);
		glScalef(.5,.1795,1.2121);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,-.1218/2-.7766,.33+.352+1.2121/2);
		glScalef(.5,.1218,1.2121);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,-.8986/2,.33+.352+1.2121+.352/2);
		glScalef(.5,.8986,.352);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-.8986/2,.33+.352+1.2121+.352+.33/2);
		glScalef(.5,.8986,.33);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-.3082/2,.33+.352+1.2121+.352+.33+15.1449/2);
		glScalef(.5,.3082,15.1449);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-.2505/2-.6479,.33+.352+1.2121+.352+.33+5.1719/2);
		glScalef(.5,.2505,5.1719);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-3.7698/2-.6479,.33+.352+1.2121+.352+.33+5.1719+.3738/2);
		glScalef(.5,3.7698,.3738);
		drawUnitSolidCube();
	}glPopMatrix();
	
	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-3.7698/2-.6479,.33+.352+1.2121+.352+.33+5.1719+.3738+3.8499+.5005/2);
		glScalef(.5,3.7698,.5005);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-.2505/2-.6479,.33+.352+1.2121+.352+.33+5.1719+.3738+3.8499+.5005+4.5575/2);
		glScalef(.5,.2505,4.5575);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-3.7695/2-.6479,.33+.352+1.2121+.352+.33+5.1719+.3738+3.8499+.5005+4.5575+.4380/2);
		glScalef(.5,3.7695,.4380);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-.2505/2-.6479-3.5149,.33+.352+1.2121+.352+.33+5.1719+.3738+3.8499+.5005+4.5575/2);
		glScalef(.5,.2505,4.5575);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-5.0823/2,.33+.352+1.2121+.352+.33+5.1719+.3738+3.8499+.5005+4.5575+.6270+.5129/2);
		glScalef(.5,5.0823,.5129);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,-4.1625,0);
		glPushMatrix();{
			glColor3f(.7,0,0);
			glTranslatef(.5/2,-.8986/2,.33+.352/2);
			glScalef(.5,.8986,.352);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glColor3f(.7,0,0);
			glTranslatef(.5/2,-.1795/2,.33+.352+1.2121/2);
			glScalef(.5,.1795,1.2121);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glColor3f(.7,0,0);
			glTranslatef(.5/2,-.1218/2-.7766,.33+.352+1.2121/2);
			glScalef(.5,.1218,1.2121);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glColor3f(.7,0,0);
			glTranslatef(.5/2,-.8986/2,.33+.352+1.2121+.352/2);
			glScalef(.5,.8986,.352);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glColor3f(.5,0,0);
			glTranslatef(.5/2,-.8986/2,.33+.352+1.2121+.352+.33/2);
			glScalef(.5,.8986,.33);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glColor3f(.5,0,0);
			glTranslatef(.5/2,-.3082/2-.5985,.33+.352+1.2121+.352+.33+15.1449/2);
			glScalef(.5,.3082,15.1449);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glColor3f(.5,0,0);
			glTranslatef(.5/2,-.2505/2,.33+.352+1.2121+.352+.33+5.1719/2);
			glScalef(.5,.2505,5.1719);
			drawUnitSolidCube();
		}glPopMatrix();

	}glPopMatrix();


	//upper surface
	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-5.0819/2,2.2461+.3282+15.1449+.5129/2);
		glScalef(.5,5.0819,.5129);
		drawUnitSolidCube();
	}glPopMatrix();
	
	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2+.2,-5.0819/2,2.2461+.3282+15.1449+.5129+1.1914/2);
		glScalef(.5,5.0819,1.1914);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.3,0,0);
		glTranslatef(.5/2,-5.0819/2,2.2461+.3282+15.1449+.5129+1.1914+.2118/2);
		glScalef(.5,5.0819,.2118);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2+.1,-5.0819/2,2.2461+.3282+15.1449+.5129+1.1914+.2118+.1/2);
		glScalef(.5,5.0819,.1);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2+.15,-5.0819/2,2.2461+.3282+15.1449+.5129+1.1914+.2118+.1+1.1748/2);
		glScalef(.5,5.0819,1.1748);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-5.0819/2,2.2461+.3282+15.1449+.5129+1.1914+.2118+.1+1.1748+.1573/2);
		glScalef(.5,5.0819,.1573);
		drawUnitSolidCube();
	}glPopMatrix();


	//back surface
	
	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2+.25,-4.4532/2-.3082,.33+.352+1.2121+.352+.33+5.1719+.3738+9.973/2);
		glScalef(.5,4.4532,9.973);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2+.25,-.3392/2-.3082,.33+.352+1.2121+.352+.33+5.53/2);
		glScalef(.5,.3392,5.53);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2+.25,-.3392/2-.3082,.33+.352+1.2121+.352+.33+5.53/2);
		glScalef(.5,.3392,5.53);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2+.25,-.3392/2-4.4177,.33+.352+1.2121+.352+.33+5.53/2);
		glScalef(.5,.3392,5.53);
		drawUnitSolidCube();
	}glPopMatrix();


	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2+.25,-.5971/2-.1795,.33+.352+1.2121/2);
		glScalef(.5,.5971,1.2121);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2+.25,-.63/2-4.32,.33+.352+1.2121/2);
		glScalef(.5,.63,1.2121);
		drawUnitSolidCube();
	}glPopMatrix();

}
void drawRedSideWallDesignLessPart(){
	glPushMatrix();{
		glTranslatef(2.25/2,-5.6193/2,6.4524/2);
		glScalef(2.25,5.6193,6.1044+.3480);
		drawUnitSolidCube();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(.5/2,-.3081/2,6.4524+11.2668/2);
		glScalef(.5,.3081,11.2668);
		drawUnitSolidCube();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(.5/2,-.5129/2-5.1064,6.4524+11.2668/2);
		glScalef(.5,.5129,11.2668);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(.5/2,-5.6193/2,6.4524+11.2668+.5129/2);
		glScalef(.5,5.6193,.5129);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(2.1/2+.15,-.15/2-.3081-1.6491,6.4524+2.5/2);
		glScalef(2.1,.15,2.5);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(2.1/2+.15,-.15/2-.3081-1.6491-1.35,6.4524+2.5/2);
		glScalef(2.1,.15,2.5);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(2.1/2+.15,-1.5/2-.3081-1.6491,6.4524+2.5+.15/2);
		glScalef(2.1,1.5,.15);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2+.25,-4.7983/2-.3081,6.4524+2.5+.15+8.6123/2);
		glScalef(.5,4.7983,8.6123);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2+.25,-1.6419/2-.3081,6.4524+2.65/2);
		glScalef(.5,1.6419,2.65);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2+.25,-1.6419/2-.3081-3.1491,6.4524+2.65/2);
		glScalef(.5,1.6419,2.65);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,.3,.3);
		glTranslatef(-.5/2+2.25,-1.1999/2-1.6491-.3081-.15,6.4524+2.5/2);
		glScalef(.5,1.1999,2.5);
		drawUnitSolidCube();
	}glPopMatrix();

	//upper surface
	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-5.6193/2,2.2461+.3282+15.1449+.5129/2);
		glScalef(.5,5.6193,.5129);
		drawUnitSolidCube();
	}glPopMatrix();
	
	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2+.2,-5.6193/2,2.2461+.3282+15.1449+.5129+1.1914/2);
		glScalef(.5,5.6193,1.1914);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.3,0,0);
		glTranslatef(.5/2,-5.6193/2,2.2461+.3282+15.1449+.5129+1.1914+.2118/2);
		glScalef(.5,5.6193,.2118);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2+.1,-5.6193/2,2.2461+.3282+15.1449+.5129+1.1914+.2118+.1/2);
		glScalef(.5,5.6193,.1);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2+.15,-5.6193/2,2.2461+.3282+15.1449+.5129+1.1914+.2118+.1+1.1748/2);
		glScalef(.5,5.6193,1.1748);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-5.6193/2,2.2461+.3282+15.1449+.5129+1.1914+.2118+.1+1.1748+.1573/2);
		glScalef(.5,5.6193,.1573);
		drawUnitSolidCube();
	}glPopMatrix();

}
void drawRedSideWall(){
	
	drawRedSideWallDesignedPart();

	glPushMatrix();{
		glTranslatef(0,-5.0819,0);
		drawRedSideWallDesignedPart();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,-5.0819*2,0);
		drawRedSideWallDesignedPart();
	}glPopMatrix();
	
	
	glPushMatrix();{
		glTranslatef(0,-5.0819*3,0);
		drawRedSideWallDesignLessPart();
	}glPopMatrix();
}
void drawRedBackDesignLessWall(){
	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(2.77/2,-5.6104/2,.33/2);
		glScalef(2.7700,5.6104,.33);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,-5.6104/2,.33+.352/2);
		glScalef(.5,5.6104,.352);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,-.5129/2,.33+.352+1.2121/2);
		glScalef(.5,.5129,1.2121);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,-.5264/2-5.0959,.33+.352+1.2121/2);
		glScalef(.5,.5264,1.2121);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2,-5.6104/2,.33+.352+1.2121+.352/2);
		glScalef(.5,5.6104,.352);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.8,.8,.8);
		glTranslatef(.5/2,-5.6104/2,2.2461+.3282/2);
		glScalef(.5,5.6104,.3282);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.8,.8,.8);
		glTranslatef(.5/2,-.2947/2,2.2461+.3282+15.145/2);
		glScalef(.5,.2947,15.145);
		drawUnitSolidCube();
	}glPopMatrix();
	
	glPushMatrix();{
		glColor3f(.8,.8,.8);
		glTranslatef(.5/2,.2947/2-5.6104,2.2461+.3282+15.145/2);
		glScalef(.5,.2947,15.145);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(1,1,1);
		glTranslatef(.5/2,-5.6104/2,2.2461+.3282+15.1449+.5129/2);
		glScalef(.5,5.6104,.5129);
		drawUnitSolidCube();
	}glPopMatrix();

	//upper surface
	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-5.6104/2,2.2461+.3282+15.1449+.5129/2);
		glScalef(.5,5.6104,.5129);
		drawUnitSolidCube();
	}glPopMatrix();
	
	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2+.2,-5.6104/2,2.2461+.3282+15.1449+.5129+1.1914/2);
		glScalef(.5,5.6104,1.1914);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.3,0,0);
		glTranslatef(.5/2,-5.6104/2,2.2461+.3282+15.1449+.5129+1.1914+.2118/2);
		glScalef(.5,5.6104,.2118);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2+.1,-5.6104/2,2.2461+.3282+15.1449+.5129+1.1914+.2118+.1/2);
		glScalef(.5,5.6104,.1);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2+.15,-5.6104/2,2.2461+.3282+15.1449+.5129+1.1914+.2118+.1+1.1748/2);
		glScalef(.5,5.6104,1.1748);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2,-5.6104/2,2.2461+.3282+15.1449+.5129+1.1914+.2118+.1+1.1748+.1573/2);
		glScalef(.5,5.6104,.1573);
		drawUnitSolidCube();
	}glPopMatrix();


	//back surface
	glPushMatrix();{
		glColor3f(.7,0,0);
		glTranslatef(.5/2+.15,-5.021/2-.2947,2.2461+.3282+15.145/2);
		glScalef(.5,5.021,15.145);
		drawUnitSolidCube();
	}glPopMatrix();
	glPushMatrix();{
		glColor3f(.5,0,0);
		glTranslatef(.5/2+.15,-4.5711/2-.5129,.33+.352+1.2121/2);
		glScalef(.5,4.5711,1.2121);
		drawUnitSolidCube();
	}glPopMatrix();
}
void drawRedFrontWall(){
	glPushMatrix();{
		glTranslatef(-1.3127,1.47*sin(45.0)+1.47/2-.25,0);
		drawRedMinar();
	}glPopMatrix();
	
	drawRedNarrowFrontWall();
	
	glPushMatrix();{
		glTranslatef(0,-3.4,0);
		drawRedThinMinar();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,-3.6,0);
		drawRedWideFrontWall();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,-3.55-9,0);
		drawRedThinMinarTall();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,-3.55-9-.32,0);
		drawRedBigFrontGate();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,-3.55-9-.32-21.5350-.2,0);
		drawRedThinMinarTall();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,-3.55-9-.32-21.5350-.2-.32,0);
		drawRedWideFrontWall();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,-3.55-9-.32-21.5350-.2-.32-8.95,0);
		drawRedThinMinar();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,-3.55-9-.32-21.5350-.2-.32-8.95-.32,0);
		drawRedNarrowFrontWall();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(-1.3127,-3.55-9-.32-21.5350-.2-.32-8.95-.32-3.1018-(1.47*sin(45.0)+1.47/2-.25),0);
		drawRedMinar();
	}glPopMatrix();

}
void drawRedBackWall(){
	drawRedBackDesignLessWall();
	
	glPushMatrix();{
		glTranslatef(0,-5.6104,0);
		drawRedThinMinar();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,-5.6104-.2,0);
		drawRedWideFrontWall();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,-5.7604-9,0);
		drawRedThinMinarTall();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,-5.7604-9-.32,0);
		drawRedBigFrontGate();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,-5.7604-9-.32-21.5350-.2,0);
		drawRedThinMinarTall();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,-5.7604-9-.32-21.5350-.2-.32,0);
		drawRedWideFrontWall();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,-5.7604-9-.32-21.5350-.2-.32-8.95,0);
		drawRedThinMinar();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,-5.7604-9-.32-21.5350-.2-.32-8.95-.32,0);
		drawRedBackDesignLessWall();
	}glPopMatrix();
}
void drawRedSideWallLeft(){
		
	drawRedSideWallDesignLessPart();

	glPushMatrix();{
		glTranslatef(0,-5.6193,0);
		drawRedSideWallDesignedPart();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,-5.6193-5.0819*1,0);
		drawRedSideWallDesignedPart();
	}glPopMatrix();
	
	
	glPushMatrix();{
		glTranslatef(0,-5.6193-5.0819*2,0);
		drawRedSideWallDesignedPart();
	}glPopMatrix();
}

void drawRedPart(){
	drawRedFrontWall();

	glPushMatrix();{
		glTranslatef(1.47*sin(45.0)+1.47/2-.25+.3127,-3.55-9-.32-21.5350-.2-.32-8.95-.32-3.1018-(1.47*sin(45.0)+1.47/2-.25)*2+1.0322,0);
		glRotatef(90,0,0,1);
		drawRedSideWall();
	}glPopMatrix();

	glPushMatrix();{
		
		glTranslatef(1.89+5.0819*3+5.6193+.5,-49.5,0);
		glRotatef(180,0,0,1);
		drawRedBackWall();
	}glPopMatrix();
	
	glPushMatrix();{
		glTranslatef(1.89+5.0819*3+5.6193,2.5,0);
		glRotatef(-90,0,0,1);
		drawRedSideWallLeft();
	}glPopMatrix();

	glPushMatrix();{
		//roof
		glTranslatef(21/2+.9,-50.3456/2+1.5244,2.0811+17.173+.5);
		glScalef(21,50.3456,1);
		drawUnitSolidCube();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(21/2+.9,-50.3456/2+1.5244,2.0811+17.173+1);
		drawBigGombuj();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(21/2+.9,-50.3456/2+1.5244+14.2/2+2.8117+9/2,2.0811+17.173+1);
		drawGombuj();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(21/2+.9,-50.3456/2+1.5244-14.2/2-2.8117-9/2,2.0811+17.173+1);
		drawGombuj();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(17.4835,-1.7744-.5648,0);
		drawRedMinar();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(17.4835,-.4722-1.7744*2-42.9065,0);
		drawRedMinar();
	}glPopMatrix();
}
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<windows.h>
#include<GL/glut.h>
#define BLACK 0, 0, 0
#define M_PI 3.1415923

/****************************************************************
/Function Prototype
*****************************************************************/
void drawGrid();
void glutCircle(float cx, float cy, float r, int num_segments);
void glutSolidCircle(float r, int num_segments);
void glutSolidCylinder(float r, float height, int nothing, int num_segments);
void glutWireCylinder(float r, float height, int slices, int num_segments);
void loadBmp(void);
int LoadBitmap(char *filename);

//new functions
void door();
void miniDoor();
void drawUnitSolidCube();
void minarTop();
void minarTopBars();
void gombuj();
void gombujTop();
void moon();
void minarRing();
void smallTorus();
void floorRailing();

/****************************************************************
/Global Variables
*****************************************************************/

//make a global variable -- for tracking the anglular position of camera
int num_texture = -1;
GLuint texid1;

GLUquadric* IDquadric=gluNewQuadric() ;//=new GLUquadricObj

double cameraAngle;			//in radian
double cameraAngleDelta;
int stopCamera=1;
int changeLookAt;

double cameraHeight;	
double cameraRadius;

double globeAngle; //in degree

double cameraAxisAngle;			//in radian
double cameraLookAtHeight;

int canDrawGrid;


double cameraX;
double cameraY;

double lookAtX;
double lookAtY;

float mirrorCylinder[50];

/****************************************************************
/main functions openGL
*****************************************************************/

void display(){

	//clear the display
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(BLACK, 0);	//color black
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	/********************
	/ set-up camera here
	********************/
	//load the correct matrix -- MODEL-VIEW matrix
	glMatrixMode(GL_MODELVIEW);

	//initialize the matrix
	glLoadIdentity();

	//now give three info
	//1. where is the camera (viewer)?
	//2. where is the camera is looking?
	//3. Which direction is the camera's UP direction?

	//instead of CONSTANT information, we will define a circular path.
	//gluLookAt(-30,-30,50,	0,0,0,	0,0,1);

	//gluLookAt(cameraRadius*cos(cameraAngle), cameraRadius*sin(cameraAngle), cameraHeight,		0,0,0,		0,0,1);
	
	cameraX=cameraRadius*cos(cameraAngle);
	cameraY=cameraRadius*sin(cameraAngle);
	
	if(changeLookAt==1){
		lookAtX=cameraRadius*cos(cameraAxisAngle)+cameraX;
		lookAtY=cameraRadius*sin(cameraAxisAngle)+cameraY;
		changeLookAt=0;
	}
	
	gluLookAt(cameraX, cameraY, cameraHeight,	lookAtX,lookAtY,cameraLookAtHeight,	0,0,1);

	//NOTE: the camera still CONSTANTLY looks at the center
	// cameraAngle is in RADIAN, since you are using inside COS and SIN
	
	//again select MODEL-VIEW
	glMatrixMode(GL_MODELVIEW);

	drawGrid();
	/****************************
	/ Add your objects from here
	****************************/
	//glScalef(0.5,0.5,0.5);
	//door();
	//miniDoor();

	//minarRing();
	//smallTorus();
	floorRailing();

	//minarTop();
	//gombujTop();
	//moon();



	//ADD this line in the end --- if you use double buffer (i.e. GL_DOUBLE)
	glutSwapBuffers();
}

void animate(){
	//codes for any changes in Camera
	if(stopCamera==0){
		cameraAngle += cameraAngleDelta;	// camera will rotate at 0.002 radians per frame.	// keep the camera steady NOW!!
	}

	globeAngle+=1;
	if(globeAngle==360){
		globeAngle=0;
	}

	//codes for any changes in Models
	
	//rectAngle -= 1;

	//MISSING SOMETHING? -- YES: add the following
	glutPostRedisplay();	//this will call the display AGAIN
}

void keyboardListener(unsigned char key, int x,int y){
	switch(key){

		case '1':	//reverse the rotation of camera
			cameraAngleDelta = -cameraAngleDelta;
			break;

		case '2':	//increase rotation of camera by 10%
			cameraAngleDelta *= 1.1;
			break;

		case '3':	//decrease rotation
			cameraAngleDelta /= 1.1;
			break;

		case '8':	//toggle grids
			canDrawGrid = 1 - canDrawGrid;
			break;

		case 27:	//ESCAPE KEY -- simply exit
			exit(0);
			break;

		case 'w':
			cameraLookAtHeight+=10;
			break;
		case 's':
			cameraLookAtHeight-=10;
			break;
		case 'a':
			changeLookAt=1;
			cameraAxisAngle+=.1;
			break;
		case 'd':
			changeLookAt=1;
			cameraAxisAngle-=.1;
			break;
		default:
			break;
	}
}

void specialKeyListener(int key, int x,int y){
	switch(key){
		case GLUT_KEY_DOWN:		//down arrow key
			cameraHeight += 10;
			break;
		case GLUT_KEY_UP:		// up arrow key
			cameraHeight -= 10;
			break;

		case GLUT_KEY_RIGHT:
			cameraAngle -= .05;
			break;
		case GLUT_KEY_LEFT:
			cameraAngle += .05;
			break;

		case GLUT_KEY_PAGE_UP:
			cameraRadius += 10;
			break;
		case GLUT_KEY_PAGE_DOWN:
			if(cameraRadius > 10)
				cameraRadius -= 10;
			break;

		case GLUT_KEY_INSERT:
			break;

		case GLUT_KEY_HOME:
			break;
		case GLUT_KEY_END:
			break;

		default:
			break;
	}
}

void mouseListener(int button, int state, int x, int y){	//x, y is the x-y of the screen (2D)
	switch(button){
		case GLUT_LEFT_BUTTON:
			if(state == GLUT_DOWN){		// 2 times?? in ONE click? -- solution is checking DOWN or UP
				cameraAngleDelta = -cameraAngleDelta;	
			}
			break;

		case GLUT_RIGHT_BUTTON:
			if(state == GLUT_DOWN){		// 2 times?? in ONE click? -- solution is checking DOWN or UP
				stopCamera=1-stopCamera;
			}
			break;

		case GLUT_MIDDLE_BUTTON:
			//........
			break;

		default:
			break;
	}
}


void init(){
	//codes for initialization
	cameraAngle = 0;	//// init the cameraAngle
	cameraAngleDelta = 0.002;
	globeAngle=0;
	canDrawGrid = true;
	cameraHeight = 150;
	cameraRadius = 150;

	loadBmp();

	for(int i=0;i<50;i++){
		mirrorCylinder[i]=rand()%5+10;
	}

	//clear the screen
	glClearColor(BLACK, 0);

	/************************
	/ set-up projection here
	************************/
	//load the PROJECTION matrix
	glMatrixMode(GL_PROJECTION);
	
	//initialize the matrix
	glLoadIdentity();

	//give PERSPECTIVE parameters
	gluPerspective(70,	1,	0.1,	10000.0);
	//field of view in the Y (vertically)
	//aspect ratio that determines the field of view in the X direction (horizontally)
	//near distance
	//far distance
}

int main(int argc, char **argv){
	glutInit(&argc,argv);
	glutInitWindowSize(900, 900);
	glutInitWindowPosition(0, 0);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGB);	//Depth, Double buffer, RGB color

	glutCreateWindow("My OpenGL Program");

	init();

	glEnable(GL_DEPTH_TEST);	//enable Depth Testing

	glutDisplayFunc(display);	//display callback function
	glutIdleFunc(animate);		//what you want to do in the idle time (when no drawing is occuring)

	//ADD keyboard listeners:
	glutKeyboardFunc(keyboardListener);
	glutSpecialFunc(specialKeyListener);

	//ADD mouse listeners:
	glutMouseFunc(mouseListener);

	glutMainLoop();		//The main loop of OpenGL

	return 0;
}



void drawGrid(){
	int i;

	//WILL draw grid IF the "canDrawGrid" is true:

	if(canDrawGrid == 1){
		glColor3f(0.3, 0.3, 0.3);	//grey
		glBegin(GL_LINES);{
			for(i=-10;i<=10;i++){

				if(i==0)
					continue;	//SKIP the MAIN axes

				//lines parallel to Y-axis
				glVertex3f(i*10, -100, 0);
				glVertex3f(i*10,  100, 0);

				//lines parallel to X-axis
				glVertex3f(-100, i*10, 0);
				glVertex3f( 100, i*10, 0);

			}
		}glEnd();
		// draw the two AXES
		//glColor3f(1, 1, 1);	//100% white
		glColor3f(0.3, 0.3, 0.3);
		glBegin(GL_LINES);{
			//Y axis
			glVertex3f(0, -150, 0);	// intentionally extended to -150 to 150, no big deal
			glVertex3f(0,  150, 0);

			//X axis
			glVertex3f(-150, 0, 0);
			glVertex3f( 150, 0, 0);
		}glEnd();
	}

	
}

void glutCircle(float cx, float cy, float r, int num_segments) {
    glBegin(GL_LINE_LOOP);
    for (int i = 0; i < num_segments; i++)   {
        float theta = 2.0f * 3.1415926f * float(i) / float(num_segments);//get the current angle 
        float x = r * cosf(theta);//calculate the x component 
        float y = r * sinf(theta);//calculate the y component 
        glVertex2f(x + cx, y + cy);//output vertex 
    }
    glEnd();
}

void glutSolidCircle(float r, int num_segments) {
    int i=0;
	float theta=(i*2*M_PI)/num_segments;
	float x1=r*cos(theta);
	float y1=r*sin(theta);
	for(i=1;i<=num_segments;i++){
		theta=(i*2*M_PI)/num_segments;
		float x2=r*cos(theta);
		float y2=r*sin(theta);
		
		glBegin(GL_TRIANGLES);{
			glVertex3f(0,0,0);
			glVertex3f(x1,y1,0);
			glVertex3f(x2,y2,0);
		}glEnd();
		
		x1=x2;
		y1=y2;

	}
}

void glutSolidCylinder(float r, float height, int nothing, int num_segments) {
    int i=0;
	float theta=(i*2*M_PI)/num_segments;
	float x1=r*cos(theta);
	float y1=r*sin(theta);
	for(i=1;i<=num_segments;i++){
		theta=(i*2*M_PI)/num_segments;
		float x2=r*cos(theta);
		float y2=r*sin(theta);
		
		glBegin(GL_TRIANGLES);{
			glVertex3f(0,0,0);
			glVertex3f(x1,y1,0);
			glVertex3f(x2,y2,0);
		}glEnd();


		glBegin(GL_TRIANGLES);{
			glVertex3f(0,0,height);
			glVertex3f(x1,y1,height);
			glVertex3f(x2,y2,height);
		}glEnd();
		
		glBegin(GL_QUADS);{
			glVertex3f(x1,y1,0);
			glVertex3f(x2,y2,0);
			glVertex3f(x2,y2,height);
			glVertex3f(x1,y1,height);
		}glEnd();


		x1=x2;
		y1=y2;

	}
}

void glutWireCylinder(float r, float height, int slices, int num_segments) {
    int i=0;
	float theta=(i*2*M_PI)/num_segments;
	float x1=r*cos(theta);
	float y1=r*sin(theta);
	for(i=1;i<=num_segments;i++){
		theta=(i*2*M_PI)/num_segments;
		float x2=r*cos(theta);
		float y2=r*sin(theta);
		
		/*glBegin(GL_TRIANGLES);{
			glVertex3f(0,0,0);
			glVertex3f(x1,y1,0);
			glVertex3f(x2,y2,0);
		}glEnd();


		glBegin(GL_TRIANGLES);{
			glVertex3f(0,0,height);
			glVertex3f(x1,y1,height);
			glVertex3f(x2,y2,height);
		}glEnd();
		*/
		float slice_height=height/(slices*1.0);
		float h=0;
		for(int j=0;j<=slices;j++){
			
			glBegin(GL_LINES);{
				glVertex3f(x1,y1,h);
				glVertex3f(x2,y2,h);
			}glEnd();
			h+=slice_height;
		}


		
		glBegin(GL_LINES);{
			glVertex3f(x1,y1,0);
			glVertex3f(x1,y1,height);
		}glEnd();


		glBegin(GL_LINES);{
			glVertex3f(x2,y2,0);
			glVertex3f(x2,y2,height);
		}glEnd();

		x1=x2;
		y1=y2;

	}
}

int LoadBitmap(char *filename)
{
    int i, j=0;
    FILE *l_file;
    unsigned char *l_texture;

    BITMAPFILEHEADER fileheader;
    BITMAPINFOHEADER infoheader;
    RGBTRIPLE rgb;

    num_texture++;

    if( (l_file = fopen(filename, "rb"))==NULL) return (-1);

    fread(&fileheader, sizeof(fileheader), 1, l_file);

    fseek(l_file, sizeof(fileheader), SEEK_SET);
    fread(&infoheader, sizeof(infoheader), 1, l_file);

    l_texture = (byte *) malloc(infoheader.biWidth * infoheader.biHeight * 4);
    memset(l_texture, 0, infoheader.biWidth * infoheader.biHeight * 4);

 for (i=0; i < infoheader.biWidth*infoheader.biHeight; i++)
    {
            fread(&rgb, sizeof(rgb), 1, l_file);

            l_texture[j+0] = rgb.rgbtRed;
            l_texture[j+1] = rgb.rgbtGreen;
            l_texture[j+2] = rgb.rgbtBlue;
            l_texture[j+3] = 255;
            j += 4;
    }
    fclose(l_file);

    glBindTexture(GL_TEXTURE_2D, num_texture);

    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);

// glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    glTexImage2D(GL_TEXTURE_2D, 0, 4, infoheader.biWidth, infoheader.biHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, l_texture);
     gluBuild2DMipmaps(GL_TEXTURE_2D, 4, infoheader.biWidth, infoheader.biHeight, GL_RGBA, GL_UNSIGNED_BYTE, l_texture);

    free(l_texture);

    return (num_texture);

}

void loadBmp(void)
{

	texid1 =LoadBitmap("brickwall.bmp");   /*here bkg1.bmp is the bitmap image to be used as texture, texid is global varible declared to uniquely  identify this particular image*/
}


void door(){
	glColor3f(1,1,1);
	glPushMatrix();{
		glPushMatrix();{
			glTranslatef(.5,0,33);
			glScalef(1,20,66);
			drawUnitSolidCube();
		}glPopMatrix();
		
		glPushMatrix();{
			glTranslatef(1+39+.5,0,33);
			glScalef(1,20,66);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glTranslatef(20.5,0,.5);
			glScalef(39,20,1);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glTranslatef(20.5,0,66-.5);
			glScalef(39,20,1);
			drawUnitSolidCube();
		}glPopMatrix();

		glColor3f(0.8,0.8,0.8);
		/*glPushMatrix();{
			glTranslatef(-20,0,0);
			glPushMatrix();{
				glTranslatef(0,0,0);
				//glScalef(39,30,3);
				glRotatef(90,1,0,0);
				gluCylinder(IDquadric,12 ,12 ,30,20,20);
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(2*12-39,0,0);
				//glScalef(39,30,3);
				glRotatef(90,1,0,0);
				gluCylinder(IDquadric,12 ,12 ,30,20,20);
			}glPopMatrix();
			glColor3f(0.5,0.5,0.5);
			glPushMatrix();{
				glTranslatef(0,-15,0);
				glBegin(GL_QUADS);{
					glVertex3f(-7.5,15,20);
					glVertex3f(-7.5,-15,20);
					glVertex3f(5,-15,11);
					glVertex3f(5,15,11);
				}glEnd();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,-15,0);
				//glRotatef(90,0,1,0);
				glBegin(GL_QUADS);{
					glVertex3f(-7.5,15,20);
					glVertex3f(-7.5,-15,20);
					glVertex3f(-7.5-12.5,-15,11);
					glVertex3f(-7.5-12.5,15,11);
				}glEnd();
			}glPopMatrix();
			
		}glPopMatrix();
		*/
		//inside door design
		glPushMatrix();{
			glTranslatef(19.5+1,8,22);
			glPushMatrix();{
				glTranslatef(0,0,14.5);
				glScalef(38,1,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(-18.5,0,0);
				glScalef(1,1,30);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(18.5,0,0);
				glScalef(1,1,30);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(-9.5,0,0);
				glScalef(1,1,30);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(9.5,0,0);
				glScalef(1,1,30);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(-14,0,0);
				glScalef(1,1,30);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(14,0,0);
				glScalef(1,1,30);
				drawUnitSolidCube();
			}glPopMatrix();

			glPushMatrix();{
				glTranslatef(14,0,5);
				glScalef(9,1,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(14,0,-5);
				glScalef(9,1,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(14,0,-14.5);
				glScalef(9,1,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(-14,0,5);
				glScalef(9,1,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(-14,0,-5);
				glScalef(9,1,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(-14,0,-14.5);
				glScalef(9,1,1);
				drawUnitSolidCube();
			}glPopMatrix();
		}glPopMatrix();

		//door side
		glPushMatrix();{
			glTranslatef(39.5,-8,22);
			glPushMatrix();{
				glTranslatef(0,0,-3.5);
				glScalef(1,1,37);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,5.5,0);
				glScalef(1,1,30);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,11,0);
				glScalef(1,1,30);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,16,-3.5);
				glScalef(1,1,37);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,8,14.5);
				glScalef(1,16,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,8,5);
				glScalef(1,16,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,8,-5);
				glScalef(1,16,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,8,-14.5);
				glScalef(1,16,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,8,-20.5);
				glScalef(1,16,1);
				drawUnitSolidCube();
			}glPopMatrix();
		}glPopMatrix();

		
		glPushMatrix();{
			glTranslatef(1.5,-8,22);
			glPushMatrix();{
				glTranslatef(0,0,-3.5);
				glScalef(1,1,37);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,5.5,0);
				glScalef(1,1,30);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,11,0);
				glScalef(1,1,30);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,16,-3.5);
				glScalef(1,1,37);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,8,14.5);
				glScalef(1,16,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,8,5);
				glScalef(1,16,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,8,-5);
				glScalef(1,16,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,8,-14.5);
				glScalef(1,16,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,8,-20.5);
				glScalef(1,16,1);
				drawUnitSolidCube();
			}glPopMatrix();
		}glPopMatrix();
	}glPopMatrix();
	
	
	glPushMatrix();{
		glTranslatef(0,0,50);
		glRotatef(90,1,0,0);
		
		double equ[4];
		equ[0]=0;
		equ[1]=-1;
		equ[2]=0;
		equ[3]=15;

		glClipPlane(GL_CLIP_PLANE0,equ);

		glEnable(GL_CLIP_PLANE0);{
		double tran=0;
		for(double i=1;i<20;i=i+.2){
			tran=8*log(i);
			glPushMatrix();{
				glTranslatef(i,tran,0);
				glScalef(1,25,10);
				drawUnitSolidCube();
			}glPopMatrix();
		}
		tran=0;
		for(i=1;i<20;i=i+.2){
			tran=8*log(i);
			glPushMatrix();{
				glTranslatef(-i+41,tran,0);
				glScalef(1,25,10);
				drawUnitSolidCube();
			}glPopMatrix();
		}
			};glDisable(GL_CLIP_PLANE0);
	}glPopMatrix();

	



}

void miniDoor(){
	glColor3f(1,1,1);
	glPushMatrix();{
		glPushMatrix();{
			glTranslatef(.5,0,33);
			glScalef(1,20,66);
			drawUnitSolidCube();
		}glPopMatrix();
		
		glPushMatrix();{
			glTranslatef(1+39+.5,0,33);
			glScalef(1,20,66);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glTranslatef(20.5,0,.5);
			glScalef(39,20,1);
			drawUnitSolidCube();
		}glPopMatrix();

		glPushMatrix();{
			glTranslatef(20.5,0,66-.5);
			glScalef(39,20,1);
			drawUnitSolidCube();
		}glPopMatrix();

		glColor3f(0.8,0.8,0.8);
	
		//inside door design
		glPushMatrix();{
			glTranslatef(19.5+1,8,22);
			glPushMatrix();{
				glTranslatef(0,0,14.5);
				glScalef(38,1,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,0,-14.5-6);
				glScalef(38,1,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(-18.5,0,-3.5);
				glScalef(1,1,37);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(18.5,0,-3.5);
				glScalef(1,1,37);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(-6,0,-3);
				glScalef(1,1,36);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(6,0,-3);
				glScalef(1,1,36);
				drawUnitSolidCube();
			}glPopMatrix();

			glPushMatrix();{
				glTranslatef(0,0,5.5);
				glScalef(12,1,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(14,0,-10.5);
				glScalef(15,1,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(-14,0,-10.5);
				glScalef(15,1,1);
				drawUnitSolidCube();
			}glPopMatrix();
		}glPopMatrix();

		//door side
		glPushMatrix();{
			glTranslatef(39.5,-8,22);
			glPushMatrix();{
				glTranslatef(0,0,-3.5);
				glScalef(1,1,37);
				drawUnitSolidCube();
			}glPopMatrix();
			
			glPushMatrix();{
				glTranslatef(0,8,14.5);
				glScalef(1,16,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,8,-10.5);
				glScalef(1,16,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,8,-20.5);
				glScalef(1,16,1);
				drawUnitSolidCube();
			}glPopMatrix();
		}glPopMatrix();

	
		glPushMatrix();{
			glTranslatef(1.5,-8,22);
			//glTranslatef(39.5,-8,22);
			glPushMatrix();{
				glTranslatef(0,0,-3.5);
				glScalef(1,1,37);
				drawUnitSolidCube();
			}glPopMatrix();
			
			glPushMatrix();{
				glTranslatef(0,8,14.5);
				glScalef(1,16,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,8,-10.5);
				glScalef(1,16,1);
				drawUnitSolidCube();
			}glPopMatrix();
			glPushMatrix();{
				glTranslatef(0,8,-20.5);
				glScalef(1,16,1);
				drawUnitSolidCube();
			}glPopMatrix();
		}glPopMatrix();
	}glPopMatrix();
	
	
	glPushMatrix();{
		glTranslatef(0,0,50);
		glRotatef(90,1,0,0);
		
		double equ[4];
		equ[0]=0;
		equ[1]=-1;
		equ[2]=0;
		equ[3]=15;

		glClipPlane(GL_CLIP_PLANE0,equ);

		glEnable(GL_CLIP_PLANE0);{
		double tran=0;
		for(double i=1;i<20;i=i+.2){
			tran=8*log(i);
			glPushMatrix();{
				glTranslatef(i,tran,0);
				glScalef(1,25,18);
				drawUnitSolidCube();
			}glPopMatrix();
		}
		tran=0;
		for(i=1;i<20;i=i+.2){
			tran=8*log(i);
			glPushMatrix();{
				glTranslatef(-i+41,tran,0);
				glScalef(1,25,18);
				drawUnitSolidCube();
			}glPopMatrix();
		}

		tran=0;
		glPushMatrix();{
			glColor3f(0.4,0.5,0.4);
			glTranslatef(20.5,15,10);
		for(i=3;i<28;i=i+.4){
			tran=5.5*log(i);
			glPushMatrix();{
				glTranslatef(0,-i,-tran);
				glScalef(38,1,1);
				drawUnitSolidCube();
			}glPopMatrix();
		}	
		}glPopMatrix();
		
		};glDisable(GL_CLIP_PLANE0);
	}glPopMatrix();
}


		//glEnable(GL_TEXTURE_2D);
		//glBindTexture(GL_TEXTURE_2D, texid1); // here texid corresponds a bitmap image.
		//gluQuadricNormals(IDquadric,GLU_SMOOTH);
		//gluQuadricTexture(IDquadric, GLU_TRUE);
		//gluCylinder(IDquadric,25 ,25 ,50,20,20);
		//glDisable(GL_TEXTURE_2D);

void drawUnitSolidCube(){
	glPushMatrix();{

		glTranslatef(-.5,-.5,-.5);
		
		glBegin(GL_QUADS);{
			glVertex3f(0,0,0);
			glVertex3f(1,0,0);
			glVertex3f(1,1,0);
			glVertex3f(0,1,0);
		}glEnd();
		glBegin(GL_QUADS);{
			glVertex3f(0,0,0);
			glVertex3f(1,0,0);
			glVertex3f(1,0,1);
			glVertex3f(0,0,1);
		}glEnd();
		
		glBegin(GL_QUADS);{
			glVertex3f(0,0,0);
			glVertex3f(0,1,0);
			glVertex3f(0,1,1);
			glVertex3f(0,0,1);
		}glEnd();
		//glColor3f(0,1,0);
		glBegin(GL_QUADS);{
			glVertex3f(0,1,0);
			glVertex3f(1,1,0);
			glVertex3f(1,1,1);
			glVertex3f(0,1,1);
		}glEnd();
		//glColor3f(0,0,1);
		glBegin(GL_QUADS);{
			glVertex3f(0,0,1);
			glVertex3f(1,0,1);
			glVertex3f(1,1,1);
			glVertex3f(0,1,1);
		}glEnd();
		//glColor3f(1,0,0);
		glBegin(GL_QUADS);{
			glVertex3f(1,0,0);
			glVertex3f(1,1,0);
			glVertex3f(1,1,1);
			glVertex3f(1,0,1);
		}glEnd();

	};glPopMatrix();
}

void minarTop(){
	//botto,
	glColor3f(0.9,0.9,0.9);
	glPushMatrix();{
		glRotatef(22.5,0,0,1);
		gluCylinder(IDquadric,25,25,2,8,20);
	}glPopMatrix();
	glColor3f(0.8,0.8,0.8);
	glPushMatrix();{
		glTranslatef(0,0,2);
		glRotatef(22.5,0,0,1);
		glutSolidCircle(25,8);
	}glPopMatrix();
	
	//under gombuj
	glColor3f(0.9,0.9,0.9);
	glPushMatrix();{
		glTranslatef(0,0,42);
		glRotatef(22.5,0,0,1);
		gluCylinder(IDquadric,30,30,2,8,20);
	}glPopMatrix();
	glColor3f(0.8,0.8,0.8);
	glPushMatrix();{
		glTranslatef(0,0,44);
		glRotatef(22.5,0,0,1);
		glutSolidCircle(30,8);
	}glPopMatrix();
	glColor3f(0.8,0.8,0.8);
	glPushMatrix();{
		glTranslatef(0,0,42);
		glRotatef(22.5,0,0,1);
		glutSolidCircle(30,8);
	}glPopMatrix();
	

	glColor3f(0.9,0.9,0.9);
	glPushMatrix();{
		glTranslatef(0,0,44);
		glRotatef(22.5,0,0,1);
		gluCylinder(IDquadric,22,22,4,8,20);
	}glPopMatrix();
	glColor3f(0.7,0.7,0.7);
	glPushMatrix();{
		glTranslatef(0,0,48);
		glRotatef(22.5,0,0,1);
		glutSolidCircle(22,8);
	}glPopMatrix();

	
	//gombuj
	glPushMatrix();{
		glTranslatef(0,0,52);
		glScalef(18,18,18);
		gombuj();
	}glPopMatrix();
	
	//bars
	glColor3f(1,1,1);
	glPushMatrix();{
		glTranslatef(0,22,2);
		minarTopBars();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,-22,2);
		minarTopBars();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(22,0,2);
		glRotatef(90,0,0,1);
		minarTopBars();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-22,0,2);
		glRotatef(90,0,0,1);
		minarTopBars();
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(15.6,-15.6,2);
		glRotatef(45,0,0,1);
		minarTopBars();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-15.6,15.6,2);
		glRotatef(45,0,0,1);
		minarTopBars();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-15.6,-15.6,2);
		glRotatef(-45,0,0,1);
		minarTopBars();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(15.6,15.6,2);
		glRotatef(-45,0,0,1);
		minarTopBars();
	}glPopMatrix();
}

void minarTopBars(){
	glPushMatrix();{
		glTranslatef(0,0,39.5);
		glScalef(20,2,2);
		drawUnitSolidCube();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-9,0,20);
		glScalef(2,2,40);
		drawUnitSolidCube();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(9,0,20);
		glScalef(2,2,40);
		drawUnitSolidCube();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(-10,0,29);
		glBegin(GL_QUADS);{
			glVertex3f(0,0,0);
			glVertex3f(0,0,10);
			glVertex3f(10,0,10);
			glVertex3f(10,0,9);
		}glEnd();
		glBegin(GL_QUADS);{
			glVertex3f(20,0,0);
			glVertex3f(20,0,10);
			glVertex3f(10,0,10);
			glVertex3f(10,0,9);
		}glEnd();
	}glPopMatrix();
}

void gombuj(){
	double equ[4];
	equ[0]=0;
	equ[1]=0;
	equ[2]=1;
	equ[3]=.4;

	glClipPlane(GL_CLIP_PLANE0,equ);

	glEnable(GL_CLIP_PLANE0);{
		glColor3f(1,1,1);
		glPushMatrix();{
			gluSphere(IDquadric,1,20,20);
		}glPopMatrix();
	}glDisable(GL_CLIP_PLANE0);

	//glPushMatrix();{
	//	glTranslatef(0,0,.81);
	//	gluCylinder(IDquadric,0.58,0,.3,20,20);
	//}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,0,1.12);
		glScalef(0.01,0.01,0.01);
		gombujTop();
	}glPopMatrix();


}

void gombujTop(){
	glColor3f(0.7,0.5,0.4);
	glPushMatrix();{
		gluCylinder(IDquadric,1,1,50,20,20);
	}glPopMatrix();
	glPushMatrix();{
		gluCylinder(IDquadric,10,8,5,20,20);
		glTranslatef(0,0,5);
		glutSolidCircle(8,20);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,12);
		gluSphere(IDquadric,6,20,20);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,26);
		gluSphere(IDquadric,8,20,20);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,26);
		gluCylinder(IDquadric,7,1,15,20,20);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,42);
		gluCylinder(IDquadric,1,3,1,20,20);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,43);
		gluCylinder(IDquadric,3,1,1,20,20);
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,0,50);
		gluSphere(IDquadric,4,20,20);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,50);
		gluCylinder(IDquadric,3.5,1,10,20,20);
	}glPopMatrix();

	glPushMatrix();{
		glTranslatef(0,0,62);
		gluSphere(IDquadric,3,20,20);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,65);
		gluCylinder(IDquadric,1,2,1,20,20);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,66);
		gluCylinder(IDquadric,2,0,5,20,20);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,55);
		glRotatef(90,1,0,0);
		glScalef(0.15,0.15,0.15);
		moon();
	}glPopMatrix();
	
	glColor3f(0.7,0.7,0.7);
	glPushMatrix();{
		glTranslatef(0,0,-6);
		gluCylinder(IDquadric,15,10,6,20,20);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,-10);
		gluCylinder(IDquadric,20,15,4,20,20);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,-13);
		gluCylinder(IDquadric,28,20,3,20,20);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,-14);
		gluCylinder(IDquadric,35,28,1,20,20);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,-18);
		gluCylinder(IDquadric,28,35,4,20,20);
	}glPopMatrix();
	
}

void moon(){
	glPushMatrix();{
		glBegin(GL_QUADS);{
			glVertex3f(0,5,0);
			glVertex3f(20,8,0);
			glVertex3f(20,-3,0);
			glVertex3f(0,-7,0);
		}glEnd();
		glBegin(GL_QUADS);{
			glVertex3f(20,8,0);
			glVertex3f(20,-3,0);
			glVertex3f(40,10,0);
			glVertex3f(40,20,0);
		}glEnd();
		glBegin(GL_QUADS);{
			glVertex3f(40,10,0);
			glVertex3f(40,20,0);
			glVertex3f(55,35,0);
			glVertex3f(55,30,0);
		}glEnd();
		glBegin(GL_TRIANGLES);{
			glVertex3f(55,35,0);
			glVertex3f(55,30,0);
			glVertex3f(63,45,0);
		}glEnd();
	}glPopMatrix();
	glPushMatrix();{
		glRotatef(180,0,1,0);
		glBegin(GL_QUADS);{
			glVertex3f(0,5,0);
			glVertex3f(20,8,0);
			glVertex3f(20,-3,0);
			glVertex3f(0,-7,0);
		}glEnd();
		glBegin(GL_QUADS);{
			glVertex3f(20,8,0);
			glVertex3f(20,-3,0);
			glVertex3f(40,10,0);
			glVertex3f(40,20,0);
		}glEnd();
		glBegin(GL_QUADS);{
			glVertex3f(40,10,0);
			glVertex3f(40,20,0);
			glVertex3f(55,35,0);
			glVertex3f(55,30,0);
		}glEnd();
		glBegin(GL_TRIANGLES);{
			glVertex3f(55,35,0);
			glVertex3f(55,30,0);
			glVertex3f(63,45,0);
		}glEnd();
	}glPopMatrix();

}

void minarRing(){
	//torus
	glColor3f(0.6,0.6,0.6);
	glPushMatrix();{
		glTranslatef(0,0,0);
		glutSolidTorus(0.5,14,20,20);
	}glPopMatrix();
	//cone
	glColor3f(0.6,0.6,0.6);
	glPushMatrix();{
		glTranslatef(0,0,2);
		gluCylinder(IDquadric,14,16,1,20,20);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,3);
		gluDisk(IDquadric,0,16,20,20);
	}glPopMatrix();
	//cone
	glColor3f(0.8,0.8,0.8);
	glPushMatrix();{
		glTranslatef(0,0,3);
		gluCylinder(IDquadric,14,20,5,20,20);
	}glPopMatrix();
	glColor3f(0.6,0.6,0.6);
	glPushMatrix();{
		glTranslatef(0,0,8);
		gluCylinder(IDquadric,22,22,1,20,20);
	}glPopMatrix();
	glColor3f(0.9,0.9,0.9);
	glPushMatrix();{
		glTranslatef(0,0,8);
		gluDisk(IDquadric,0,22,20,20);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,9);
		gluDisk(IDquadric,0,22,20,20);
	}glPopMatrix();

	glColor3f(0.7,0.7,0.7);
	glPushMatrix();{
		glTranslatef(0,0,9);
		gluCylinder(IDquadric,20,20,5,20,20);
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(0,0,9);
		gluCylinder(IDquadric,19,19,5,20,20);
	}glPopMatrix();
	glColor3f(0.8,0.8,0.8);
	glPushMatrix();{
		glTranslatef(0,0,14);
		gluDisk(IDquadric,19,20,20,20);
	}glPopMatrix();
	
	GLUquadric *qu=gluNewQuadric();
	float x=0;
	float y=0;
	float r=19.5;
	for(int i=0;i<360;i=i+20){
		x=r*cos((M_PI*i)/180);
		y=r*sin((M_PI*i)/180);
		glPushMatrix();{
			glColor3f(0.3,0.3,0.3);
			glTranslatef(x,y,9+3);
			glScalef(1.5,1.5,6);
			glRotatef(i,0,0,1);
			drawUnitSolidCube();
			glColor3f(0.8,0.5,0.5);
			glTranslatef(0,0,0.6);
			glScalef(1,1,.4);
			gluSphere(qu,0.4,4,4);
		}glPopMatrix();


		x=(r+1.5)*cos((M_PI*i)/180);
		y=(r+1.5)*sin((M_PI*i)/180);
		glPushMatrix();{
			glColor3f(0.3,0.3,0.3);
			glTranslatef(x,y,3.5);
			glScalef(0.2,0.2,0.2);
			glRotatef(90,1,0,0);
			glRotatef(i,0,1,0);
			smallTorus();
		}glPopMatrix();
		x=(r+2)*cos((M_PI*i)/180);
		y=(r+2)*sin((M_PI*i)/180);
		glPushMatrix();{
			glColor3f(0.8,0.5,0.5);
			glTranslatef(x,y,6.5);
			gluSphere(qu,0.8,4,4);
		}glPopMatrix();
	}

}

void smallTorus(){
	double equ[4];
	equ[0]=-1.5;
	equ[1]=2;
	equ[2]=0;
	equ[3]=-20;

	glClipPlane(GL_CLIP_PLANE0,equ);

	glEnable(GL_CLIP_PLANE0);{
		glPushMatrix();{
			glutSolidTorus(3,20,10,10);
		}glPopMatrix();
	}glDisable(GL_CLIP_PLANE0);
	
}

void floorRailing(){
	glColor3f(0.6,0.6,0.6);
	glPushMatrix();{
		glTranslatef(-6,0,3);
		glScalef(1,2,6);
		drawUnitSolidCube();
	}glPopMatrix();
	glPushMatrix();{
		glTranslatef(6,0,3);
		glScalef(1,2,6);
		drawUnitSolidCube();
	}glPopMatrix();
	glColor3f(1,1,1);
	glPushMatrix();{
		glBegin(GL_QUADS);{
			glVertex3f(-8,0,0);glColor3f(0.9,0.9,0.9);
			glVertex3f(8,0,0);glColor3f(0.9,0.9,0.9);
			glVertex3f(8,0,5);glColor3f(0.8,0.8,0.8);
			glVertex3f(-8,0,5);glColor3f(0.8,0.8,0.8);
		}glEnd();
		glBegin(GL_QUADS);{
			glVertex3f(-8,1,0);glColor3f(0.9,0.9,0.9);
			glVertex3f(8,1,0);glColor3f(0.9,0.9,0.9);
			glVertex3f(8,1,5);glColor3f(0.8,0.8,0.8);
			glVertex3f(-8,1,5);glColor3f(0.8,0.8,0.8);
		}glEnd();
		glBegin(GL_QUADS);{
			glVertex3f(-8,0,5);glColor3f(0.8,0.8,0.8);
			glVertex3f(8,0,5);glColor3f(0.9,0.9,0.9);
			glVertex3f(8,1,5);glColor3f(0.8,0.8,0.8);
			glVertex3f(-8,1,5);glColor3f(0.9,0.9,0.9);
		}glEnd();
	}glPopMatrix();
}